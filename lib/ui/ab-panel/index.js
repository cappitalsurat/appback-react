'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PanelContent = exports.PanelHeader = exports.Panel = undefined;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

require('./style.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Panel = exports.Panel = function Panel(props) {
  return _react2.default.createElement(
    'div',
    null,
    props.children
  );
};

var PanelHeader = exports.PanelHeader = function PanelHeader(props) {
  return _react2.default.createElement(
    _semanticUiReact.Header,
    { textAlign: 'center', as: 'h4', attached: 'top' },
    props.children
  );
};

var PanelContent = exports.PanelContent = function PanelContent(props) {
  return _react2.default.createElement(
    _semanticUiReact.Segment,
    { className: 'outer-segment-padding', attached: true },
    _react2.default.createElement(
      'div',
      { className: 'segment-grid-table' },
      props.children
    )
  );
};