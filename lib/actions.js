"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.customMethodRequest = customMethodRequest;
exports.showLoading = showLoading;
exports.hideLoading = hideLoading;
exports.modelsRequest = modelsRequest;
exports.modelDataRequestSuccess = modelDataRequestSuccess;
exports.setModelInitialData = setModelInitialData;
exports.setCurrentActiveItem = setCurrentActiveItem;
exports.saveModelFormDataSuccess = saveModelFormDataSuccess;
exports.saveModelFormData = saveModelFormData;
exports.createModelFormDataSuccess = createModelFormDataSuccess;
exports.createModelFormData = createModelFormData;
exports.clearFormData = clearFormData;
exports.getSelectModelData = getSelectModelData;
exports.mergeGridConfig = mergeGridConfig;
exports.DeleteGridRow = DeleteGridRow;
exports.getPreLoadFormData = getPreLoadFormData;
exports.resetGridDataOnly = resetGridDataOnly;
exports.importFile = importFile;
exports.exportGridData = exportGridData;

var _lodash = require("lodash");

var _lodash2 = _interopRequireDefault(_lodash);

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } // import _ from 'underscore';


// ------------------------------------
// Actions
// ------------------------------------
function customMethodRequest(configData) {
    var page = configData.page,
        resultsPerPage = configData.resultsPerPage,
        AbModelInstance = configData.AbModelInstance,
        AbModelName = configData.AbModelName,
        externalSortColumn = configData.externalSortColumn,
        externalSortAscending = configData.externalSortAscending,
        externalFilter = configData.externalFilter,
        filters = configData.filters,
        include = configData.include,
        errorCallback = configData.errorCallback,
        loaderCallback = configData.loaderCallback,
        advanceFilter = configData.advanceFilter,
        dataSourceOptions = configData.dataSourceOptions,
        customGridFilter = configData.customGridFilter,
        showPager = configData.showPager,
        dataSuccessCallback = configData.dataSuccessCallback;

    page = page || 1;
    var recordCount = void 0;
    var skip = (page - 1) * resultsPerPage;
    var limit = resultsPerPage;
    var order = externalSortColumn;
    var sortOrder = externalSortAscending ? " ASC" : " DESC";
    var filterCols = _lodash2.default.map(filters, function (filterCol) {
        return _defineProperty({}, filterCol, { "regexp": "/" + externalFilter + "/i" });
    });
    var where = {
        "or": filterCols
    };
    order = order + sortOrder;
    return function (dispatch) {
        dispatch(showLoading(AbModelName));
        if (loaderCallback) {
            loaderCallback('SHOW_LOADER');
        }
        var countFilter = where.or.length ? where : {};
        var methodName = dataSourceOptions.methodName,
            data = dataSourceOptions.data,
            params = dataSourceOptions.params,
            options = dataSourceOptions.options;

        if (customGridFilter) {
            countFilter = _extends({}, countFilter, customGridFilter);
        }

        if (advanceFilter) {
            countFilter = _extends({}, countFilter, advanceFilter);
        }

        var filter = void 0;
        if (include.length > 0) {
            filter = { order: order, limit: limit, skip: skip, where: countFilter, include: include };
        } else {
            filter = { order: order, limit: limit, skip: skip, where: countFilter };
        }
        var finalFilter = { "filter": filter };
        if (params) {
            finalFilter = _extends({}, finalFilter, params);
        }
        if (showPager != null && showPager === false) {
            limit = recordCount;
        }
        AbModelInstance.custom(methodName, data, finalFilter, options).then(function (response) {
            var payloadData = {};
            var recordCount = response.count;
            payloadData.results = response.data;
            payloadData.currentPage = page - 1;
            payloadData.maxPages = Math.ceil(recordCount / limit);
            payloadData.modelName = AbModelName;
            payloadData.recordCount = recordCount;
            payloadData.resultsPerPage = limit;
            payloadData.externalSortColumn = externalSortColumn;
            payloadData.externalSortAscending = externalSortAscending;
            payloadData.externalFilter = externalFilter;
            payloadData.advanceFilter = advanceFilter;
            payloadData.filters = filters;
            payloadData.includes = include;
            dispatch(hideLoading(AbModelName));
            if (loaderCallback) {
                loaderCallback('HIDE_LOADER');
            }
            if (dataSuccessCallback) {
                dataSuccessCallback(response);
            }
            dispatch(modelDataRequestSuccess(payloadData));
        }, function (error) {
            console.log("error", error);
            errorCallback(error);
            dispatch(hideLoading(AbModelName));
            if (loaderCallback) {
                loaderCallback('HIDE_LOADER');
            }
            return { result: "fail", data: error };
        });
    };
}

function showLoading(AbModelName) {
    return {
        type: _constants.SHOW_LOADER,
        payload: { AbModelName: AbModelName }
    };
}

function hideLoading(AbModelName) {
    return {
        type: _constants.HIDE_LOADER,
        payload: { AbModelName: AbModelName }
    };
}

function modelsRequest(configData) {
    var page = configData.page,
        resultsPerPage = configData.resultsPerPage,
        AbModelInstance = configData.AbModelInstance,
        AbModelName = configData.AbModelName,
        externalSortColumn = configData.externalSortColumn,
        externalSortAscending = configData.externalSortAscending,
        externalFilter = configData.externalFilter,
        filters = configData.filters,
        include = configData.include,
        customGridFilter = configData.customGridFilter,
        errorCallback = configData.errorCallback,
        loaderCallback = configData.loaderCallback,
        advanceFilter = configData.advanceFilter,
        showPager = configData.showPager,
        dataSuccessCallback = configData.dataSuccessCallback;

    page = page || 1;
    var recordCount = void 0;
    var skip = (page - 1) * resultsPerPage;
    var limit = resultsPerPage;
    var order = externalSortColumn;
    var sortOrder = externalSortAscending ? " ASC" : " DESC";
    var filterCols = _lodash2.default.map(filters, function (filterCol) {
        return _defineProperty({}, filterCol, { "regexp": "/" + externalFilter + "/i" });
    });
    var where = {
        "or": filterCols
    };
    order = order + sortOrder;
    return function (dispatch) {
        dispatch(showLoading(AbModelName));
        if (loaderCallback) {
            loaderCallback('SHOW_LOADER');
        }
        var countFilter = where.or.length ? where : {};
        if (customGridFilter) {
            countFilter = _extends({}, countFilter, customGridFilter);
        }
        if (advanceFilter) {
            countFilter = _extends({}, countFilter, advanceFilter);
        }
        AbModelInstance.count(countFilter).fetch().then(function (resp) {
            // if (resp.body().data() && resp.statusCode() === 200) {
            var recordCount = resp.count;
            if (showPager != null && showPager === false) {
                limit = recordCount;
            }
            var filter = void 0;
            if (include.length > 0) {
                filter = { order: order, limit: limit, skip: skip, where: countFilter, include: include };
            } else {
                filter = { order: order, limit: limit, skip: skip, where: countFilter };
            }
            // console.log("filter", JSON.stringify(filter));
            AbModelInstance.all(filter).fetch().then(function (response) {
                // if (Array.isArray(response.body()) && response.statusCode() === 200) {
                //     let actualData = _.map(response.body(), function (row) { return row.data() });
                var payloadData = {};
                payloadData.results = response;
                payloadData.currentPage = page - 1;
                payloadData.maxPages = Math.ceil(recordCount / limit);
                payloadData.modelName = AbModelName;
                payloadData.recordCount = recordCount;
                payloadData.resultsPerPage = limit;
                payloadData.externalSortColumn = externalSortColumn;
                payloadData.externalSortAscending = externalSortAscending;
                payloadData.externalFilter = externalFilter;
                payloadData.advanceFilter = advanceFilter;
                payloadData.filters = filters;
                payloadData.includes = include;
                dispatch(hideLoading(AbModelName));
                if (loaderCallback) {
                    loaderCallback('HIDE_LOADER');
                }
                if (dataSuccessCallback) {
                    dataSuccessCallback(response);
                }
                dispatch(modelDataRequestSuccess(payloadData));
                // }
            }, function (error) {
                console.log("error", error);
                errorCallback(error);
                dispatch(hideLoading(AbModelName));
                if (loaderCallback) {
                    loaderCallback('HIDE_LOADER');
                }
                return { result: "fail", data: error };
            });
        }, function (error) {
            errorCallback(error);
            dispatch(hideLoading(AbModelName));
            if (loaderCallback) {
                loaderCallback('HIDE_LOADER');
            }
            return { result: "fail", data: error };
        });
    };
}

function modelDataRequestSuccess(payloadData) {
    return {
        type: _constants.MODELS_DATA_REQUEST_SUCCESS,
        payload: payloadData
    };
}

function setModelInitialData(abModelName, initialConfigData) {
    return {
        type: _constants.SET_MODEL_INITIAL_STATE,
        payload: { modelName: abModelName, initialConfigData: initialConfigData }
    };
}

function setCurrentActiveItem(payloadData) {
    var currentActiveItem = payloadData.currentActiveItem,
        modelName = payloadData.modelName,
        action = payloadData.action;

    return {
        type: _constants.SET_CURRENT_ACTIVE_ITEM,
        payload: {
            modelName: modelName,
            form: {
                currentActiveItem: currentActiveItem,
                action: action
            }
        }
    };
}

function saveModelFormDataSuccess(payloadData) {
    return {
        type: _constants.SAVE_MODEL_FORM_DATA_SUCCESS,
        payload: payloadData

    };
}

//transformData flag used only for FormWrapper Hoc
//isFormWrapper flag sent from FormWrapper Hoc
function saveModelFormData(payloadData) {
    return function (dispatch) {
        var AbModelInstance = payloadData.AbModelInstance,
            formMetaData = payloadData.formMetaData,
            AbModelName = payloadData.AbModelName,
            saveCallback = payloadData.saveCallback,
            errorCallback = payloadData.errorCallback,
            dataSourceType = payloadData.dataSourceType,
            dataSourceOptions = payloadData.dataSourceOptions,
            transformFieldsDetail = payloadData.transformFieldsDetail,
            transformFlagDetail = payloadData.transformFlagDetail,
            _payloadData$transfor = payloadData.transformData,
            transformData = _payloadData$transfor === undefined ? true : _payloadData$transfor,
            _payloadData$isFormWr = payloadData.isFormWrapper,
            isFormWrapper = _payloadData$isFormWr === undefined ? false : _payloadData$isFormWr;
        var formData = formMetaData.formData,
            schema = formMetaData.schema;

        var finalDataToSave = {};

        if (!isFormWrapper) {
            var visbleFields = Object.keys(schema.properties);
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = visbleFields[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var fieldName = _step.value;

                    if (formData.hasOwnProperty(fieldName)) {
                        finalDataToSave[fieldName] = formData[fieldName];
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        } else {
            finalDataToSave = formData;
        }
        var transformInner = function transformInner(transformArray, transformedFormData) {
            _lodash2.default.map(_lodash2.default.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    // transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    // delete transformedFormData[fieldName];
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = _extends({}, transformedFormData, _lodash2.default.cloneDeep(transformedFormData[fieldName]));
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        var tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = _extends({}, _lodash2.default.cloneDeep(tempData));
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _lodash2.default.map(transformedFormData[fieldName], function (fieldObject) {
                        if (transformArray[fieldName]) {
                            transformInner(transformArray[fieldName], fieldObject);
                        }
                    });
                }
            });
            return transformedFormData;
        };
        var transformFieldsValue = function transformFieldsValue(transformArray, transformedFormData) {
            _lodash2.default.map(_lodash2.default.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = _extends({}, transformedFormData, _lodash2.default.cloneDeep(transformedFormData[fieldName]));
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        var tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = _extends({}, _lodash2.default.cloneDeep(tempData));
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _lodash2.default.map(transformedFormData[fieldName], function (fieldObject, index) {
                        if (transformArray[fieldName]) {
                            var _tempData = transformInner(transformArray[fieldName], fieldObject);
                            transformedFormData[fieldName][index] = _extends({}, _lodash2.default.cloneDeep(_tempData));
                        }
                    });
                }
            });
            return transformedFormData;
        };

        var finalDataSet = transformData ? transformFieldsValue(transformFieldsDetail, finalDataToSave) : finalDataToSave;
        finalDataSet = JSON.stringify(finalDataSet, function (k, v) {
            if (v === undefined) {
                return null;
            }return v;
        });
        finalDataSet = JSON.parse(finalDataSet);
        var dataSource = null;
        if (dataSourceType && dataSourceOptions) {
            var CREATE = dataSourceOptions.CREATE,
                EDIT = dataSourceOptions.EDIT;

            finalDataSet["id"] = formData.id;
            if (EDIT && EDIT.methodName) {
                var methodName = EDIT.methodName,
                    params = EDIT.params,
                    options = EDIT.options;

                dataSource = AbModelInstance.custom(methodName, finalDataSet, params, options);
            } else if (CREATE && CREATE.methodName) {
                var _methodName = EDIT.methodName,
                    _params = EDIT.params,
                    _options = EDIT.options;

                dataSource = AbModelInstance.custom(_methodName, finalDataSet, _params, _options);
            }

            // if (CREATE && CREATE.methodName) {
            //     const { methodName, params, options } = CREATE;
            //     dataSource = AbModelInstance.custom(methodName, formData, params, options)
            // }
        }
        if (!dataSource) {
            dataSource = AbModelInstance.one(formData.id).update(finalDataSet);
        }

        // let dataSource = null
        // if (dataSourceType && dataSourceOptions && dataSourceOptions.methodName) {
        //     const { methodName, params, options } = dataSourceOptions;
        //     finalDataToSave["id"] = formData.id;
        //     dataSource = AbModelInstance.custom(methodName, finalDataToSave, params, options)
        // } else {
        //     dataSource = AbModelInstance.one(formData.id).update(finalDataToSave)
        // }
        if (dataSource) {
            dataSource.then(function (resp) {
                if (resp) {
                    var newItemData = resp;
                    var _payloadData$includes = payloadData.includes,
                        includes = _payloadData$includes === undefined ? [] : _payloadData$includes;

                    var apiFilter = {};
                    if (includes.length > 0) {
                        apiFilter = { include: includes };
                    }
                    AbModelInstance.one(formData.id, apiFilter).fetch().then(function (response) {
                        if (response) {
                            dispatch(saveModelFormDataSuccess({ modelName: AbModelName, data: response, saveCallback: saveCallback }));
                            saveCallback("edit", response);
                        }
                    }, function (error) {
                        errorCallback("edit", error);
                        return { result: "fail", data: error };
                    });
                } else {
                    errorCallback("edit", { "statusCode": 200, "message": "Problem in updatind form data", "name": "AbModelForm" });
                    return { result: "fail", data: { "message": "Problem in updating" } };
                }
            }, function (error) {
                errorCallback("edit", error);
                return { result: "fail", data: error };
            });
        }
    };
}

function createModelFormDataSuccess(payloadData) {
    return {
        type: _constants.CREATE_MODEL_FORM_DATA_SUCCESS,
        payload: payloadData
    };
}

//transformData flag used only for FormWrapper Hoc
//isFormWrapper flag sent from FormWrapper Hoc
function createModelFormData(payloadData) {
    var AbModelInstance = payloadData.AbModelInstance,
        formMetaData = payloadData.formMetaData,
        AbModelName = payloadData.AbModelName,
        saveCallback = payloadData.saveCallback,
        errorCallback = payloadData.errorCallback,
        dataSourceType = payloadData.dataSourceType,
        dataSourceOptions = payloadData.dataSourceOptions,
        transformFieldsDetail = payloadData.transformFieldsDetail,
        transformFlagDetail = payloadData.transformFlagDetail,
        _payloadData$transfor2 = payloadData.transformData,
        transformData = _payloadData$transfor2 === undefined ? true : _payloadData$transfor2,
        _payloadData$isFormWr2 = payloadData.isFormWrapper,
        isFormWrapper = _payloadData$isFormWr2 === undefined ? false : _payloadData$isFormWr2;
    var formData = formMetaData.formData,
        schema = formMetaData.schema;

    var finalDataToSave = {};
    if (!isFormWrapper) {
        var visbleFields = Object.keys(schema.properties);
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = visbleFields[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var fieldName = _step2.value;

                if (formData.hasOwnProperty(fieldName)) {
                    finalDataToSave[fieldName] = formData[fieldName];
                }
            }
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }
    } else {
        finalDataToSave = formData;
    }

    return function (dispatch) {
        var transformInner = function transformInner(transformArray, transformedFormData) {
            _lodash2.default.map(_lodash2.default.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    // transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    // delete transformedFormData[fieldName];
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = _extends({}, transformedFormData, _lodash2.default.cloneDeep(transformedFormData[fieldName]));
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        var tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = _extends({}, _lodash2.default.cloneDeep(tempData));
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _lodash2.default.map(transformedFormData[fieldName], function (fieldObject) {
                        if (transformArray[fieldName]) {
                            transformInner(transformArray[fieldName], fieldObject);
                        }
                    });
                }
            });
            return transformedFormData;
        };

        var transformFieldsValue = function transformFieldsValue(transformArray, transformedFormData) {
            _lodash2.default.map(_lodash2.default.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = _extends({}, transformedFormData, _lodash2.default.cloneDeep(transformedFormData[fieldName]));
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        var tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = _extends({}, _lodash2.default.cloneDeep(tempData));
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _lodash2.default.map(transformedFormData[fieldName], function (fieldObject, index) {
                        if (transformArray[fieldName]) {
                            var _tempData2 = transformInner(transformArray[fieldName], fieldObject);
                            transformedFormData[fieldName][index] = _extends({}, _lodash2.default.cloneDeep(_tempData2));
                        }
                    });
                }
            });
            return transformedFormData;
        };

        var finalDataSet = transformData ? transformFieldsValue(transformFieldsDetail, finalDataToSave) : finalDataToSave;
        finalDataSet = JSON.stringify(finalDataSet, function (k, v) {
            if (v === undefined) {
                return null;
            }return v;
        });
        finalDataSet = JSON.parse(finalDataSet);
        var dataSource = null;
        if (dataSourceOptions) {
            var CREATE = dataSourceOptions.CREATE;

            if (CREATE && CREATE.methodName) {
                var methodName = CREATE.methodName,
                    params = CREATE.params,
                    options = CREATE.options;

                dataSource = AbModelInstance.custom(methodName, finalDataSet, params, options);
            }
        }
        if (!dataSource) {
            dataSource = AbModelInstance.create(finalDataSet);
        }
        if (dataSource) {
            dataSource.then(function (response) {
                if (response) {
                    dispatch(createModelFormDataSuccess({ modelName: AbModelName, data: response, saveCallback: saveCallback }));
                    saveCallback("create", response);
                }
            }, function (error) {
                console.log("error", error);
                errorCallback("create", error);
                return { result: "fail", data: error };
            });
        }
    };
}

function clearFormData(payloadData) {
    return {
        type: _constants.CLEAR_FORM_DATA,
        payload: payloadData
    };
}

function getSelectModelData(payload) {
    var AbModelInstance = payload.AbModelInstance,
        filter = payload.filter;

    return function (dispatch) {
        return AbModelInstance.all(filter).fetch();
    };
}

function mergeGridConfig(payloadData) {
    return {
        type: _constants.MERGE_GRID_CONFIG,
        payload: payloadData
    };
}

function DeleteGridRow(payloadData) {
    var id = payloadData.id,
        AbModelInstance = payloadData.AbModelInstance,
        data = payloadData.data,
        AbModelName = payloadData.AbModelName,
        deleteCallback = payloadData.deleteCallback,
        errorCallback = payloadData.errorCallback,
        abModelData = payloadData.abModelData,
        gridReloadConfigData = payloadData.gridReloadConfigData,
        dataSourceType = payloadData.dataSourceType;

    if (data.id) {
        return function (dispatch) {
            AbModelInstance.one(data.id).delete().then(function (response) {
                if (response) {
                    if (deleteCallback) {
                        deleteCallback(response);
                    }
                    if (dataSourceType && dataSourceType === _constants.GRID_DATA_SOURCE_TYPE.METHOD) {
                        return dispatch(customMethodRequest(gridReloadConfigData));
                    } else {
                        return dispatch(modelsRequest(gridReloadConfigData));
                    }
                }
            }, function (error) {
                console.log("error", error);
                if (errorCallback) {
                    errorCallback(error);
                }
                return { result: "fail", data: error };
            });
        };
    } else {
        errorCallback({ message: "Problem in deleting data, Id missing." });
    }
}

function getPreLoadFormData(payloadData) {
    var AbModelInstance = payloadData.AbModelInstance,
        filter = payloadData.filter,
        AbModelName = payloadData.AbModelName,
        saveCallback = payloadData.saveCallback,
        errorCallback = payloadData.errorCallback,
        dataSourceType = payloadData.dataSourceType,
        dataSourceOptions = payloadData.dataSourceOptions,
        transformSelectDetail = payloadData.transformSelectDetail;

    var transformSelectValue = function transformSelectValue(transformArray, dataObject) {
        _lodash2.default.map(_lodash2.default.uniq(transformArray), function (fieldName) {
            if (dataObject && dataObject.constructor === Array) {
                _lodash2.default.map(dataObject, function (data) {
                    if (data && data[fieldName]) {
                        if (transformArray[fieldName]) {
                            transformSelectValue(transformArray[fieldName], data[fieldName]);
                        } else {
                            if (data[fieldName].constructor === Array) {
                                data[fieldName] = data[fieldName].join(",");
                            }
                        }
                    }
                });
            } else if (dataObject && dataObject.constructor === Object) {

                if (dataObject && dataObject[fieldName]) {
                    if (transformArray[fieldName]) {
                        transformSelectValue(transformArray[fieldName], dataObject[fieldName]);
                    } else {
                        if (dataObject[fieldName].constructor === Array) {
                            dataObject[fieldName] = dataObject[fieldName].join(",");
                        }
                    }
                }
            }
        });
    };
    if (filter && filter.id) {
        return function (dispatch) {
            var dataSource = null;
            if (dataSourceType && dataSourceOptions && dataSourceOptions.methodName) {
                var methodName = dataSourceOptions.methodName,
                    data = dataSourceOptions.data,
                    params = dataSourceOptions.params,
                    options = dataSourceOptions.options;

                dataSource = AbModelInstance.custom(methodName, data, _extends({}, filter), options);
            } else {
                dataSource = AbModelInstance.one(filter.id).fetch();
            }

            if (dataSource) {
                dataSource.then(function (response) {
                    if (response) {
                        if (saveCallback) {
                            saveCallback("preload", response);
                        }

                        if (transformSelectDetail && transformSelectDetail.length > 0) {
                            transformSelectValue(transformSelectDetail, response);
                        }
                        return dispatch(setCurrentActiveItem({ currentActiveItem: response, modelName: AbModelName, action: _constants.ACTIONBUTTON.EDIT }));
                    } else {
                        if (errorCallback) {
                            errorCallback("preload", "No preload Data Found");
                        }
                    }
                }, function (error) {
                    console.log("error", error);
                    if (errorCallback) {
                        errorCallback("preload", error);
                    }
                });
            }
        };
    } else {
        return true;
    }
}

function resetGridDataOnly(abModelName) {
    return {
        type: _constants.RESET_GRID_DATA_ONLY,
        payload: { modelName: abModelName }
    };
}

function importFile(payloadData) {
    return function (dispatch) {
        var file = payloadData.file,
            importUrl = payloadData.importUrl,
            successCallback = payloadData.successCallback,
            errorCallback = payloadData.errorCallback;


        var formData = new FormData();
        formData.append("file", file);
        // formData.append("token", responseData.token);
        var xhr = new XMLHttpRequest();
        // xhr.withCredentials = true;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200 || xhr.status === 201) {
                    // Successfully uploaded the file.
                    console.log("success", xhr);
                    if (successCallback) {
                        successCallback(JSON.parse(xhr.responseText));
                    }
                } else {
                    // The file could not be uploaded.
                    console.log("Error", xhr);
                    if (errorCallback) {
                        errorCallback(JSON.parse(xhr.responseText).error);
                    }
                }
            }
        };
        xhr.upload.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
                console.log("add upload event-listener" + evt.loaded + "/" + evt.total);
            }
        }, false);
        xhr.open("POST", importUrl);
        xhr.send(formData);
    };
}

function exportGridData(data) {
    var page = data.page;
    var modelName = data.modelName,
        config = data.config,
        format = data.format,
        resultsPerPage = data.resultsPerPage,
        externalSortColumn = data.externalSortColumn,
        externalSortAscending = data.externalSortAscending,
        externalFilter = data.externalFilter,
        filters = data.filters,
        include = data.include,
        customGridFilter = data.customGridFilter,
        advanceFilter = data.advanceFilter,
        exportStartCallback = data.exportStartCallback,
        exportFinishCallback = data.exportFinishCallback,
        exportErrorCallback = data.exportErrorCallback,
        type = data.type,
        dataSourceOptions = data.dataSourceOptions;

    exportStartCallback();
    var filter = {};
    var fileName = config.fileName ? config.fileName + '.xlsx' : 'report.xlsx';
    var exportUrl = config.apiUrl + "/" + modelName;
    if (dataSourceOptions && dataSourceOptions.methodName) {
        exportUrl += "/" + dataSourceOptions.methodName;
    }

    page = page || 1;
    var recordCount = void 0;
    var skip = (page - 1) * resultsPerPage;
    var limit = resultsPerPage;
    var order = externalSortColumn;
    var sortOrder = externalSortAscending ? " ASC" : " DESC";
    var filterCols = _lodash2.default.map(filters, function (filterCol) {
        return _defineProperty({}, filterCol, { "regexp": "/" + externalFilter + "/i" });
    });
    var where = {
        "or": filterCols
    };
    order = order + sortOrder;
    var countFilter = where.or.length ? where : {};
    if (customGridFilter) {
        countFilter = _extends({}, countFilter, customGridFilter);
    }
    if (advanceFilter) {
        countFilter = _extends({}, countFilter, advanceFilter);
    }
    filter = { order: order, where: countFilter };
    if (type != "ALL") {
        filter = Object.assign(filter, { skip: skip, limit: limit });
    }
    if (include.length > 0) {
        filter = Object.assign(filter, { include: include });
    }

    return function (dispatch) {
        var body = {
            "config": config.configObj,
            "api": {
                "url": exportUrl,
                "method": "GET",
                "qs": {
                    "filter": filter
                },
                "expression": config.expression
            }
        };
        if (dataSourceOptions && !_lodash2.default.isEmpty(dataSourceOptions.params)) {
            body.api.qs = _lodash2.default.assignIn(body.api.qs, dataSourceOptions.params);
        }

        fetch(config.apiUrl + '/service/export/' + format, {
            method: 'POST',
            headers: {
                'Authorization': config.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        }).then(function (response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            if (format === 'excel') {
                return response.blob(); //returns excel file blob
            }
            return response.json(); //returns url json of pdf file
        }).then(function (responseData) {
            console.log("exportGridData responseData: ", responseData);
            if (format === 'excel') {
                var link = document.createElement("a");
                link.href = window.URL.createObjectURL(responseData);
                document.body.appendChild(link);
                link.download = fileName;
                link.click();
                document.body.removeChild(link);
                exportFinishCallback();
            } else {
                var _link = document.createElement("a");
                _link.href = responseData.message;
                _link.target = "_blank";
                document.body.appendChild(_link);
                _link.click();
                document.body.removeChild(_link);
                exportFinishCallback();
            }
        }).catch(function (error) {
            console.log("exportGridData error: ", error);
            if (exportErrorCallback) {
                exportErrorCallback(error);
            }
        });
    };
}
// export const actions = {
//     modelsRequest,
//     modelDataRequestSuccess,
//     setModelInitialData,
//     setCurrentActiveItem,
//     saveModelFormData,
//     saveModelFormDataSuccess,
//     createModelFormData
// }