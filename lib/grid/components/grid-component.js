'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

var _jbGriddleReact = require('jb-griddle-react');

var _jbGriddleReact2 = _interopRequireDefault(_jbGriddleReact);

var _constants = require('../../constants');

var _gridRenderer = require('../../grid-renderer');

var _underscore = require('underscore');

var _underscore2 = _interopRequireDefault(_underscore);

require('./grid.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AbModelGrid = function (_Component) {
    _inherits(AbModelGrid, _Component);

    function AbModelGrid(props) {
        _classCallCheck(this, AbModelGrid);

        var _this = _possibleConstructorReturn(this, (AbModelGrid.__proto__ || Object.getPrototypeOf(AbModelGrid)).call(this, props));

        _this.customFilterFunction = _this.customFilterFunction.bind(_this);
        _this.setPage = _this.setPage.bind(_this);
        var onButtonClick = function onButtonClick(actionType, data, callback) {
            if (_constants.ACTIONBUTTON.DELETE === actionType) {
                var _this$props$pageSize = _this.props.pageSize,
                    pageSize = _this$props$pageSize === undefined ? null : _this$props$pageSize;
                var _this$props$abModelDa = _this.props.abModelData,
                    currentPage = _this$props$abModelDa.currentPage,
                    resultsPerPage = _this$props$abModelDa.resultsPerPage,
                    externalSortColumn = _this$props$abModelDa.externalSortColumn,
                    externalSortAscending = _this$props$abModelDa.externalSortAscending,
                    externalFilter = _this$props$abModelDa.externalFilter,
                    advanceFilter = _this$props$abModelDa.advanceFilter;

                var gridReloadConfigData = {
                    page: currentPage,
                    resultsPerPage: pageSize ? pageSize : resultsPerPage,
                    externalSortColumn: externalSortColumn,
                    externalSortAscending: externalSortAscending,
                    externalFilter: externalFilter,
                    AbModelInstance: props.AbModelInstance,
                    filters: _this.state.filterColumn,
                    include: _this.state.includeModels,
                    advanceFilter: advanceFilter
                };
                props.deleteGridRow({ data: data, gridReloadConfigData: gridReloadConfigData });
            } else {

                var a = props.setCurrentActiveItem(actionType, data);
                if (callback) {
                    callback(actionType, data);
                }
            }
        };
        var advanceFilterColumnNames = _underscore2.default.chain(props.columnMetadata).filter(function (colMetaData) {
            return colMetaData.advanceFilter ? true : false;
        }).map(function (colMetaData) {
            var colFilterData = colMetaData.advanceFilterOptions;
            colFilterData["columnName"] = colMetaData["columnName"];
            return colFilterData;
        }).value();
        var filters = _underscore2.default.chain(props.columnMetadata).filter(function (colMetaData) {
            return colMetaData.filter ? true : false;
        }).pluck("columnName").value();
        var includeModels = _underscore2.default.chain(props.columnMetadata).filter(function (colMetaData) {
            return colMetaData.includeModel ? true : false;
        }).pluck("includeModel").uniq().value();
        var external = void 0;
        var modelCols = [];
        if (props.modelCols) {
            modelCols = props.modelCols;
        } else {
            modelCols = _underscore2.default.pluck(props.columnMetadata, "columnName");
        }
        var columnMetadata = _underscore2.default.map(props.columnMetadata, function (colMetaData) {
            if (!colMetaData.customComponent) {
                if (colMetaData.type === _constants.COLUMNTYPE.LINK) {
                    colMetaData["customComponent"] = function (props) {
                        return (0, _gridRenderer.LinkRenderer)(props);
                    };
                } else if (colMetaData.type === _constants.COLUMNTYPE.ACTIONS) {
                    colMetaData["customComponent"] = function (props) {
                        return (0, _gridRenderer.actionComponent)(props, onButtonClick);
                    };
                } else {
                    colMetaData["customComponent"] = function (props) {
                        return (0, _gridRenderer.dataRenderer)(props);
                    };
                }
            }
            return colMetaData;
        });

        _this.state = {
            firstTimeLoad: true,
            filterColumn: filters,
            includeModels: includeModels,
            modelCols: modelCols,
            columnMetadata: props.columnMetadata,
            advanceFilterConfig: advanceFilterColumnNames,
            currentPage: 0
        };
        return _this;
    }

    _createClass(AbModelGrid, [{
        key: 'getChildContext',
        value: function getChildContext() {
            return { modelName: this.props.modelName };
        }
    }, {
        key: 'componentWillMount',
        value: function componentWillMount() {
            var setAdvanceFilterConfig = this.props.setAdvanceFilterConfig;
            var advanceFilterConfig = this.state.advanceFilterConfig;

            setAdvanceFilterConfig({ advanceFilterConfig: advanceFilterConfig });
            if (this.state.firstTimeLoad && this.props.showFilter === false && this.props.abModelData) {
                this.getExternalData();
            }
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            this.props.resetGridData();
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            var oldCustomGridFilter = this.props.customGridFilter;
            var newCustomGridFilter = nextProps.customGridFilter;
            if (!_underscore2.default.isEqual(oldCustomGridFilter, newCustomGridFilter)) {
                this.setFilter(nextProps.externalFilter);
            }
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps, prevState) {
            if (this.state.firstTimeLoad && this.props.showFilter === false && this.props.abModelData) {
                this.getExternalData();
            }
        }
    }, {
        key: 'onRowClick',
        value: function onRowClick(row, event) {
            // console.log(row.props.data);
        }
    }, {
        key: 'getExternalData',
        value: function getExternalData(page) {
            var _props = this.props,
                getModels = _props.getModels,
                _props$pageSize = _props.pageSize,
                pageSize = _props$pageSize === undefined ? null : _props$pageSize;
            var _props$abModelData = this.props.abModelData,
                resultsPerPage = _props$abModelData.resultsPerPage,
                externalFilter = _props$abModelData.externalFilter,
                advanceFilter = _props$abModelData.advanceFilter,
                externalSortColumn = _props$abModelData.externalSortColumn,
                externalSortAscending = _props$abModelData.externalSortAscending;

            var configData = {
                page: page,
                resultsPerPage: pageSize ? pageSize : resultsPerPage,
                externalSortColumn: this.props.externalSortColumn != null && this.state.firstTimeLoad ? this.props.externalSortColumn : externalSortColumn,
                externalSortAscending: this.props.externalSortAscending != null && this.state.firstTimeLoad ? this.props.externalSortAscending : externalSortAscending,
                externalFilter: externalFilter,
                AbModelInstance: this.props.AbModelInstance,
                filters: this.state.filterColumn,
                include: this.state.includeModels,
                advanceFilter: advanceFilter
            };
            getModels(configData);
            if (this.state.firstTimeLoad) {
                this.setState({ firstTimeLoad: false });
            }
        }
    }, {
        key: 'setPageSize',
        value: function setPageSize(size) {
            var getModels = this.props.getModels;
            var _props$abModelData2 = this.props.abModelData,
                externalSortColumn = _props$abModelData2.externalSortColumn,
                externalSortAscending = _props$abModelData2.externalSortAscending,
                externalFilter = _props$abModelData2.externalFilter,
                advanceFilter = _props$abModelData2.advanceFilter;

            var configData = {
                page: 0,
                resultsPerPage: size,
                externalSortColumn: externalSortColumn,
                externalSortAscending: externalSortAscending,
                externalFilter: externalFilter,
                AbModelInstance: this.props.AbModelInstance,
                filters: this.state.filterColumn,
                include: this.state.includeModels,
                advanceFilter: advanceFilter
            };
            getModels(configData);
        }
    }, {
        key: 'changeSort',
        value: function changeSort(sort, sortAscending) {
            var getModels = this.props.getModels;
            var _props$abModelData3 = this.props.abModelData,
                resultsPerPage = _props$abModelData3.resultsPerPage,
                externalFilter = _props$abModelData3.externalFilter,
                advanceFilter = _props$abModelData3.advanceFilter;

            var configData = {
                page: 0,
                resultsPerPage: resultsPerPage,
                externalSortColumn: sort,
                externalSortAscending: sortAscending,
                externalFilter: externalFilter,
                AbModelInstance: this.props.AbModelInstance,
                filters: this.state.filterColumn,
                include: this.state.includeModels,
                advanceFilter: advanceFilter
            };
            getModels(configData);
            this.setState({ currentPage: 0 });
        }
    }, {
        key: 'setFilter',
        value: function setFilter(filterData) {
            var _props2 = this.props,
                getModels = _props2.getModels,
                _props2$pageSize = _props2.pageSize,
                pageSize = _props2$pageSize === undefined ? null : _props2$pageSize;
            var _props$abModelData4 = this.props.abModelData,
                resultsPerPage = _props$abModelData4.resultsPerPage,
                externalSortColumn = _props$abModelData4.externalSortColumn,
                externalSortAscending = _props$abModelData4.externalSortAscending,
                externalFilter = _props$abModelData4.externalFilter,
                advanceFilter = _props$abModelData4.advanceFilter;

            var configData = {
                page: 0,
                resultsPerPage: pageSize ? pageSize : resultsPerPage,
                externalSortColumn: this.props.externalSortColumn != null && this.state.firstTimeLoad ? this.props.externalSortColumn : externalSortColumn,
                externalSortAscending: this.props.externalSortAscending != null && this.state.firstTimeLoad ? this.props.externalSortAscending : externalSortAscending,
                // externalFilter: filterData,
                AbModelInstance: this.props.AbModelInstance,
                filters: this.state.filterColumn,
                include: this.state.includeModels
            };
            if (typeof filterData === "string") {
                configData["externalFilter"] = filterData;
                configData["advanceFilter"] = advanceFilter;
                var searchFilterPattern = new RegExp("^[a-zA-Z0-9  \b]+$");
                if (!searchFilterPattern.test(filterData) && filterData.length > 0) {
                    return false;
                }
            } else {
                configData["externalFilter"] = externalFilter;
                configData["advanceFilter"] = filterData;
            }
            getModels(configData);
            this.setState({ currentPage: 0 });
        }
    }, {
        key: 'customFilterFunction',
        value: function customFilterFunction(items, query) {
            return _underscore2.default.filter(items, function (item) {
                var flat = squish(item);

                for (var key in flat) {
                    if (String(flat[key]).toLowerCase().indexOf(query.value.toLowerCase()) >= 0) return true;
                };
                return false;
            });
        }
    }, {
        key: 'setPage',
        value: function setPage(index) {
            index = index > this.props.maxPages ? this.props.maxPages : index < 1 ? 1 : index + 1;
            this.getExternalData(index);
            this.setState({ currentPage: index });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var self = this;
            var _props3 = this.props,
                showFilter = _props3.showFilter,
                showSettings = _props3.showSettings,
                colsNotToDisplayInSetting = _props3.colsNotToDisplayInSetting,
                customRowComponent = _props3.customRowComponent,
                customNoDataComponent = _props3.customNoDataComponent,
                showPager = _props3.showPager,
                pageSize = _props3.pageSize,
                showLoader = _props3.showLoader,
                exportData = _props3.exportData,
                exportConfig = _props3.exportConfig,
                modelName = _props3.modelName,
                exportGridData = _props3.exportGridData,
                AbModelInstance = _props3.AbModelInstance;
            var _state = this.state,
                modelCols = _state.modelCols,
                columnMetadata = _state.columnMetadata,
                advanceFilterConfig = _state.advanceFilterConfig;


            if (!this.props.abModelData) {
                return null;
            }
            var resultsPerPage = this.props.abModelData.resultsPerPage;
            var _props$abModelData5 = this.props.abModelData,
                maxPages = _props$abModelData5.maxPages,
                results = _props$abModelData5.results,
                currentPage = _props$abModelData5.currentPage,
                externalSortColumn = _props$abModelData5.externalSortColumn,
                externalSortAscending = _props$abModelData5.externalSortAscending,
                externalFilter = _props$abModelData5.externalFilter,
                advanceFilter = _props$abModelData5.advanceFilter;

            var useCustomRowComponent = false;
            if (customRowComponent) {
                useCustomRowComponent = true;
            }
            var showfooter = showPager;
            if (results && results.length <= 0 && useCustomRowComponent === true) {
                showfooter = false;
                useCustomRowComponent = false;
            }

            var LoadingComponent = function LoadingComponent() {
                return _react2.default.createElement(_semanticUiReact.Segment, { basic: true, loading: true });
            };

            var configData = {
                page: this.state.currentPage,
                resultsPerPage: pageSize ? pageSize : resultsPerPage,
                externalSortColumn: this.props.externalSortColumn != null ? this.props.externalSortColumn : externalSortColumn,
                externalSortAscending: this.props.externalSortAscending != null ? this.props.externalSortAscending : externalSortAscending,
                externalFilter: externalFilter,
                filters: this.state.filterColumn,
                include: this.state.includeModels,
                advanceFilter: advanceFilter
            };

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                    _semanticUiReact.Grid,
                    null,
                    _react2.default.createElement(
                        _semanticUiReact.Grid.Row,
                        { columns: 1 },
                        _react2.default.createElement(
                            _semanticUiReact.Grid.Column,
                            null,
                            _react2.default.createElement(_jbGriddleReact2.default, { useExternal: true, externalSetPage: this.setPage, enableSort: true, useGriddleStyles: false, tableClassName: 'ui compact celled table',
                                columns: modelCols, columnMetadata: columnMetadata, externalLoadingComponent: LoadingComponent, externalIsLoading: showLoader,
                                useCustomFilterer: true, customFilterer: this.customFilterFunction,
                                useCustomFilterComponent: true, customFilterComponent: _gridRenderer.OtherFilter, showPager: showfooter,
                                useCustomPagerComponent: true, customPagerComponent: function customPagerComponent(props) {
                                    return (0, _gridRenderer.OtherPager)(_extends({}, props, self.props, { exportData: exportData, config: exportConfig, modelName: modelName, exportGridData: exportGridData, AbModelInstance: AbModelInstance }, configData));
                                },
                                externalSetPageSize: function externalSetPageSize(size) {
                                    return _this2.setPageSize(size);
                                }, externalMaxPage: maxPages,
                                externalCurrentPage: currentPage, results: results, resultsPerPage: pageSize ? pageSize : resultsPerPage,
                                showSettings: showSettings, showFilter: showFilter, externalChangeSort: function externalChangeSort(sort, sortAscending) {
                                    return _this2.changeSort(sort, sortAscending);
                                },
                                externalSortColumn: externalSortColumn, externalSortAscending: externalSortAscending, externalSetFilter: function externalSetFilter(filter) {
                                    return _this2.setFilter(filter);
                                },
                                metadataColumns: colsNotToDisplayInSetting, useCustomRowComponent: useCustomRowComponent, customRowComponent: customRowComponent, customNoDataComponent: customNoDataComponent })
                        )
                    )
                )
            );
        }
    }]);

    return AbModelGrid;
}(_react.Component);

exports.default = AbModelGrid;


AbModelGrid.propTypes = {
    modelName: _react.PropTypes.string.isRequired,
    showSettings: _react.PropTypes.bool,
    showPager: _react.PropTypes.bool,
    showFilter: _react.PropTypes.bool,
    columnMetadata: _react.PropTypes.array.isRequired,
    modelCols: _react.PropTypes.arrayOf(_react.PropTypes.string),
    colsNotToDisplayInSetting: _react.PropTypes.arrayOf(_react.PropTypes.string),
    appbackApi: _react.PropTypes.object.isRequired,
    customGridFilter: _react.PropTypes.object,
    deleteCallback: _react.PropTypes.func,
    errorCallback: _react.PropTypes.func.isRequired,
    loaderCallback: _react.PropTypes.func,
    dataSuccessCallback: _react.PropTypes.func,
    customRowComponent: _react.PropTypes.func,
    customNoDataComponent: _react.PropTypes.func,
    pageSize: _react.PropTypes.number,
    externalSortColumn: _react.PropTypes.string,
    externalSortAscending: _react.PropTypes.bool,
    dataSourceType: function dataSourceType(props, propName, componentName) {
        var dataSourceType = props.dataSourceType,
            dataSourceOptions = props.dataSourceOptions;

        if (dataSourceType === _constants.GRID_DATA_SOURCE_TYPE.METHOD) {
            if (!dataSourceOptions) {
                return new Error('Provide dataSourceOptions props when dataSourceOptions is GRID_DATA_SOURCE_TYPE.METHOD');
            }
        }
    },
    dataSourceOptions: _react2.default.PropTypes.shape({
        methodName: _react2.default.PropTypes.string.isRequired,
        data: _react2.default.PropTypes.object.isRequired,
        params: _react2.default.PropTypes.object.isRequired,
        options: _react2.default.PropTypes.object.isRequired
    }),
    exportData: _react.PropTypes.bool,
    exportConfig: _react.PropTypes.object,
    exportStartCallback: _react.PropTypes.func,
    exportFinishCallback: _react.PropTypes.func
};

AbModelGrid.defaultProps = {
    showSettings: false,
    showPager: true,
    showFilter: true,
    colsNotToDisplayInSetting: [],
    customGridFilter: null,
    dataSourceType: _constants.GRID_DATA_SOURCE_TYPE.MODEL,
    customRowComponent: null,
    externalSortColumn: null,
    externalSortAscending: null,
    exportData: false,
    exportConfig: null
};

AbModelGrid.childContextTypes = {
    modelName: _react.PropTypes.string
};