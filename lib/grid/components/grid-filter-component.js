'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

require('react-select/dist/react-select.css');

var _semanticUiReact = require('semantic-ui-react');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GridFilterComponent = function (_React$Component) {
    _inherits(GridFilterComponent, _React$Component);

    function GridFilterComponent(props) {
        _classCallCheck(this, GridFilterComponent);

        var _this = _possibleConstructorReturn(this, (GridFilterComponent.__proto__ || Object.getPrototypeOf(GridFilterComponent)).call(this, props));

        _this.searchChange = function (e, filter) {
            var changeFilter = _this.props.changeFilter;

            changeFilter(filter.value);
            // this.setState({ commonFilter: filter.value });
        };

        _this.onFilterClick = function (e, data) {
            var advancedFilterObj = {};
            var changeFilter = _this.props.changeFilter;
            var results = _this.state.results;

            var filters = results.map(function (filterCol, i) {
                if (filterCol) {
                    var multiSelect = filterCol.multiSelect,
                        columnName = filterCol.columnName,
                        value = filterCol.value;

                    if (value && value.length > 0) {
                        if (multiSelect) {
                            advancedFilterObj[columnName] = {
                                "inq": value.split(",")
                            };
                        } else {
                            advancedFilterObj[columnName] = value;
                        }
                    }
                }
            });
            changeFilter(advancedFilterObj);
        };

        _this.state = {
            "results": []
        };
        return _this;
    }

    _createClass(GridFilterComponent, [{
        key: 'componentWillMount',
        value: function componentWillMount() {
            var abModelData = this.props.abModelData;
            var modelName = this.context.modelName;

            if (abModelData && modelName) {
                var intialFilterConfig = abModelData[modelName]["advanceFilterConfig"];
                if (intialFilterConfig) {
                    this.setState({
                        results: intialFilterConfig
                    });
                }
            }
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.onFilterClick();
        }
    }, {
        key: 'onChange',
        value: function onChange(val, index, advanceFilter) {
            var tempResults = this.state.results;
            if (!tempResults[index]) {
                advanceFilter["value"] = val;
                tempResults[index] = advanceFilter;
            } else {
                tempResults[index]["value"] = val;
            }
            this.setState({
                results: [].concat(_toConsumableArray(tempResults))
            });
            this.onFilterClick();
        }
    }, {
        key: 'render',
        value: function render() {
            var self = this;
            var _props = this.props,
                externalFilter = _props.externalFilter,
                abModelData = _props.abModelData;

            var advanceFilter = abModelData[this.context.modelName]["advanceFilterConfig"];
            var _state = this.state,
                results = _state.results,
                commonFilter = _state.commonFilter;

            return _react2.default.createElement(
                _semanticUiReact.Grid,
                { stackable: true, style: { "marginBottom": 5 } },
                _react2.default.createElement(
                    _semanticUiReact.Grid.Row,
                    { columns: 3 },
                    _react2.default.createElement(
                        _semanticUiReact.Grid.Column,
                        null,
                        _react2.default.createElement(_semanticUiReact.Input, { fluid: true, icon: 'search', placeholder: 'Search...', onChange: self.searchChange.bind(this), value: externalFilter })
                    ),
                    advanceFilter && advanceFilter.length > 0 ? advanceFilter.map(function (filter, i) {
                        return _react2.default.createElement(
                            _semanticUiReact.Grid.Column,
                            { key: i },
                            _react2.default.createElement(_reactSelect2.default, { multi: filter.multiSelect, clearable: filter.clearable, placeholder: filter.placeholder, options: filter.dataSet, simpleValue: true, valueKey: filter.valueKey, labelKey: filter.labelKey, onChange: function onChange(val) {
                                    self.onChange(val, i, filter);filter.onChange && filter.onChange(val);
                                }, value: results && results[i] ? results[i].value : null })
                        );
                    }) : null
                )
            );
        }
    }]);

    return GridFilterComponent;
}(_react2.default.Component);

GridFilterComponent.contextTypes = {
    modelName: _react.PropTypes.string
};
exports.default = GridFilterComponent;

// {/*<Grid.Row>
//     {advanceFilter.map((filter, i) =>
//         <Grid.Column width={6} key={i}>
//             <Select multi={true} options={filter.dataSet} simpleValue clearable={true} valueKey="value" labelKey="label" onChange={(val) => self.onChange(val, i)} value={results[i]} />
//         </Grid.Column>
//     )}
// </Grid.Row>   */}


/*
    render() {
        let self = this;
        let { externalFilter, abModelData} = this.props;
        let advanceFilter = abModelData[this.context.modelName]["advanceFilterConfig"];
        let {results, commonFilter} = this.state;
        return (
            <Grid columns='equal' style={{ "marginBottom": 5 }}>
                <Grid.Row>
                    <Grid.Column width={3}>
                        <Input icon='search' placeholder='Search...' onChange={self.searchChange.bind(this)} value={externalFilter} />
                    </Grid.Column>
                </Grid.Row>
                {(advanceFilter && advanceFilter.length > 0) ? (<Grid.Row>
                    <Grid.Column>
                        <Segment>
                            <Label attached='top'>Advance Filter</Label>
                            <Grid columns='equal'>
                                <Grid.Row>
                                    {advanceFilter.map((filter, i) =>
                                        <Grid.Column width={6} key={i}>
                                            <Select multi={filter.multiSelect} clearable={filter.clearable} placeholder={filter.placeholder} options={filter.dataSet} simpleValue clearable={true} valueKey={filter.valueKey} labelKey={filter.labelKey} onChange={(val) => self.onChange(val, i, filter)} value={(results && results[i]) ? results[i].value : null} />
                                        </Grid.Column>
                                    )}
                                    <Grid.Column>
                                        <Button content='Filter' icon='filter' labelPosition='left' onClick={(e) => self.onFilterClick(e)} />
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>) : null}
            </Grid>
        )
    }*/