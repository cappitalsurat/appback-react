'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _gridFilterComponent = require('../components/grid-filter-component');

var _gridFilterComponent2 = _interopRequireDefault(_gridFilterComponent);

var _actions = require('../../actions');

var AbModelGridAction = _interopRequireWildcard(_actions);

var _constants = require('../../constants');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state, ownProps) {
  var appbackApi = ownProps.appbackApi,
      modelName = ownProps.modelName;

  var mapped = {
    abModelData: state.abModels
  };
  return mapped;
};
function mapDispatchToProps(dispatch, ownProps) {
  return {};
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_gridFilterComponent2.default);