'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _gridComponent = require('../components/grid-component');

var _gridComponent2 = _interopRequireDefault(_gridComponent);

var _actions = require('../../actions');

var AbModelGridAction = _interopRequireWildcard(_actions);

var _constants = require('../../constants');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state, ownProps) {
  var appbackApi = ownProps.appbackApi,
      modelName = ownProps.modelName;

  var mapped = {
    abModelData: state.abModels[modelName],
    AbModelInstance: appbackApi.createModelRest(modelName, null),
    showLoader: state.abModels[modelName] ? state.abModels[modelName].showLoader : false
  };
  return mapped;
};
function mapDispatchToProps(dispatch, ownProps) {
  var modelName = ownProps.modelName,
      dataSourceType = ownProps.dataSourceType,
      dataSourceOptions = ownProps.dataSourceOptions,
      customGridFilter = ownProps.customGridFilter,
      _ownProps$errorCallba = ownProps.errorCallback,
      errorCallback = _ownProps$errorCallba === undefined ? null : _ownProps$errorCallba,
      _ownProps$deleteCallb = ownProps.deleteCallback,
      deleteCallback = _ownProps$deleteCallb === undefined ? null : _ownProps$deleteCallb,
      _ownProps$loaderCallb = ownProps.loaderCallback,
      loaderCallback = _ownProps$loaderCallb === undefined ? null : _ownProps$loaderCallb,
      _ownProps$showPager = ownProps.showPager,
      showPager = _ownProps$showPager === undefined ? true : _ownProps$showPager,
      externalSortColumn = ownProps.externalSortColumn,
      externalSortAscending = ownProps.externalSortAscending,
      exportData = ownProps.exportData,
      exportConfig = ownProps.exportConfig,
      exportStartCallback = ownProps.exportStartCallback,
      exportFinishCallback = ownProps.exportFinishCallback,
      _ownProps$dataSuccess = ownProps.dataSuccessCallback,
      dataSuccessCallback = _ownProps$dataSuccess === undefined ? null : _ownProps$dataSuccess;

  var initialConfigData = { externalSortColumn: externalSortColumn, externalSortAscending: externalSortAscending, exportData: exportData, exportConfig: exportConfig };
  dispatch(AbModelGridAction.setModelInitialData(modelName, initialConfigData));

  return {
    getModels: function getModels(configData) {
      if (dataSourceType === _constants.GRID_DATA_SOURCE_TYPE.METHOD) {
        return dispatch(AbModelGridAction.customMethodRequest(_extends({}, configData, { AbModelName: modelName, customGridFilter: customGridFilter, errorCallback: errorCallback, loaderCallback: loaderCallback, dataSourceType: dataSourceType, dataSourceOptions: dataSourceOptions, showPager: showPager, dataSuccessCallback: dataSuccessCallback })));
      } else {
        return dispatch(AbModelGridAction.modelsRequest(_extends({}, configData, { AbModelName: modelName, customGridFilter: customGridFilter, errorCallback: errorCallback, loaderCallback: loaderCallback, showPager: showPager, dataSuccessCallback: dataSuccessCallback })));
      }
    },
    setCurrentActiveItem: function setCurrentActiveItem(actionType, itemData) {
      var payloadData = {
        action: actionType,
        currentActiveItem: itemData,
        modelName: modelName
      };
      return dispatch(AbModelGridAction.setCurrentActiveItem(payloadData));
    },
    setAdvanceFilterConfig: function setAdvanceFilterConfig(data) {
      return dispatch(AbModelGridAction.mergeGridConfig({ modelName: modelName, data: data }));
    },
    deleteGridRow: function deleteGridRow(rowData) {
      var data = rowData.data,
          gridReloadConfigData = rowData.gridReloadConfigData;
      var AbModelInstance = gridReloadConfigData.AbModelInstance;

      return dispatch(AbModelGridAction.DeleteGridRow({ AbModelInstance: AbModelInstance, data: data, gridReloadConfigData: _extends({}, gridReloadConfigData, { dataSourceOptions: dataSourceOptions, AbModelName: modelName, customGridFilter: customGridFilter }, { showPager: showPager }), deleteCallback: deleteCallback, dataSourceType: dataSourceType, errorCallback: errorCallback }));
    },
    resetGridData: function resetGridData() {
      dispatch(AbModelGridAction.resetGridDataOnly(modelName));
    },
    exportGridData: function exportGridData(props, format, type) {
      return dispatch(AbModelGridAction.exportGridData(_extends({}, props, { format: format, type: type, exportStartCallback: exportStartCallback, exportFinishCallback: exportFinishCallback })));
    }
  };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_gridComponent2.default);