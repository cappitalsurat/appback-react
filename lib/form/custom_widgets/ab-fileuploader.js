'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _events = require('events');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _semanticUiReact = require('semantic-ui-react');

require('./css/ab-fileuploader.css');

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _formHelper = require('../form-helper');

var _base64images = require('../../base64images');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mime = require('mime-types');

var getFileCategory = function getFileCategory(filePath) {
  var selectedFileCategory = mime.lookup(filePath);
  if (selectedFileCategory) {
    var selectedFileCategoryArray = selectedFileCategory.split('/')[0];
    if (selectedFileCategoryArray === 'application') {
      selectedFileCategoryArray = filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length) || filePath;
    }
    return selectedFileCategoryArray;
  }
  return null;
};

var AbFileUploaderWidget = function (_React$Component) {
  _inherits(AbFileUploaderWidget, _React$Component);

  function AbFileUploaderWidget(props) {
    _classCallCheck(this, AbFileUploaderWidget);

    var _this = _possibleConstructorReturn(this, (AbFileUploaderWidget.__proto__ || Object.getPrototypeOf(AbFileUploaderWidget)).call(this, props));

    var value = props.value;

    var fileCategory = null;

    _this.proxy = new _events.EventEmitter();
    _this.state = {
      progress: -1,
      hasError: false,
      selectedFile: null,
      selectedFileCategory: null,
      signedUrlObject: null,
      previewImgUrl: null,
      hasOtherError: false,
      errorMsg: null
    };
    _this.onFileSelect = _this.onFileSelect.bind(_this);
    return _this;
  }

  _createClass(AbFileUploaderWidget, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.value) {
        var fileCategory = getFileCategory(this.props.value);
        this.setState({
          selectedFileCategory: fileCategory
        });
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (this.props.value !== nextProps.value && this.props.value !== null) {
        var fileCategory = getFileCategory(nextProps.value);
        this.setState({
          selectedFileCategory: fileCategory
        });
      }
    }
  }, {
    key: 'onSubmit',
    value: function onSubmit(e) {
      var _this2 = this;

      if (e) {
        e.preventDefault();
      }
      if (this.state.selectedFile === null) {
        return;
      }
      var folderPath = this.props.options.folderPath;

      this.setState({
        progress: 0,
        hasError: false
      }, function () {
        _this2.getSignedUrl(_this2.state.selectedFile, folderPath, function (error, data) {
          if (data) {
            _this2.setState({ signedUrlObject: data });
            _this2._doUpload(data.signedRequest, _this2.state.selectedFile);
          }
        });
      });
    }
  }, {
    key: 'onFileSelect',
    value: function onFileSelect(e) {
      var _this3 = this;

      var _props = this.props,
          id = _props.id,
          options = _props.options;
      var acceptFileType = options.acceptFileType,
          typeErrorMsg = options.typeErrorMsg,
          fileSize = options.fileSize,
          fileSizeErrorMsg = options.fileSizeErrorMsg,
          startUpLoadOnSelect = options.startUpLoadOnSelect,
          onFileChange = options.onFileChange;

      var selectedFile = e.target.files[0];
      var isError = false;
      this.setState({
        hasOtherError: false,
        errorMsg: null
      }, function () {
        if (acceptFileType.filter((0, _formHelper.compareFileWithMimeType)(selectedFile.type)).length === 0) {
          isError = true;
          _this3.setState({
            hasOtherError: true,
            errorMsg: typeErrorMsg ? typeErrorMsg : 'Valid file type are ' + acceptFileType.join(',')
          });
        } else if (fileSize) {
          var fileSizeInMb = selectedFile.size / 1048576;
          if (fileSizeInMb > fileSize) {
            isError = true;
            _this3.setState({
              hasOtherError: true,
              errorMsg: fileSizeErrorMsg ? fileSizeErrorMsg : 'Upload file having less then ' + fileSize + ' MB'
            });
          }
        }
        if (!isError) {
          onFileChange(selectedFile);
          var fileType = selectedFile.type.split('/');
          if (fileType[0] === 'application') {
            fileType[0] = selectedFile.name.substring(selectedFile.name.lastIndexOf('.') + 1, selectedFile.name.length) || selectedFile.name;
          }
          _this3.setState({
            selectedFile: selectedFile,
            selectedFileCategory: fileType[0],
            previewImgUrl: window.URL.createObjectURL(selectedFile)
          }, function () {
            if (fileType[0] === 'video') {
              _this3.refs['video_' + id].load();
            }
            if (startUpLoadOnSelect) {
              _this3.onSubmit();
            }
          });
        }
      });
    }
  }, {
    key: 'getSignedUrl',
    value: function getSignedUrl(file, folderPath, callback) {
      var appbackApi = this.props.options.appbackApi;

      var params = {
        key: folderPath + '/' + file.name.substr(0, file.name.lastIndexOf('.') - 1) + '_' + (0, _moment2.default)(Date.now()).format('YYYY-MM-DDTHH:mm:ss') + file.name.substr(file.name.lastIndexOf('.')),
        extension: file.name.substr(file.name.lastIndexOf('.')),
        contentType: file.type
      };
      appbackApi.getUploadUrl(params).then(function (data) {
        if (data && data.signedRequest) {
          callback(null, data);
        } else {
          callback(data, null);
        }
      }).catch(function (error) {
        console.error('Error from getUploadUrl', error);
        callback(error, null);
      });
    }
  }, {
    key: 'cancelUpload',
    value: function cancelUpload() {
      this.proxy.emit('abort');
      this.setState({
        progress: -1,
        hasError: false
      });
    }
  }, {
    key: 'progressRenderer',
    value: function progressRenderer(progress, hasError, cancelHandler) {
      if (hasError || progress > -1) {
        if (hasError) {
          var message = _react2.default.createElement(
            'span',
            { style: { color: '#a94442' } },
            'Failed to upload ...'
          );
        }
        return _react2.default.createElement(_semanticUiReact.Progress, { percent: progress, indicating: progress !== 100, progress: true, error: hasError ? true : false, color: 'violet', success: progress === 100 });
      }
      return '';
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          previewImgUrl = _state.previewImgUrl,
          hasOtherError = _state.hasOtherError,
          errorMsg = _state.errorMsg,
          selectedFileCategory = _state.selectedFileCategory,
          selectedFile = _state.selectedFile;
      var _props2 = this.props,
          value = _props2.value,
          options = _props2.options,
          id = _props2.id;
      var inputRef = options.inputRef,
          buttonRef = options.buttonRef,
          _options$showProgress = options.showProgress,
          showProgress = _options$showProgress === undefined ? true : _options$showProgress,
          acceptFileType = options.acceptFileType;
      // const formElement = this.props.formRenderer(this.onSubmit.bind(this), this.onFileSelect);

      var progressElement = null;
      if (showProgress) {
        progressElement = this.progressRenderer(this.state.progress, this.state.hasError, this.cancelUpload.bind(this));
      }

      return _react2.default.createElement(
        'div',
        { className: 'form-control customClass' },
        _react2.default.createElement('input', { type: 'file', name: 'file', onChange: this.onFileSelect, ref: inputRef, accept: acceptFileType.join(',') }),
        _react2.default.createElement('input', { type: 'button', style: { display: 'none' }, ref: buttonRef, onClick: this.onSubmit.bind(this) }),
        hasOtherError && _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement(
            'ul',
            { className: 'error-detail bs-callout bs-callout-info' },
            _react2.default.createElement(
              'li',
              { className: 'text-danger' },
              errorMsg
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { id: 'progress-container' },
          progressElement
        ),
        _react2.default.createElement(
          'div',
          null,
          (value || previewImgUrl) && selectedFileCategory === 'image' && _react2.default.createElement(_semanticUiReact.Image, { src: value && !previewImgUrl ? '' + value : previewImgUrl, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && selectedFileCategory === 'video' && _react2.default.createElement(
            'video',
            { id: 'video1', width: '200', ref: 'video_' + id },
            _react2.default.createElement('source', { src: value && !previewImgUrl ? '' + value : previewImgUrl, type: 'video/mp4' }),
            'Your browser does not support HTML5 video.'
          ),
          (value || previewImgUrl) && selectedFileCategory === 'pdf' && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.pdfFile, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && (selectedFileCategory === 'xlsx' || selectedFile === 'xls' || selectedFileCategory === 'csv') && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.xlsFile, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && (selectedFileCategory === 'docx' || selectedFile === 'doc') && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.docFile, size: 'tiny', shape: 'rounded' }),
          (value || previewImgUrl) && (selectedFileCategory === 'pptx' || selectedFile === 'ppt') && _react2.default.createElement(_semanticUiReact.Image, { src: _base64images.pptFile, size: 'tiny', shape: 'rounded' }),
          value && !previewImgUrl && _react2.default.createElement(
            'div',
            null,
            _react2.default.createElement(
              'a',
              { href: '' + value, target: '_blank' },
              'Download'
            )
          )
        )
      );
    }
  }, {
    key: '_getFormData',
    value: function _getFormData() {
      if (this.props.formGetter) {
        return this.props.formGetter();
      }
      return this.state.selectedFile;
    }
  }, {
    key: '_doUpload',
    value: function _doUpload(url, fileObj) {
      var _this4 = this;

      var form = this._getFormData();
      var req = new XMLHttpRequest();
      req.open('PUT', url);
      req.setRequestHeader('X-Amz-ACL', 'public-read');
      req.setRequestHeader('Content-Type', fileObj.type);
      var options = this.props.options;


      req.addEventListener('load', function (e) {
        _this4.proxy.removeAllListeners(['abort']);
        var newState = { progress: 100 };
        if (req.status >= 200 && req.status <= 299) {
          var onChange = _this4.props.onChange;

          options.onUpload(_this4.state.signedUrlObject.url);
          onChange(_this4.state.signedUrlObject.url);
          _this4.setState(newState, function () {
            options.onLoad(e, req);
          });
        } else {
          newState.hasError = true;
          _this4.setState(newState, function () {
            options.onError(e, req);
          });
        }
      }, false);

      req.addEventListener('error', function (e) {
        _this4.setState({
          hasError: true
        }, function () {
          options.onError(e, req);
        });
      }, false);

      req.upload.addEventListener('progress', function (e) {
        var progress = 0;
        if (e.total !== 0) {
          progress = parseInt(e.loaded / e.total * 100, 10);
        }
        _this4.setState({
          progress: progress
        }, function () {
          options.onProgress(e, req, progress);
        });
      }, false);

      req.addEventListener('abort', function (e) {
        _this4.setState({
          progress: -1
        }, function () {
          options.onAbort(e, req);
        });
      }, false);

      this.proxy.once('abort', function () {
        req.abort();
      });

      this.props.beforeSend(req).send(form);
    }
  }]);

  return AbFileUploaderWidget;
}(_react2.default.Component);

AbFileUploaderWidget.propTypes = {
  url: _propTypes2.default.string,
  formGetter: _propTypes2.default.func,
  formRenderer: _propTypes2.default.func,
  progressRenderer: _propTypes2.default.func,
  formCustomizer: _propTypes2.default.func,
  beforeSend: _propTypes2.default.func,
  onProgress: _propTypes2.default.func,
  onLoad: _propTypes2.default.func,
  onError: _propTypes2.default.func,
  onAbort: _propTypes2.default.func,
  onChange: _propTypes2.default.func
};

AbFileUploaderWidget.defaultProps = {
  formRenderer: function formRenderer(onSubmit, onFileSelect) {
    return _react2.default.createElement(
      'form',
      { className: '_react_fileupload_form_content', ref: 'form', method: 'post', onSubmit: onSubmit },
      _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement('input', { type: 'file', name: 'file', onChange: onFileSelect, ref: function ref(_ref) {
            return undefined.inputRef = _ref;
          } })
      ),
      _react2.default.createElement('input', { type: 'submit' })
    );
  },
  formCustomizer: function formCustomizer(form) {
    return form;
  },
  beforeSend: function beforeSend(request) {
    return request;
  },
  options: {
    onProgress: function onProgress(e, request, progress) {},
    onLoad: function onLoad(e, request) {},
    onError: function onError(e, request) {},
    onAbort: function onAbort(e, request) {},
    onUpload: function onUpload(url) {},
    onFileChange: function onFileChange(file) {},
    folderPath: 'other',
    acceptFileType: ['image/*'],
    fileSize: 2,
    startUpLoadOnSelect: true
  }
};

exports.default = AbFileUploaderWidget;