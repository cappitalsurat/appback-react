'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function AbRadioWidget(props) {
    var id = props.id,
        disabled = props.disabled,
        options = props.options,
        value = props.value,
        autofocus = props.autofocus,
        _onChange = props.onChange;
    var enumOptions = options.enumOptions,
        inline = options.inline,
        radioType = options.radioType,
        radioName = options.radioName;

    var name = Math.random().toString();

    if (radioName) {
        name = radioName;
    }
    return _react2.default.createElement(
        'div',
        { className: inline ? 'fields inline' : 'fields grouped', id: id },
        enumOptions.map(function (option, index) {
            var checked = option.value && value && option.value.toString() === value.toString();
            var radio = _react2.default.createElement(
                'div',
                { key: index, className: "field" },
                _react2.default.createElement(_semanticUiReact.Radio, { color: 'green',
                    id: id + '_' + index,
                    slider: _constants.ABCHECKBOXTYPE.SLIDER === radioType,
                    toggle: _constants.ABCHECKBOXTYPE.TOGGLE === radioType,
                    checked: checked,
                    name: name,
                    value: option.value.toString(),
                    label: _react2.default.createElement(
                        'label',
                        null,
                        option.label
                    ),
                    disabled: disabled,
                    autoFocus: autofocus && index === 0,
                    onChange: function onChange(event, data) {
                        _onChange(data.value.toString());
                    } })
            );
            return radio;
        })
    );
}

AbRadioWidget.defaultProps = {
    autofocus: false,
    options: {
        inline: false,
        radioName: null
    }
};

AbRadioWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    options: _react.PropTypes.shape({
        enumOptions: _react.PropTypes.array,
        inline: _react.PropTypes.bool,
        radioType: _react.PropTypes.string,
        radioName: _react.PropTypes.string
    }).isRequired,
    value: _react.PropTypes.any,
    required: _react.PropTypes.bool,
    disabled: _react.PropTypes.bool,
    autofocus: _react.PropTypes.bool,
    onChange: _react.PropTypes.func
};

exports.default = AbRadioWidget;