'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _semanticUiReact = require('semantic-ui-react');

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AbCheckboxWidget = function AbCheckboxWidget(props) {
    var id = props.id,
        value = props.value,
        _onChange = props.onChange,
        disabled = props.disabled,
        options = props.options;
    var label = options.label,
        checkboxType = options.checkboxType,
        _options$radio = options.radio,
        radio = _options$radio === undefined ? false : _options$radio;

    return _react2.default.createElement(_semanticUiReact.Checkbox, {
        slider: _constants.ABCHECKBOXTYPE.SLIDER === checkboxType,
        toggle: _constants.ABCHECKBOXTYPE.TOGGLE === checkboxType,
        id: props.id,
        label: _react2.default.createElement(
            'label',
            null,
            label
        ),
        disabled: disabled,
        checked: value,
        radio: radio,
        onChange: function onChange(event, data) {
            _onChange(data.checked);
        } });
};

AbCheckboxWidget.defaultProps = {
    checkboxType: ""
};

AbCheckboxWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    value: _react.PropTypes.bool,
    required: _react.PropTypes.bool,
    autofocus: _react.PropTypes.bool,
    disabled: _react.PropTypes.bool,
    onChange: _react.PropTypes.func,
    checkboxType: _react.PropTypes.string
};

exports.default = AbCheckboxWidget;