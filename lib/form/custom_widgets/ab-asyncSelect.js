'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

require('react-select/dist/react-select.css');

var _constants = require('../../constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AbSelectWidgetComponent = function AbSelectWidgetComponent(props) {
    var id = props.id,
        value = props.value,
        onChange = props.onChange,
        disabled = props.disabled,
        options = props.options,
        required = props.required,
        loadOptions = props.loadOptions;
    var autoload = options.autoload,
        backspaceRemoves = options.backspaceRemoves,
        labelKey = options.labelKey,
        multi = options.multi,
        valueKey = options.valueKey,
        async = options.async,
        selectOptions = options.selectOptions,
        searchable = options.searchable,
        clearable = options.clearable,
        noResultsText = options.noResultsText,
        selectPlaceholder = options.selectPlaceholder,
        searchPromptText = options.searchPromptText,
        autofocus = options.autofocus,
        optionRenderer = options.optionRenderer,
        valueRenderer = options.valueRenderer,
        cache = options.cache,
        delimiter = options.delimiter;

    var valueChanged = function valueChanged(formData) {
        var sendData = formData == null || formData.toString().length === 0 ? undefined : formData;
        onChange(sendData);
    };
    return _react2.default.createElement(
        'div',
        { style: required && !searchable ? { height: '36px' } : {} },
        _react2.default.createElement(_reactSelect2.default.Async, { multi: multi, simpleValue: true, value: value, clearable: clearable, disabled: disabled, onChange: valueChanged, valueKey: valueKey, labelKey: labelKey, loadOptions: loadOptions, backspaceRemoves: backspaceRemoves, autoload: autoload, searchable: searchable, noResultsText: noResultsText, placeholder: selectPlaceholder, searchPromptText: searchPromptText, autofocus: autofocus, required: required, optionRenderer: optionRenderer, valueRenderer: valueRenderer, cache: cache, delimiter: delimiter }),
        required && !searchable && _react2.default.createElement('input', { tabIndex: -1, value: value, required: true, style: { width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px' } })
    );
};

var AbAsyncSelectWidget = function (_React$Component) {
    _inherits(AbAsyncSelectWidget, _React$Component);

    function AbAsyncSelectWidget(props) {
        _classCallCheck(this, AbAsyncSelectWidget);

        var _this = _possibleConstructorReturn(this, (AbAsyncSelectWidget.__proto__ || Object.getPrototypeOf(AbAsyncSelectWidget)).call(this, props));

        _this.state = {
            "initialDataLoaded": false,
            "results": []
        };
        return _this;
    }

    _createClass(AbAsyncSelectWidget, [{
        key: 'loadOptions',
        value: function loadOptions(input) {
            var _props = this.props,
                AbModelInstance = _props.AbModelInstance,
                getDataFromServer = _props.getDataFromServer,
                options = _props.options;
            var customFilter = options.customFilter,
                labelKey = options.labelKey,
                matchInputFrom = options.matchInputFrom,
                filterOnFields = options.filterOnFields,
                dataSourceType = options.dataSourceType,
                dataSourceOptions = options.dataSourceOptions;

            var filter = {};
            if (filterOnFields && filterOnFields.length > 0) {
                var filterList = _.map(filterOnFields, function (filterCol) {
                    if (matchInputFrom === _constants.ABSELECT.START) {
                        return _defineProperty({}, filterCol, { "regexp": "/^" + input + "/i" });
                    } else {
                        return _defineProperty({}, filterCol, { "regexp": "/" + input + "/i" });
                    }
                });

                var where = {
                    "or": filterList
                };
                filter = where.or.length ? where : {};
            } else {
                if (matchInputFrom === _constants.ABSELECT.START) {
                    filter = _defineProperty({}, labelKey, { "regexp": "/^" + input + "/i" });
                } else {
                    filter = _defineProperty({}, labelKey, { "regexp": "/" + input + "/i" });
                }
            }
            if (customFilter) {
                filter = _extends({}, filter, customFilter);
            }
            if (dataSourceType && dataSourceOptions && dataSourceType === _constants.GRID_DATA_SOURCE_TYPE.METHOD) {
                var methodName = dataSourceOptions.methodName,
                    data = dataSourceOptions.data,
                    _options = dataSourceOptions.options;

                return AbModelInstance.custom(methodName, data, { "filter": filter }, _options).then(function (response) {
                    if (response) {
                        if (response instanceof Array) {
                            return { options: response };
                        } else {
                            if (response instanceof Object && response.data) {
                                return { options: response.data };
                            } else {
                                return { options: [] };
                            }
                        }
                    } else {
                        return { options: [] };
                    }
                }, function (error) {
                    console.log("error", error);
                    return { options: [] };
                });
            } else {
                return getDataFromServer({ AbModelInstance: AbModelInstance, filter: filter }).then(function (response) {
                    return { options: response };
                }, function (error) {
                    console.log("error", error);
                    return { options: [] };
                });
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var selectProps = _extends({}, this.props);
            var options = this.props.options;
            var async = options.async;

            selectProps["loadOptions"] = function (input, callback) {
                return _this2.loadOptions(input, callback);
            };
            return _react2.default.createElement(AbSelectWidgetComponent, selectProps);
        }
    }]);

    return AbAsyncSelectWidget;
}(_react2.default.Component);

AbAsyncSelectWidget.defaultProps = {
    options: {
        multi: false,
        clearable: true,
        searchable: false,
        autoload: true,
        backspaceRemoves: true,
        async: false,
        noResultsText: "No results found",
        selectPlaceholder: "Select ...",
        searchPromptText: "Type to search",
        autofocus: false,
        customFilter: {},
        matchInputFrom: _constants.ABSELECT.ANY,
        optionRenderer: null,
        valueRenderer: null,
        filterOnFields: [],
        cache: true,
        delimiter: _constants.FORM_UTILS.DELIMITER
    }
};

AbAsyncSelectWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    value: _react.PropTypes.string,
    required: _react.PropTypes.bool,
    autofocus: _react.PropTypes.bool,
    disabled: _react.PropTypes.bool,
    onChange: _react.PropTypes.func,
    options: _react.PropTypes.shape({
        selectOptions: _react.PropTypes.array.required,
        labelKey: _react.PropTypes.string.required,
        valueKey: _react.PropTypes.string.required,
        multi: _react.PropTypes.bool,
        clearable: _react.PropTypes.bool,
        searchable: _react.PropTypes.bool,
        autoload: _react.PropTypes.bool,
        backspaceRemoves: _react.PropTypes.bool,
        async: _react.PropTypes.bool,
        noResultsText: _react.PropTypes.string,
        selectPlaceholder: _react.PropTypes.string,
        searchPromptText: _react.PropTypes.string,
        autofocus: _react.PropTypes.bool,
        appbackApi: _react.PropTypes.object.required,
        optionModel: _react.PropTypes.string.required,
        customFilter: _react.PropTypes.object,
        matchInputFrom: _react.PropTypes.string,
        optionRenderer: _react.PropTypes.func,
        valueRenderer: _react.PropTypes.func,
        filterOnFields: _react.PropTypes.array,
        cache: _react.PropTypes.bool,
        delimiter: _react.PropTypes.string
    }).isRequired
};
exports.default = AbAsyncSelectWidget;