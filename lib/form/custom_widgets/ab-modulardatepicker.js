"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

require("./css/ab-modulardatepicker.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// import PropTypes from "prop-types";

// import { shouldRender, parseDateString, toDateString, pad } from "../../utils";

function shouldRender(comp, nextProps, nextState) {
    var props = comp.props,
        state = comp.state;

    return !deepEquals(props, nextProps) || !deepEquals(state, nextState);
}

function parseDateString(dateString) {
    var includeTime = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
    var showDay = arguments[2];
    var showMonth = arguments[3];
    var showYear = arguments[4];

    if (!dateString) {
        return _extends({}, showYear && { year: -1 }, showMonth && { month: -1 }, showDay && { day: -1 }, {
            hour: includeTime ? -1 : 0,
            minute: includeTime ? -1 : 0,
            second: includeTime ? -1 : 0
        });
    }
    var date = new Date(dateString);
    if (Number.isNaN(date.getTime())) {
        throw new Error("Unable to parse date " + dateString);
    }
    return _extends({}, showYear && { year: date.getUTCFullYear() }, showMonth && { month: date.getUTCMonth() + 1 }, showDay && { day: date.getUTCDate() }, {
        hour: includeTime ? date.getUTCHours() : 0,
        minute: includeTime ? date.getUTCMinutes() : 0,
        second: includeTime ? date.getUTCSeconds() : 0
    });
}

function toDateString(_ref) {
    var _ref$year = _ref.year,
        year = _ref$year === undefined ? 1972 : _ref$year,
        _ref$month = _ref.month,
        month = _ref$month === undefined ? 1 : _ref$month,
        _ref$day = _ref.day,
        day = _ref$day === undefined ? 0 : _ref$day,
        _ref$hour = _ref.hour,
        hour = _ref$hour === undefined ? 0 : _ref$hour,
        _ref$minute = _ref.minute,
        minute = _ref$minute === undefined ? 0 : _ref$minute,
        _ref$second = _ref.second,
        second = _ref$second === undefined ? 0 : _ref$second;
    var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

    var utcTime = Date.UTC(year, month - 1, day, hour, minute, second);
    var datetime = new Date(utcTime).toJSON();
    return time ? datetime : datetime.slice(0, 10);
}

function pad(num, size) {
    var s = String(num);
    while (s.length < size) {
        s = "0" + s;
    }
    return s;
}

function deepEquals(a, b) {
    var ca = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
    var cb = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];

    // Partially extracted from node-deeper and adapted to exclude comparison
    // checks for functions.
    // https://github.com/othiym23/node-deeper
    if (a === b) {
        return true;
    } else if (typeof a === "function" || typeof b === "function") {
        // Assume all functions are equivalent
        // see https://github.com/mozilla-services/react-jsonschema-form/issues/255
        return true;
    } else if ((typeof a === "undefined" ? "undefined" : _typeof(a)) !== "object" || (typeof b === "undefined" ? "undefined" : _typeof(b)) !== "object") {
        return false;
    } else if (a === null || b === null) {
        return false;
    } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime();
    } else if (a instanceof RegExp && b instanceof RegExp) {
        return a.source === b.source && a.global === b.global && a.multiline === b.multiline && a.lastIndex === b.lastIndex && a.ignoreCase === b.ignoreCase;
    } else if (isArguments(a) || isArguments(b)) {
        if (!(isArguments(a) && isArguments(b))) {
            return false;
        }
        var slice = Array.prototype.slice;
        return deepEquals(slice.call(a), slice.call(b), ca, cb);
    } else {
        if (a.constructor !== b.constructor) {
            return false;
        }

        var ka = Object.keys(a);
        var kb = Object.keys(b);
        // don't bother with stack acrobatics if there's nothing there
        if (ka.length === 0 && kb.length === 0) {
            return true;
        }
        if (ka.length !== kb.length) {
            return false;
        }

        var cal = ca.length;
        while (cal--) {
            if (ca[cal] === a) {
                return cb[cal] === b;
            }
        }
        ca.push(a);
        cb.push(b);

        ka.sort();
        kb.sort();
        for (var j = ka.length - 1; j >= 0; j--) {
            if (ka[j] !== kb[j]) {
                return false;
            }
        }

        var key = void 0;
        for (var k = ka.length - 1; k >= 0; k--) {
            key = ka[k];
            if (!deepEquals(a[key], b[key], ca, cb)) {
                return false;
            }
        }

        ca.pop();
        cb.pop();

        return true;
    }
}

function isArguments(object) {
    return Object.prototype.toString.call(object) === "[object Arguments]";
}

function rangeOptions(start, stop) {
    var options = [];
    for (var i = start; i <= stop; i++) {
        options.push({ value: i, label: pad(i, 2) });
    }
    return options;
}

function readyForChange(state) {
    return Object.keys(state).every(function (key) {
        return state[key] !== -1;
    });
}

function DateElement(props) {
    var type = props.type,
        range = props.range,
        value = props.value,
        select = props.select,
        rootId = props.rootId,
        disabled = props.disabled,
        readonly = props.readonly,
        autofocus = props.autofocus,
        registry = props.registry,
        onBlur = props.onBlur;

    var id = rootId + "_" + type;
    var SelectWidget = registry.widgets.SelectWidget;

    return _react2.default.createElement(SelectWidget, {
        schema: { type: "integer" },
        id: id,
        className: "form-control",
        options: { enumOptions: rangeOptions(range[0], range[1]) },
        placeholder: type,
        value: value,
        disabled: disabled,
        readonly: readonly,
        autofocus: autofocus,
        onChange: function onChange(value) {
            return select(type, value);
        },
        onBlur: onBlur
    });
}

var AbModularDatePickerWidget = function (_Component) {
    _inherits(AbModularDatePickerWidget, _Component);

    function AbModularDatePickerWidget(props) {
        _classCallCheck(this, AbModularDatePickerWidget);

        var _this = _possibleConstructorReturn(this, (AbModularDatePickerWidget.__proto__ || Object.getPrototypeOf(AbModularDatePickerWidget)).call(this, props));

        _initialiseProps.call(_this);

        var _props$options = props.options,
            _props$options$showDa = _props$options.showDay,
            showDay = _props$options$showDa === undefined ? true : _props$options$showDa,
            _props$options$showMo = _props$options.showMonth,
            showMonth = _props$options$showMo === undefined ? true : _props$options$showMo,
            _props$options$showYe = _props$options.showYear,
            showYear = _props$options$showYe === undefined ? true : _props$options$showYe;

        _this.state = parseDateString(props.value, props.time, showDay, showMonth, showYear);
        _this.dateElementProps = _this.dateElementProps.bind(_this);
        return _this;
    }

    _createClass(AbModularDatePickerWidget, [{
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(nextProps) {
            var _nextProps$options = nextProps.options,
                _nextProps$options$sh = _nextProps$options.showDay,
                showDay = _nextProps$options$sh === undefined ? true : _nextProps$options$sh,
                _nextProps$options$sh2 = _nextProps$options.showMonth,
                showMonth = _nextProps$options$sh2 === undefined ? true : _nextProps$options$sh2,
                _nextProps$options$sh3 = _nextProps$options.showYear,
                showYear = _nextProps$options$sh3 === undefined ? true : _nextProps$options$sh3;

            this.setState(parseDateString(nextProps.value, nextProps.time, showDay, showMonth, showYear));
        }
    }, {
        key: "shouldComponentUpdate",
        value: function shouldComponentUpdate(nextProps, nextState) {
            return shouldRender(this, nextProps, nextState);
        }
    }, {
        key: "dateElementProps",
        value: function dateElementProps() {
            var time = this.props.time;
            var _state = this.state,
                year = _state.year,
                month = _state.month,
                day = _state.day,
                hour = _state.hour,
                minute = _state.minute,
                second = _state.second;

            var data = [{ type: "year", range: [1900, 2020], value: year }, { type: "month", range: [1, 12], value: month }, { type: "day", range: [1, 31], value: day }];
            if (time) {
                data.push({ type: "hour", range: [0, 23], value: hour }, { type: "minute", range: [0, 59], value: minute }, { type: "second", range: [0, 59], value: second });
            }
            return data;
        }
    }, {
        key: "render",
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                id = _props.id,
                disabled = _props.disabled,
                readonly = _props.readonly,
                autofocus = _props.autofocus,
                registry = _props.registry,
                onBlur = _props.onBlur,
                options = _props.options;
            var _options$showDay = options.showDay,
                showDay = _options$showDay === undefined ? true : _options$showDay,
                _options$showMonth = options.showMonth,
                showMonth = _options$showMonth === undefined ? true : _options$showMonth,
                _options$showYear = options.showYear,
                showYear = _options$showYear === undefined ? true : _options$showYear;

            var data = this.dateElementProps();
            return _react2.default.createElement(
                "ul",
                { className: "list-inline" },
                data.map(function (elemProps, i) {
                    return elemProps.type === "day" && showDay || elemProps.type === "month" && showMonth || elemProps.type === "year" && showYear ? _react2.default.createElement(
                        "li",
                        { key: i },
                        _react2.default.createElement(DateElement, _extends({
                            rootId: id,
                            select: _this2.onChange
                        }, elemProps, {
                            disabled: disabled,
                            readonly: readonly,
                            registry: registry,
                            onBlur: onBlur,
                            autofocus: autofocus && i === 0
                        }))
                    ) : null;
                }),
                _react2.default.createElement(
                    "li",
                    null,
                    _react2.default.createElement(
                        "a",
                        {
                            href: "#",
                            className: "btn btn-warning btn-clear",
                            onClick: this.clear },
                        "Clear"
                    )
                )
            );
        }
    }]);

    return AbModularDatePickerWidget;
}(_react.Component);

// if (process.env.NODE_ENV !== "production") {


AbModularDatePickerWidget.defaultProps = {
    time: false,
    disabled: false,
    readonly: false,
    autofocus: false
};

var _initialiseProps = function _initialiseProps() {
    var _this3 = this;

    this.onChange = function (property, value) {
        _this3.setState(_defineProperty({}, property, typeof value === "undefined" ? -1 : value), function () {
            // Only propagate to parent state if we have a complete date{time}
            if (readyForChange(_this3.state)) {
                _this3.props.onChange(toDateString(_this3.state, _this3.props.time));
            }
        });
    };

    this.setNow = function (event) {
        event.preventDefault();
        var _props2 = _this3.props,
            time = _props2.time,
            disabled = _props2.disabled,
            readonly = _props2.readonly,
            onChange = _props2.onChange,
            options = _props2.options;
        var _options$showDay2 = options.showDay,
            showDay = _options$showDay2 === undefined ? true : _options$showDay2,
            _options$showMonth2 = options.showMonth,
            showMonth = _options$showMonth2 === undefined ? true : _options$showMonth2,
            _options$showYear2 = options.showYear,
            showYear = _options$showYear2 === undefined ? true : _options$showYear2;

        if (disabled || readonly) {
            return;
        }
        var nowDateObj = parseDateString(new Date().toJSON(), time, showDay, showMonth, showYear);
        _this3.setState(nowDateObj, function () {
            return onChange(toDateString(_this3.state, time));
        });
    };

    this.clear = function (event) {
        event.preventDefault();
        var _props3 = _this3.props,
            time = _props3.time,
            disabled = _props3.disabled,
            readonly = _props3.readonly,
            onChange = _props3.onChange,
            options = _props3.options;
        var _options$showDay3 = options.showDay,
            showDay = _options$showDay3 === undefined ? true : _options$showDay3,
            _options$showMonth3 = options.showMonth,
            showMonth = _options$showMonth3 === undefined ? true : _options$showMonth3,
            _options$showYear3 = options.showYear,
            showYear = _options$showYear3 === undefined ? true : _options$showYear3;


        if (disabled || readonly) {
            return;
        }
        _this3.setState(parseDateString("", time, showDay, showMonth, showYear), function () {
            return onChange(undefined);
        });
    };
};

AbModularDatePickerWidget.propTypes = {
    schema: _react.PropTypes.object.isRequired,
    id: _react.PropTypes.string.isRequired,
    value: _react.PropTypes.string,
    required: _react.PropTypes.bool,
    disabled: _react.PropTypes.bool,
    readonly: _react.PropTypes.bool,
    autofocus: _react.PropTypes.bool,
    onChange: _react.PropTypes.func,
    onBlur: _react.PropTypes.func,
    time: _react.PropTypes.bool,
    showDay: _react.PropTypes.bool,
    showMonth: _react.PropTypes.bool,
    showYear: _react.PropTypes.bool
};
// }

exports.default = AbModularDatePickerWidget;