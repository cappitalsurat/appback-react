'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _formComponent = require('../components/form-component');

var _formComponent2 = _interopRequireDefault(_formComponent);

var _actions = require('../../actions');

var AbModelFormAction = _interopRequireWildcard(_actions);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var mapStateToProps = function mapStateToProps(state, ownProps) {
  var appbackApi = ownProps.appbackApi,
      modelName = ownProps.modelName;

  var mapped = {
    abModelData: state.abModels[modelName],
    formData: state.abModels[modelName] ? state.abModels[modelName].form.currentActiveItem : {},
    formAction: state.abModels[modelName] ? state.abModels[modelName].form.action : null,
    filterCols: state.abModels[modelName] ? state.abModels[modelName].filterCols : [],
    includes: state.abModels[modelName] ? state.abModels[modelName].includes : [],
    AbModelInstance: appbackApi.createModelRest(modelName, null)
  };
  return mapped;
};
function mapDispatchToProps(dispatch, ownProps) {
  dispatch(AbModelFormAction.setModelInitialData(ownProps.modelName));
  return {
    saveModelFormData: function saveModelFormData(payloadData) {
      var modelName = ownProps.modelName,
          errorCallback = ownProps.errorCallback;

      payloadData["AbModelName"] = modelName;
      payloadData["errorCallback"] = errorCallback;
      return dispatch(AbModelFormAction.saveModelFormData(payloadData));
    },
    createModelFormData: function createModelFormData(payloadData) {
      var modelName = ownProps.modelName,
          errorCallback = ownProps.errorCallback;

      payloadData["AbModelName"] = ownProps.modelName;
      payloadData["errorCallback"] = errorCallback;
      return dispatch(AbModelFormAction.createModelFormData(payloadData));
    },
    clearFormData: function clearFormData() {
      dispatch(AbModelFormAction.clearFormData({ modelName: ownProps.modelName }));
    },
    getPreLoadFormData: function getPreLoadFormData(loadData) {
      var filter = loadData.filter,
          AbModelInstance = loadData.AbModelInstance,
          saveCallback = loadData.saveCallback,
          errorCallback = loadData.errorCallback;

      dispatch(AbModelFormAction.getPreLoadFormData(_extends({ AbModelName: ownProps.modelName }, _extends({}, loadData))));
    }
  };
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps, null, { withRef: true })(_formComponent2.default);