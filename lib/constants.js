'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
// ------------------------------------
// Constants
// ------------------------------------

var MODELS_DATA_REQUEST_SUCCESS = exports.MODELS_DATA_REQUEST_SUCCESS = 'MODELS_DATA_REQUEST_SUCCESS';
var SET_MODEL_INITIAL_STATE = exports.SET_MODEL_INITIAL_STATE = 'SET_MODEL_INITIAL_STATE';
var SET_CURRENT_ACTIVE_ITEM = exports.SET_CURRENT_ACTIVE_ITEM = 'SET_CURRENT_ACTIVE_ITEM';
var SAVE_MODEL_FORM_DATA = exports.SAVE_MODEL_FORM_DATA = 'SAVE_MODEL_FORM_DATA';
var SAVE_MODEL_FORM_DATA_SUCCESS = exports.SAVE_MODEL_FORM_DATA_SUCCESS = 'SAVE_MODEL_FORM_DATA_SUCCESS';
var CREATE_MODEL_FORM_DATA = exports.CREATE_MODEL_FORM_DATA = 'CREATE_MODEL_FORM_DATA';
var CREATE_MODEL_FORM_DATA_SUCCESS = exports.CREATE_MODEL_FORM_DATA_SUCCESS = 'CREATE_MODEL_FORM_DATA_SUCCESS';
var CLEAR_FORM_DATA = exports.CLEAR_FORM_DATA = 'CLEAR_FORM_DATA';
var MERGE_GRID_CONFIG = exports.MERGE_GRID_CONFIG = "MERGE_GRID_CONFIG";
var RESET_GRID_DATA_ONLY = exports.RESET_GRID_DATA_ONLY = "RESET_GRID_DATA_ONLY";
var SHOW_LOADER = exports.SHOW_LOADER = 'SHOW_LOADER';
var HIDE_LOADER = exports.HIDE_LOADER = 'HIDE_LOADER';

var COLUMNTYPE = exports.COLUMNTYPE = {
    BOOL: "bool",
    DATE: "date",
    ACTIONS: "actions",
    LINK: "link"
};

var ACTIONBUTTON = exports.ACTIONBUTTON = {
    VIEW: "view",
    EDIT: "edit",
    DELETE: "delete",
    CREATE: "create"
};

var ABCHECKBOXTYPE = exports.ABCHECKBOXTYPE = {
    TOGGLE: "toggle",
    SLIDER: "slider"
};

var ABSELECT = exports.ABSELECT = {
    START: "start",
    ANY: "any"
};

var GRID_DATA_SOURCE_TYPE = exports.GRID_DATA_SOURCE_TYPE = {
    MODEL: "model",
    METHOD: "method"
};

var FORM_CONDITION_TYPE = exports.FORM_CONDITION_TYPE = {
    "DEPENDENCY": "dependency",
    "VISIBILITY": "visibility"
};

var FORM_CONDITION_ACTION = exports.FORM_CONDITION_ACTION = {
    "HIDDEN": "hidden",
    "DISABLED": "disabled",
    "ENABLED": "enabled"
};

var SELECT_MULTI_OPTIONS = exports.SELECT_MULTI_OPTIONS = {
    ARRAY: "ARRAY",
    STRING: "STRING"
};

var FORM_DATA_SOURCE_OPTIONS_TYPE = exports.FORM_DATA_SOURCE_OPTIONS_TYPE = {
    CREATE: "CREATE",
    EDIT: "EDIT",
    LOAD: "LOAD"
};

var FORM_UTILS = exports.FORM_UTILS = {
    DELIMITER: ','
};