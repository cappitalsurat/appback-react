'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.OtherFilter = exports.OtherPager = exports.actionComponent = exports.dataRenderer = exports.LinkRenderer = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _constants = require('./constants');

var _semanticUiReact = require('semantic-ui-react');

var _abSelect = require('./form/custom_widgets/ab-select');

var _abSelect2 = _interopRequireDefault(_abSelect);

var _reactSelect = require('react-select');

var _reactSelect2 = _interopRequireDefault(_reactSelect);

require('react-select/dist/react-select.css');

var _gridFilterContainer = require('./grid/containers/grid-filter-container');

var _gridFilterContainer2 = _interopRequireDefault(_gridFilterContainer);

var _gridConfirmComponent = require('./grid/components/grid-confirm-component');

var _gridConfirmComponent2 = _interopRequireDefault(_gridConfirmComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LinkRenderer = exports.LinkRenderer = function LinkRenderer(props) {
    var typeOptions = props.metadata.typeOptions;
    var path = typeOptions.path;

    if (path) {
        return _react2.default.createElement(
            'a',
            { href: path + "/" + props.data, target: '_top' },
            props.data
        );
    } else {
        return _react2.default.createElement(
            'a',
            null,
            props.data
        );
    }
};

var dataRenderer = exports.dataRenderer = function dataRenderer(props) {
    var type = props.metadata.type;

    if (type) {
        if (type === _constants.COLUMNTYPE.DATE) {
            var typeOptions = props.metadata.typeOptions;
            var format = typeOptions.format;

            if (format) {
                return _react2.default.createElement(
                    'div',
                    null,
                    (0, _moment2.default)(props.data).format(format)
                );
            } else {
                return _react2.default.createElement(
                    'div',
                    null,
                    (0, _moment2.default)(props.data).format()
                );
            }
        } else if (type === _constants.COLUMNTYPE.BOOL) {
            var _typeOptions = props.metadata.typeOptions;
            var truePlaceHolder = _typeOptions.truePlaceHolder,
                falsePlaceHolder = _typeOptions.falsePlaceHolder;

            if (truePlaceHolder && falsePlaceHolder) {
                return _react2.default.createElement(
                    'div',
                    null,
                    props.data ? truePlaceHolder : falsePlaceHolder
                );
            } else {
                return _react2.default.createElement(
                    'div',
                    null,
                    props.data
                );
            }
        } else {
            return _react2.default.createElement(
                'div',
                null,
                props.data
            );
        }
    } else {
        return _react2.default.createElement(
            'div',
            null,
            props.data
        );
    }
};

var actionComponent = exports.actionComponent = function actionComponent(props, onClickButton) {
    var _defaultButtonsList;

    var defaultButtonsList = (_defaultButtonsList = {}, _defineProperty(_defaultButtonsList, _constants.ACTIONBUTTON.VIEW, {
        "label": "View",
        "key": _constants.ACTIONBUTTON.VIEW,
        "icon": "info"
    }), _defineProperty(_defaultButtonsList, _constants.ACTIONBUTTON.EDIT, {
        "label": "Edit",
        "key": _constants.ACTIONBUTTON.EDIT,
        "icon": "edit"
    }), _defineProperty(_defaultButtonsList, _constants.ACTIONBUTTON.DELETE, {
        "label": "Delete",
        "key": _constants.ACTIONBUTTON.DELETE,
        "icon": "trash"
    }), _defaultButtonsList);
    var typeOptions = props.metadata.typeOptions;

    var finalButtonList = [];

    function orderButtons(buttonsOrder, button, buttonComponent) {
        if (buttonsOrder && buttonsOrder.indexOf(button.key) > -1) {
            var buttonOrderIndex = buttonsOrder.indexOf(button.key);
            finalButtonList[buttonOrderIndex] = buttonComponent;
        } else {
            finalButtonList.push(buttonComponent);
        }
    }
    if (typeOptions) {
        var buttons = typeOptions.buttons,
            defaultButtons = typeOptions.defaultButtons,
            clickHandler = typeOptions.clickHandler,
            _typeOptions$deleteCo = typeOptions.deleteConfirmModalOptions,
            deleteConfirmModalOptions = _typeOptions$deleteCo === undefined ? null : _typeOptions$deleteCo,
            _typeOptions$buttonsO = typeOptions.buttonsOrder,
            buttonsOrder = _typeOptions$buttonsO === undefined ? null : _typeOptions$buttonsO;

        if (defaultButtons && defaultButtons.length > 0) {
            defaultButtons.map(function (buttonName, i) {
                var button = defaultButtonsList[buttonName];
                var buttonComponent = null;
                if (button.key === _constants.ACTIONBUTTON.DELETE) {
                    buttonComponent = _react2.default.createElement(_gridConfirmComponent2.default, { key: i, triggerProps: button, index: i, confirmCallback: function confirmCallback(e) {
                            return onClickButton(button.key, props.rowData, clickHandler);
                        }, modalOptions: deleteConfirmModalOptions });
                } else {
                    buttonComponent = _react2.default.createElement(_semanticUiReact.Popup, { key: i,
                        trigger: _react2.default.createElement(_semanticUiReact.Button, { icon: button.icon, onClick: function onClick(e) {
                                return onClickButton(button.key, props.rowData, clickHandler);
                            } }),
                        content: button.label
                    });
                }
                orderButtons(buttonsOrder, button, buttonComponent);
            });
        }
        if (buttons && buttons.length > 0) {
            buttons.map(function (button, i) {
                var buttonComponent = null;
                if (button.component) {
                    buttonComponent = button.component(props);
                } else {
                    buttonComponent = _react2.default.createElement(_semanticUiReact.Popup, { key: i + "Ext",
                        trigger: _react2.default.createElement(_semanticUiReact.Button, { icon: button.icon, onClick: function onClick(e) {
                                return onClickButton(button.key, props.rowData, clickHandler);
                            } }),
                        content: button.label
                    });
                }
                orderButtons(buttonsOrder, button, buttonComponent);
            });
        }
    }
    return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
            _semanticUiReact.Button.Group,
            { basic: true, size: 'small' },
            finalButtonList
        )
    );
};

var OtherPager = exports.OtherPager = function OtherPager(props) {
    var pageChange = function pageChange(event) {
        props.setPage(parseInt(event.target.getAttribute("data-value")));
    };
    var onChange = function onChange(event, args) {
        props.setPage(args.value);
    };
    var previousPage = function previousPage() {
        props.previous();
        // refs.pageDrp.setValue(props.currentPage - 1);
    };
    var nextPage = function nextPage() {
        props.next();
        // refs.pageDrp.setValue(props.currentPage + 1);
    };
    var exportModelData = function exportModelData(modelName, config, format) {
        var link = document.createElement("a");
        var url = config.apiUrl + '/' + modelName + '/report/' + format + '?access_token=' + config.token + '&filter={}';
        link.href = url;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };
    // render() {
    var previous = "";
    var next = "";
    var exportComponent = "";

    previous = _react2.default.createElement(_semanticUiReact.Button, { size: 'mini', content: props.previousText, disabled: props.currentPage == 0, icon: 'angle left', labelPosition: 'left', floated: 'left', onClick: previousPage.bind(undefined) });

    var isLastPage = props.currentPage == props.maxPage - 1;
    next = _react2.default.createElement(_semanticUiReact.Button, _extends({ size: 'mini', content: props.nextText, disabled: isLastPage, icon: 'angle right', labelPosition: 'right' }, !props.exportData && { floated: 'right' }, { onClick: nextPage.bind(undefined) }));

    var options = [];
    var pagesObj = [];
    for (var i = 0; i < props.maxPage; i++) {
        pagesObj.push({ text: i + 1, value: i });
    }

    options = _react2.default.createElement(
        'span',
        { className: 'page-count-wrapper' },
        _react2.default.createElement(_semanticUiReact.Dropdown, { pointing: true, size: 'mini', scrolling: true, options: pagesObj, value: props.currentPage, onChange: onChange.bind(undefined) }),
        '/ ',
        props.maxPage
    );

    var exportOptions = [{ text: 'All', value: 'ALL' }, { text: 'Current Page', value: 'CURRENT' }];

    exportComponent = _react2.default.createElement(
        'div',
        { className: 'float-right' },
        _react2.default.createElement(
            _semanticUiReact.Menu,
            { size: 'mini' },
            _react2.default.createElement(
                _semanticUiReact.Menu.Item,
                null,
                _react2.default.createElement(_semanticUiReact.Popup, {
                    trigger: _react2.default.createElement(
                        _semanticUiReact.Dropdown,
                        { icon: 'file excel outline', basic: true, compact: true, className: 'icon' },
                        _react2.default.createElement(
                            _semanticUiReact.Dropdown.Menu,
                            null,
                            _react2.default.createElement(
                                _semanticUiReact.Dropdown.Item,
                                { onClick: function onClick() {
                                        props.exportGridData(props, 'excel', 'ALL');
                                    } },
                                'All'
                            ),
                            _react2.default.createElement(
                                _semanticUiReact.Dropdown.Item,
                                { onClick: function onClick() {
                                        props.exportGridData(props, 'excel', 'CURRENT');
                                    } },
                                'Page'
                            )
                        )
                    ),
                    content: 'Export as Excel'
                })
            ),
            _react2.default.createElement(
                _semanticUiReact.Menu.Item,
                null,
                _react2.default.createElement(_semanticUiReact.Popup, {
                    trigger: _react2.default.createElement(
                        _semanticUiReact.Dropdown,
                        { icon: 'file pdf outline', basic: true, compact: true, className: 'icon' },
                        _react2.default.createElement(
                            _semanticUiReact.Dropdown.Menu,
                            null,
                            _react2.default.createElement(
                                _semanticUiReact.Dropdown.Item,
                                { onClick: function onClick() {
                                        props.exportGridData(props, 'pdf', 'ALL');
                                    } },
                                'All'
                            ),
                            _react2.default.createElement(
                                _semanticUiReact.Dropdown.Item,
                                { onClick: function onClick() {
                                        props.exportGridData(props, 'pdf', 'CURRENT');
                                    } },
                                'Page'
                            )
                        )
                    ),
                    content: 'Export as Pdf'
                })
            )
        )
    );

    return props.exportData ? _react2.default.createElement(
        'div',
        null,
        previous,
        options,
        next,
        exportComponent
    ) : _react2.default.createElement(
        'div',
        { className: 'center' },
        previous,
        options,
        next
    );
    // }
};

var OtherFilter = exports.OtherFilter = function OtherFilter(props, advanceFilter) {
    // let answer = null;
    // const searchChange = (e, filter) => {
    //     props.changeFilter(filter.value);
    // }
    // const OnSelectChange = (value) =>{
    //     answer=value;
    // }
    // const { changeFilter, results, currentResults, placeholderText } = props;
    // let value = "";
    return _react2.default.createElement(_gridFilterContainer2.default, _extends({}, props, { advanceFilter: advanceFilter }));
    /*return (
        
        <Grid columns='equal' style={{ "margin-bottom": 5 }}>
            <Grid.Row>
                <Grid.Column width={3}>
                    <Input icon='search' placeholder='Search...' onChange={searchChange.bind(this)} />
                </Grid.Column>
               
            </Grid.Row>
        </Grid>
    )*/
};