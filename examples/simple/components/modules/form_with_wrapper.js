import React, { Component } from 'react';
import { AbModelConstants, AbModelForm, AbModelGrid, FormWrapper, FormHelper, AbFormWidgets } from 'appback-react'
import { Segment, Form as CustomForm, Button, Input, Header } from 'semantic-ui-react'
import Form from "jb-react-jsonschema-form";
import { geoData } from "../data.js";
const { COLUMNTYPE, ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE } = AbModelConstants;
const { filterFormDataBasedOnSchema, getFormTransformList, transformFieldsValueBefore } = FormHelper;
const { AbSyncSelectWidget } = AbFormWidgets;
import moment from 'moment';
import AppbackApi from "appback-api";
const appbackApi = new AppbackApi('http://localhost:4000/api')
let widgets = {
    AbSyncSelect: AbSyncSelectWidget
};

class WrappedSemanticComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {

            formData: props.formData || {}
        }
    }
    render() {
        const { saveModelFormData, createModelFormData, AbModelInstance, saveCallback, errorCallback } = this.props;
        const saveSuccessCallBack = (event, data) => {
            this.setState({
                formData: {}
            }, saveCallback(event, data))
            console.log("WrappedSemanticComponent save and edit callback data EVENT", event);
            console.log("WrappedSemanticComponent save and edit callback data DATA", data);
        }

        const saveErrorCallBack = (event, error) => {
            console.log("WrappedSemanticComponent save and edit error callback data EVENT", event);
            console.log("WrappedSemanticComponent save and edit error callback data DATA", error);
            errorCallback(event, error)
        }
        const onFormSubmit = (e, { formData }) => {
            e.preventDefault();
            let finalData = _.pick(this.state.formData, _.keys(formData));
            let payloadData = {
                formMetaData: { formData: finalData },
                AbModelInstance: AbModelInstance,
                saveCallback: saveSuccessCallBack,
                errorCallback: saveErrorCallBack,
                transformData: false
            }
            if (this.state.formData.id) {
                saveModelFormData(payloadData);
            } else {
                createModelFormData(payloadData);
            }
        }
        const handleChange = (e, { name, value }) => {
            let formData = _.cloneDeep(this.state.formData);
            formData[name] = value;
            this.setState({ formData });
        }
        const clearFormData = () => {
            this.setState({
                formData: {}
            })
        }
        return (
            <CustomForm onSubmit={onFormSubmit}>
                {this.state.formData.id && <Input name="id" placeholder='Id' value={this.state.formData.id} type="hidden" />}
                <CustomForm.Field>
                    <label>Task Name</label>
                    <Input name="name" placeholder='Task Name' value={this.state.formData.name} onChange={handleChange} />
                </CustomForm.Field>
                <CustomForm.Field>
                    <label>Task Description</label>
                    <Input name="description" placeholder='Task Description' value={this.state.formData.description} onChange={handleChange} />
                </CustomForm.Field>
                <Button type='submit'>Submit</Button>
            </CustomForm>
        )
    }
}

class WrappedJsonSchemaComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: props.formData || {}
        }
    }
    render() {
        const { saveModelFormData, createModelFormData, AbModelInstance, saveCallback, errorCallback } = this.props;
        const saveSuccessCallBack = (event, data) => {
            this.setState({
                formData: {}
            }, saveCallback(event, data))
            console.log("WrappedJsonSchemaComponent save and edit callback data EVENT", event);
            console.log("WrappedJsonSchemaComponent save and edit callback data DATA", data);
        }

        const saveErrorCallBack = (event, error) => {
            console.log("WrappedJsonSchemaComponent save and edit error callback data EVENT", event);
            console.log("WrappedJsonSchemaComponent save and edit error callback data DATA", error);
            errorCallback(event, error)
        }

        const schema = {
            title: "TodoLists",
            type: "object",
            properties: {
                name: {
                    type: "string",
                    title: "Task Name",
                    "minLength": 5,
                    "required": true,
                    "errorMsgs": {
                        "required": "Task name is required",
                        "minLength": "Please enter more than 5 character."
                    }
                },
                description: {
                    type: "string",
                    title: "Task Description",
                    "required": true,
                    "errorMsgs": {
                        "required": "Task description is required"
                    }
                },
                userId: {
                    type: "string",
                    title: "Task By User"
                }
            }
        };
        // if (this.state.formData.id) {
        //     schema["properties"]["id"] = {
        //         id: {
        //             type: "string",
        //             title: "Id",
        //         }
        //     }
        // }

        const uiSchema = {
            "name": {
                "ui:placeholder": "Task Name"
            },
            "description": {
                "ui:placeholder": "Task Description"
            },
            "userId": {    //it will load user list from database(prefilled from database when it load) To make it auto complete set async option to true
                "ui:widget": "AbSyncSelect",
                "ui:options": {
                    optionModel: "todousers",
                    labelKey: "email",
                    valueKey: "id",
                    searchable: true,
                    selectPlaceholder: "Select User..",
                    noResultsText: "No User found",
                    async: true,
                    appbackApi: appbackApi,
                    customFilter: { "disabled": false },
                    matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
                    filterOnFields: ["email", "id"],
                    cache: false,
                }
            },
        }

        const onFormChange = (formData) => {
            console.log("form data change event callback from Form", formData);
            // this.setState({ formData });
        }

        const onFormSubmit = ({ schema, formData }) => {
            // let finalData = _.pick(this.state.formData, _.keys(formData));
            let finalData = filterFormDataBasedOnSchema(schema, formData)
            if (formData.id) {
                finalData["id"] = formData.id;
            }
            let payloadData = {
                formMetaData: { formData: finalData },
                AbModelInstance: AbModelInstance,
                saveCallback: saveSuccessCallBack,
                errorCallback: saveErrorCallBack,
                transformData: false
            }
            if (this.state.formData.id) {
                saveModelFormData(payloadData);
            } else {
                createModelFormData(payloadData);
            }
        }

        return (
            <Form schema={schema}
                uiSchema={uiSchema}
                onChange={(data) => onFormChange(data)}
                onSubmit={(data) => onFormSubmit(data)}
                onError={(error) => console.log(error)}
                formData={this.state.formData}
                className="ui form"
                widgets={widgets}
            >
                <div>
                    <button type="submit" ref="formSubmitBtn" className="ui button">Submit</button>
                </div>
            </Form>
        )
    }
}

class WrappedJsonSchemaTransformComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // formData: props.formData || {},
            schema: {
                title: "TodoLists",
                type: "object",
                properties: {
                    taskDetails: {
                        title: "",
                        type: "object",
                        classNames: "three fields",
                        transform: "out",
                        properties: {
                            name: {
                                type: "string",
                                title: "Task Name",
                                "minLength": 5,
                                "required": true,
                                "errorMsgs": {
                                    "required": "Task name is required",
                                    "minLength": "Please enter more than 5 character."
                                }
                            },
                            description: {
                                type: "string",
                                title: "Task Description",
                                "required": true,
                                "errorMsgs": {
                                    "required": "Task description is required"
                                }
                            },
                            userId: {
                                type: "string",
                                title: "Task By User"
                            }
                        }
                    },
                }
            },
            uiSchema: {
                "taskDetails": {
                    "name": {
                        "ui:placeholder": "Task Name"
                    },
                    "description": {
                        "ui:placeholder": "Task Description"
                    },
                    "userId": {    //it will load user list from database(prefilled from database when it load) To make it auto complete set async option to true
                        "ui:widget": "AbSyncSelect",
                        "ui:options": {
                            optionModel: "todousers",
                            labelKey: "email",
                            valueKey: "id",
                            searchable: true,
                            selectPlaceholder: "Select User..",
                            noResultsText: "No User found",
                            async: true,
                            appbackApi: appbackApi,
                            customFilter: { "disabled": false },
                            matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
                            filterOnFields: ["email", "id"],
                            cache: false,
                        }
                    }
                }
            }
        }
        this.state.formData = transformFieldsValueBefore(this.state.schema, props.formData);
        if (props.formData.id) {
            this.state.formData["id"] = props.formData.id;
        }
    }
    render() {
        const { saveModelFormData, createModelFormData, AbModelInstance, saveCallback, errorCallback } = this.props;
        const saveSuccessCallBack = (event, data) => {
            this.setState({
                formData: {}
            }, saveCallback(event, data))
            console.log("WrappedJsonSchemaTransformComponent save and edit callback data EVENT", event);
            console.log("WrappedJsonSchemaTransformComponent save and edit callback data DATA", data);
        }

        const saveErrorCallBack = (event, error) => {
            console.log("WrappedJsonSchemaTransformComponent save and edit error callback data EVENT", event);
            console.log("WrappedJsonSchemaTransformComponent save and edit error callback data DATA", error);
            errorCallback(event, error)
        }

        const onFormChange = (formData) => {
            console.log("form data change event callback from Form", formData);
            // this.setState({ formData });
        }

        const onFormSubmit = ({ schema, formData }) => {
            let finalData = filterFormDataBasedOnSchema(schema, formData)
            const { transformFieldsDetail, transformFlagDetail } = getFormTransformList(schema)
            if (formData.id) {
                finalData["id"] = formData.id;
            }
            let payloadData = {
                formMetaData: { formData: finalData, schema },
                AbModelInstance: AbModelInstance,
                saveCallback: saveSuccessCallBack,
                errorCallback: saveErrorCallBack,
                transformFieldsDetail,
                transformFlagDetail,
                transformData: true
            }
            if (this.state.formData.id) {
                saveModelFormData(payloadData);
            } else {
                createModelFormData(payloadData);
            }
        }

        return (
            <Form schema={this.state.schema}
                uiSchema={this.state.uiSchema}
                onChange={(data) => onFormChange(data)}
                onSubmit={(data) => onFormSubmit(data)}
                onError={(error) => console.log(error)}
                formData={this.state.formData}
                className="ui form"
                widgets={widgets}
            >
                <div>
                    <button type="submit" ref="formSubmitBtn" className="ui button">Submit</button>
                </div>
            </Form>
        )
    }
}

class FormWithWrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            key: new Date().getTime(),
            formData: {}
        }
    }

    errorCallback(error) {
        console.log("Error from component", error);
    }

    render() {
        let self = this;
        const { appbackApi } = this.props;

        const actionButtonHandler = (event, rowData) => {
            console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
            if (event === "edit") {
                this.setState({
                    formData: rowData
                })
            }
        }

        const saveSuccessCallBack = (event, data) => {
            this.setState({
                key: new Date().getTime(),
                formData: {}
            })
            console.log("save and edit callback data EVENT", event);
            console.log("save and edit callback data DATA", data);
        }

        const saveErrorCallBack = (event, error) => {
            console.log("save and edit error callback data EVENT", event);
            console.log("save and edit error callback data DATA", error);
        }

        let todoColumnMetaData = [
            {
                "columnName": "name",
                "order": 1,
                "locked": false,
                "visible": true,
                "displayName": "Task Name",
                "filter": true,
            },
            {
                "columnName": "description",
                "order": 2,
                "locked": false,
                "visible": true,
                "displayName": "Description",
            },
            {
                "columnName": "createdAt",
                "order": 4,
                "locked": false,
                "visible": true,
                "displayName": "Created At",
                "type": COLUMNTYPE.DATE,
                "typeOptions": {
                    "format": "DD MMM YYYY hh:mm:ss A" //format is from moment js, url: http://momentjs.com/docs/#/parsing/string-format/ 
                }
            },
            {
                "columnName": "updatedAt",
                "order": 5,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Operation",
                "type": COLUMNTYPE.ACTIONS,
                "typeOptions": {
                    "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
                    "clickHandler": actionButtonHandler,
                    "buttons": [
                        {
                            "label": "Archive",
                            "key": "archive",
                            "icon": "archive"
                        }
                    ]
                }
            },
            {
                "columnName": "todouser.email",
                "order": 3,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Task By User",
                "includeModel": "todouser",
                "type": COLUMNTYPE.LINK,
                "typeOptions": {
                    "path": "/todouser" //make it path
                }
            }
        ]

        const SemanticFormComponent = FormWrapper({
            modelName: "TodoLists",
            schema: {},
            uiSchema: {},
            appbackApi: appbackApi,
            // saveCallback: saveSuccessCallBack,
            // errorCallback: saveErrorCallBack,
            // onformDataChange: (data) => self.onFormChange(data)
        })(WrappedSemanticComponent)

        const JsonSchemaFormComponent = FormWrapper({
            modelName: "TodoLists",
            schema: {},
            uiSchema: {},
            appbackApi: appbackApi,
        })(WrappedJsonSchemaComponent)

        const JsonSchemaFormTransformComponent = FormWrapper({
            modelName: "TodoLists",
            schema: {},
            uiSchema: {},
            appbackApi: appbackApi,
        })(WrappedJsonSchemaTransformComponent)

        return (
            <div>
                <Segment color='red'>
                    <h3>EXAMPLE INCLUDES:</h3>
                    <ul>
                        <li>Basic usage of FormWrapper with Custom Form.</li>
                        <li>How to use Custom Form(semantic-ui-react) with a wrapper component</li>
                        <li>Create new record for perticular model.</li>
                        <li>Demonstrated edit feature of form by click edit button in show grid.</li>
                    </ul>
                </Segment>
                <Segment color="red">
                    <AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} key={this.state.key} pageSize={5} />
                </Segment>
                <Header as='h2' attached='top'>
                    Semantic Form
                </Header>
                <Segment color="red" attached>
                    <SemanticFormComponent formData={this.state.formData} saveCallback={saveSuccessCallBack} errorCallback={saveErrorCallBack} />
                </Segment>
                <Header as='h2' attached='top'>
                    Json Schema Form
                </Header>
                <Segment color="red" attached>
                    <JsonSchemaFormComponent formData={this.state.formData} saveCallback={saveSuccessCallBack} errorCallback={saveErrorCallBack} />
                </Segment>
                <Header as='h2' attached='top'>
                    Json Schema Form With Custom Layout
                </Header>
                <Segment color="red" attached>
                    <JsonSchemaFormTransformComponent formData={this.state.formData} saveCallback={saveSuccessCallBack} errorCallback={saveErrorCallBack} />
                </Segment>
            </div>
        );

    }
}

export default FormWithWrapper