import React, { Component } from 'react';
import AppbackApi from "appback-api";
import { AbModelConstants, AbModelForm, AbFormWidgets } from 'appback-react'
import { Segment, Container, Grid } from 'semantic-ui-react'
import { geoData } from "../data.js";
const { ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE, SELECT_MULTI_OPTIONS } = AbModelConstants;
const { AbSyncSelectWidget, AbImportWidget } = AbFormWidgets;

class AbWidgetsDemo extends Component {
  constructor(props) {
    super(props);
  }

  submitButtonClick() {
    this.refs.myModelForm.getWrappedInstance().submitFormClick();
  }

  cancelButtonClick() {
    this.refs.myModelForm.getWrappedInstance().cancelFormClick();
  }

  formErrorCallback(action, error) {
    console.log("error From form action", action);
    console.log("error From form error", error);
  }
  onFormChange(data) {
    console.log("form data change event callback from AbModelForm", data);
  }

  importSuccessCallback(response) {
    console.log("Import success: ", response.message);
  }

  importErrorCallback(error) {
    console.log("Import Error: ", error.message);
  }

  render() {
    let self = this;
    const { appbackApi } = this.props;
    let isLoggedIn = appbackApi.isLoggedIn();

    const actionButtonHandler = (event, rowData) => {
      console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
    }

    const saveSuccessCallBack = (event, data) => {
      console.log("save and edit callback data EVENT", event);
      console.log("save and edit callback data DATA", data);
    }

    const renderValue = (option) => {
      return (<div><strong >{option.name}</strong>-{option.description}</div>)
    }

    const schema = {
      title: " ",
      type: "object",
      properties: {
        countryName: { type: "string", title: "1. AbSelect", default: 'US' },
        multiCountryNameString: { type: "string", title: "2. AbSelect (with multiple option selection) will return Comma seperated string as output" },
        multiCountryNameArray: { type: "string", title: "3. AbSelect (with multiple option selection) will return array of selected list" },
        userId: { type: "string", title: "4. AbSyncSelect" },
        userId2: { type: "string", title: "5. AbAsyncSelect" },
        abSyncDataFromMethod: { type: "string", title: "6. AbSyncSelect with custom method to load data" },
        abAsyncDataFromMethod: { type: "string", title: "7. AbAsyncSelect with custom method to load data" },
        abModelCustomText: { type: "string", title: "8. Showing custom rendered text in options" },
        normalCheckbox: { type: "boolean", title: "9. AbCheckbox ( Normal )", default: false },
        toggleCheckbox: { type: "boolean", title: "10. AbCheckbox ( Toggle )", default: false },
        sliderCheckbox: { type: "boolean", title: "11. AbCheckbox ( Slider )", default: false },
        numberEnumRadio: {
          type: "number",
          title: "12. AbRadio",
          enum: [
            1,
            2,
            3
          ],
          enumNames: ["Apple", "Mango", "Orange"]
        },
        "multipleChoicesList": {
          "type": "array",
          "title": "13. AbCheckboxes (multiple checkbox list)",
          "items": {
            "type": "string",
            "enum": [
              "foo",
              "bar",
              "fuzz",
              "qux"
            ],
            enumNames: ["one", "two", "three", "Four"]
          },
          "uniqueItems": true
        },
        datePickerBox: { type: "string", title: "14. Datetime Picker" },
        amount: { type: "number", title: "Amount" }
      }
    };

    const country = [
      { value: 'AU', label: "Australia" },
      { value: 'US', label: "United States of America" },
    ];

    const uiSchema = {
      countryName: {
        "ui:widget": "AbSelect",
        "ui:options": {
          selectOptions: country,
          labelKey: "label",
          valueKey: "value",
          searchable: true,
          selectPlaceholder: "Select country..",
          noResultsText: "No states found",
        }
      },
      multiCountryNameString: {
        "ui:widget": "AbSelect",
        "ui:options": {
          selectOptions: country,
          labelKey: "label",
          valueKey: "value",
          searchable: true,
          selectPlaceholder: "Select country..",
          noResultsText: "No states found",
          multi: true,
          // delimiter:'%'
        }
      },
      multiCountryNameArray: {
        "ui:widget": "AbSelect",
        "ui:options": {
          selectOptions: country,
          labelKey: "label",
          valueKey: "value",
          searchable: true,
          selectPlaceholder: "Select country..",
          noResultsText: "No states found",
          multi: true,
          multiOptions: {
            outputAs: SELECT_MULTI_OPTIONS.ARRAY
          }
        }
      },
      userId: {    //it will load user list from database(prefilled from database when it load) To make it auto complete set async option to true
        "ui:widget": "AbSyncSelect",
        "ui:options": {
          optionModel: "todousers",
          labelKey: "email",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Select User..",
          noResultsText: "No User found",
          appbackApi: appbackApi,
          customFilter: { "disabled": false },
          matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
          filterOnFields: ["email", "id"],
          cache: false,
        }
      },
      userId2: {    //it will load user list from database(prefilled from database when it load) To make it auto complete set async option to true
        "ui:widget": "AbAsyncSelect",
        "ui:options": {
          optionModel: "todousers",
          labelKey: "email",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Type 'user'",
          noResultsText: "No User found",
          appbackApi: appbackApi,
          customFilter: { "disabled": false },
          matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
          filterOnFields: ["email", "id"],
          cache: false,
          autoload: false
        }
      },
      abSyncDataFromMethod: {    //Example of dropdown source of data from custom api
        "ui:widget": "AbSyncSelect",
        "ui:options": {
          optionModel: "TodoLists",
          labelKey: "name",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Select User..",
          noResultsText: "No User found",
          async: true,
          appbackApi: appbackApi,
          matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
          filterOnFields: ["name", "description"],
          dataSourceType: GRID_DATA_SOURCE_TYPE.METHOD,
          dataSourceOptions: { methodName: "getLists", data: {}, params: {}, options: { type: "get" } },
        }
      },
      abAsyncDataFromMethod: {    //Example of dropdown source of data from custom api
        "ui:widget": "AbAsyncSelect",
        "ui:options": {
          optionModel: "TodoLists",
          labelKey: "name",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Type 'user'",
          noResultsText: "No User found",
          appbackApi: appbackApi,
          matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
          filterOnFields: ["name", "description"],
          dataSourceType: GRID_DATA_SOURCE_TYPE.METHOD,
          dataSourceOptions: { methodName: "getLists", data: {}, params: {}, options: { type: "get" } },
          autoload: false
        }
      },
      abModelCustomText: {
        "ui:widget": "AbSyncSelect",
        "ui:options": {
          optionModel: "abmodels",
          labelKey: "name",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Select Model..",
          noResultsText: "No model found",
          appbackApi: appbackApi,
          optionRenderer: renderValue,
          valueRenderer: renderValue
        }
      },
      normalCheckbox: {
        "ui:widget": "AbCheckbox",
        "ui:options": {
          "label": "Default checkbox"
        }
      },
      toggleCheckbox: {
        "ui:widget": "AbCheckbox",
        "ui:options": {
          "label": "Toggle checkbox",
          "checkboxType": ABCHECKBOXTYPE.TOGGLE
        }
      },
      sliderCheckbox: {
        "ui:widget": "AbCheckbox",
        "ui:options": {
          "label": "Slider checkbox",
          "checkboxType": ABCHECKBOXTYPE.SLIDER
        }
      },
      numberEnumRadio: {
        "ui:widget": "AbRadio",
        "ui:options": {
          inline: true,
          radioName: "numradio"
        }
      },
      multipleChoicesList: {
        "ui:widget": "AbCheckboxes",
        "ui:options": {
          inline: false,
        }
      },
      datePickerBox: {
        "ui:widget": "AbDateTimePicker",
        "ui:readonly": false,
        "ui:placeholder": "Select Deleted date (MMM DD YYYY hh:mm A)",
        // "ui:disabled": true,
        "ui:options": {
          // targetTimeZone: "GMT-08:00:US/Pacific", // target timezone which will be append at end of date, if not given then it will consider your timezone
          className: "customClass", //You can provide class here, using this you can customize calender css
          // displayDateFormat: false,  //set this false for Time only picker, you can provide format for date here as well for display purpose
          // displayTimeFormat: false,  //set this false for Date only picker, you can provide format for time here as well for display purpose
          // minDate: new Date(), //minimum valid date to be displayed in the datepicker
          // maxDate: new Date() //maximum valid date to be displayed in the datepicker
          clearable: true,
          // displayDateFormat:'MM/DD/YYYY'
        }
      },
      amount: {
        "ui:widget": "AbMaskedInput",
        "ui:placeholder": "Amount",
        "ui:options": {
          "icon": "dollar",
          "iconPosition": "left",
          "cleaveOptions": { //check https://github.com/nosir/cleave.js/ for other masking options
            "numeral": true,
            "numeralThousandsGroupStyle": 'thousand'
          }
        }
      }
    }
    let opt = {
      optionModel: "todousers",
      labelKey: "email",
      valueKey: "id",
      searchable: true,
      selectPlaceholder: "Select User..",
      noResultsText: "No User found",
      appbackApi: appbackApi,
      cache: false,
    }
    return (
      <div>
        <Segment color='red'>
          <h3>EXAMPLE INCLUDES:</h3>
          <ol>
            <li>It will load passed data in dropdown (you can pass dataset to load it dropdown) Will return Comma seperated string as output eg."india,US,USA"</li>
            <li>It will load passed data in dropdown (you can pass dataset to load it dropdown) Will return Array as output eg.["india","US","USA"](Refer config for this)</li>
            <li>
              Provide <b>multi:true</b> options in ui:options to make multiple option to be selected
              <ul>
                <li>Default multiple value will be split by ',', but you can pass your own delimiter in ui:options for eg: <b>delimiter:'%'</b>, and it will split using '%'</li>
              </ul>
            </li>

            <li>
              AbSyncSelect:
              <ul>
                <li>It will load <b>todousers</b> model data in dropdown from database</li>
                <li>Filter on list of fields on typing in dropdown</li>
                <li>CustomFilter which will apply while fetching data from server</li>
                <li>Support multi select as well using <b>multi:true</b> option</li>
              </ul>
            </li>
            <li>
              AbAsyncSelect:
              <ul>
                <li>It will load <b>todousers</b> model data in dropdown based on typing in dropdown (type user in dropdown to see result)</li>
                <li>Filter on list of fields on typing in dropdown</li>
                <li>You can use <b>autoload</b> option for preload all data by setting it to <b>true</b></li>
                <li>CustomFilter which will apply while fetching data from server</li>
                <li>Support multi select as well using <b>multi:true</b> option</li>
              </ul>
            </li>
            <li>AbSyncSelect : Load data using custom method instead of using modelName</li>
            <li>AbAsyncSelect : Load data using custom method instead of using modelName</li>
            <li>We can show custom content by providing render method in select</li>
            <li>Normal checkbox</li>
            <li>Toggel Checkbox (Provide <b>checkboxType = ABCHECKBOXTYPE.TOGGLE</b> in <b>ui:options</b> ).</li>
            <li>Slider Checkbox (Provide <b>checkboxType = ABCHECKBOXTYPE.SLIDER</b> in <b>ui:options</b> ).</li>
            <li>AbRadio you can pass <b>radioType</b>  =  ABCHECKBOXTYPE.TOGGLE or  ABCHECKBOXTYPE.SLIDER  to look like toggle or radio. (Provide <b>radioType </b> with suitable option(TOGGLE ? SLIDER) in  <b>ui:options</b> )</li>
            <li>
              AbCheckboxes (multiple checkbox list):
              <ul>
                <li>It will generate list of checkbox</li>
                <li>You can show it inline as well by passing <b>inline = true</b> in ui options</li>
                <li>You can also show Toggle or slider checkbox by Providing <b>checkboxType</b> option, same as AbCheckbox</li>
                <li>It will provide comma seperated list of checked checkbox value</li>
              </ul>
            </li>
            <li>
              AbDateTimePicker :
              <ul>
                <li>It will render datetimepicker</li>
                <li>Support date only and time only picker as well please refer this example schema code to know more</li>
                <li>You can provide <b>targetTimeZone</b> prop to save it or retrive as perticular timezone</li>
                <li>You need to validate datetime from your from validate method. You can find code in <b>basic_form.js</b> file source.</li>
              </ul>
            </li>
            <li>
              AbInput :
              <ul>
                <li>For displaying icon in the input box</li>
              </ul>
            </li>
          </ol>
        </Segment>
        <Segment color="red">
          <AbModelForm modelName="TodoLists" ref="myModelForm" schema={schema} uiSchema={uiSchema} appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack} errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)} />
        </Segment>
        <Segment color="blue">
          <h3>Use widget as standalone component without from:</h3>
          <AbSyncSelectWidget schema={{}} id="ab" required={false} options={opt} onChange={(data) => console.log("daatatatat", data)} />
        </Segment>
        <Segment color="blue">
          <h3>Import Component used as standalone component:</h3>
          <AbImportWidget importUrl="http://localhost:4000/api/ListItems/import" successCallback={this.importSuccessCallback} errorCallback={this.importErrorCallback} />
        </Segment>
      </div>
    );

  }
}

export default AbWidgetsDemo