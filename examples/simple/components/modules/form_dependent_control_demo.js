import React, { Component } from 'react';
import AppbackApi from "appback-api";
import { AbModelConstants, AbModelForm } from 'appback-react'
import { Segment, Container, Grid } from 'semantic-ui-react'
import { geoData } from "../data.js";
const { ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE } = AbModelConstants;

class AbModelFormDependentDemo extends Component {
  constructor(props) {
    super(props);
  }

  submitButtonClick() {
    this.refs.myModelForm.getWrappedInstance().submitFormClick();
  }

  cancelButtonClick() {
    this.refs.myModelForm.getWrappedInstance().cancelFormClick();
  }

  formErrorCallback(action, error) {
    console.log("error From form action", action);
    console.log("error From form error", error);
  }
  onFormChange(data) {
    console.log("form data change event callback from AbModelForm", data);
  }

  render() {
    let self = this;
    const { appbackApi } = this.props;
    let isLoggedIn = appbackApi.isLoggedIn();

    const actionButtonHandler = (event, rowData) => {
      console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
    }

    const saveSuccessCallBack = (event, data) => {
      console.log("save and edit callback data EVENT", event);
      console.log("save and edit callback data DATA", data);
    }

    const schema = {
      title: " ",
      type: "object",
      properties: {
        normalCheckbox: { type: "boolean", title: "1. Check to enable textbox and uncheck to disable textbox", default: false },
        firstName: {
          type: "string",
          title: " ",
          "condition": {
            type: FORM_CONDITION_TYPE.VISIBILITY, //condition to disable or hide this control based on "normalCheckbox" field value
            dependentField: "normalCheckbox", //Field name that we given in form schema on which our field is dependent for action
            valueCheck: true, //value that we will be comparing with dependent field value for perform action on this field
            action: FORM_CONDITION_ACTION.DISABLED, //DISABLED: will disable field, HIDDEN: will hide field
          }
        },
        normalCheckbox2: { type: "boolean", title: "2. Check to show textbox and uncheck to hide textbox", default: false },
        lastName: {
          type: "string",
          title: " ",
          "condition": {
            type: FORM_CONDITION_TYPE.VISIBILITY, //condition to disable or hide this control based on "normalCheckbox" field value
            dependentField: "normalCheckbox2", //Field name that we given in form schema on which our field is dependent for action
            valueCheck: true, //value that we will be comparing with dependent field value for perform action on this field
            action: FORM_CONDITION_ACTION.HIDDEN, //DISABLED: will disable field, HIDDEN: will hide field
          }
        },
        address: { type: "string", title: "3. Show textbox if it has any value else hide textbox" },
        addressDependent: {
          type: "string",
          title: " ",
          "condition": {
            type: FORM_CONDITION_TYPE.VISIBILITY, //condition to disable or hide this control based on "normalCheckbox" field value
            dependentField: "address", //Field name that we given in form schema on which our field is dependent for action
            action: FORM_CONDITION_ACTION.HIDDEN, //DISABLED: will disable field, HIDDEN: will hide field
          }
        },
        countryName: { type: "string", title: "4. Country Name (On countryName change it will show state names based on country selected)" },
        stateName: {
          type: "string",
          title: "State Name",
          condition: {
            type: FORM_CONDITION_TYPE.DEPENDENCY, //condition to fill state name dropdown based on country field value
            dependentField: "countryName", //Field name that we given in form schema
            fieldCheck: "countryName", //Field name that we will be comparing in state dataset for perticular country value
            action: FORM_CONDITION_ACTION.HIDDEN,
          },
        },
        "abModel": {
          "type": "string",
          "title": "5. AbModels (Will load data from database)"
        },
        "abField": {
          "type": "string",
          "title": "AbFields (Will load data from database based on abModel selected value",
          "condition": {
            type: FORM_CONDITION_TYPE.DEPENDENCY, //condition to fill state name dropdown based on country field value
            dependentField: "abModel", //Field name that we given in form schema
            fieldCheck: "modelId", //Field name that we will be comparing in state dataset for perticular country value
            action: FORM_CONDITION_ACTION.HIDDEN,
          },
        }
      }
    };

    const country = [
      { value: 'AU', label: "Australia" },
      { value: 'US', label: "United States of America" },
    ];

    const uiSchema = {
      address: {
        "ui:placeholder": "Enter anything to see textbox"
      },
      addressDependent: {
        "ui:placeholder": "I am visible because dependent has value"
      },
      countryName: {
        "ui:widget": "AbSelect",
        "ui:options": {
          selectOptions: country,
          labelKey: "label",
          valueKey: "value",
          searchable: true,
          selectPlaceholder: "Select country..",
          noResultsText: "No states found",
        }
      },
      stateName: {
        "ui:widget": "AbSelect",
        "ui:disabled": false,
        "ui:options": {
          selectOptions: geoData,
          labelKey: "label",
          valueKey: "value",
          searchable: true,
          selectPlaceholder: "Select states..",
          noResultsText: "No states found",
        }
      },
      "normalCheckbox": {
        "ui:widget": "AbCheckbox",
        "ui:options": {
          "label": "Will enable / disable textbox"
        }
      },
      "normalCheckbox2": {
        "ui:widget": "AbCheckbox",
        "ui:options": {
          "label": "Will show / hide textbox"
        }
      },
      abModel: {
        "ui:widget": "AbSyncSelect",
        "ui:options": {
          optionModel: "abmodels",
          labelKey: "name",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Select Model..",
          noResultsText: "No model found",
          appbackApi: appbackApi
        }
      },
      abField: {
        "ui:widget": "AbSyncSelect",
        "ui:options": {
          optionModel: "abfields",
          labelKey: "name",
          valueKey: "id",
          searchable: false,
          selectPlaceholder: "Select Model..",
          noResultsText: "No model found",
          appbackApi: appbackApi,
          autoload: true,
        }
      }
    }

    return (
      <div>
        <Segment color='red'>
          <h3>EXAMPLE INCLUDES:</h3>
          <ol>
            <li>It's below textbox will be enable on check and disabled on uncheck ( it will check value of checkbox if it is <b>true</b> then only it will enable textbox, you can provide check value in <b>valueCheck</b> attritbute )</li>
            <li>It's below textbox will be shown on check and hide on uncheck</li>
            <li>Enter anything in textbox to see it's below textbox ( it will check that value exits or not based on that it will show or hide textbox, you can also make it enable and disabled)</li>
            <ul>
              <li>We had provided contry data to Country Name Control</li>
              <li>We had provided all country's state data</li>
              <li>State dropdown will show state names based on country selected in country Name</li>
            </ul>
            <li>On AbModels value change, it fetch and load data in AbFields based on Abmodels selected value</li>
          </ol>
        </Segment>
        <Segment color="red">
          <AbModelForm modelName="TodoLists" ref="myModelForm" schema={schema} uiSchema={uiSchema} appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack} errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)} />
        </Segment>
      </div>
    );

  }
}

export default AbModelFormDependentDemo