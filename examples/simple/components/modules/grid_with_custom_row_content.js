import React, { Component } from 'react';
import { AbModelGrid, AbModelConstants } from 'appback-react'
import { Segment } from 'semantic-ui-react'
const { COLUMNTYPE, ACTIONBUTTON } = AbModelConstants;

class GridWithCustomRowContent extends Component {
    constructor(props) {
        super(props);
        this.errorCallback = this.errorCallback.bind(this);
        this.deleteCallback = this.deleteCallback.bind(this);

    }
    errorCallback(error) {
        console.log("Error from component", error);
    }

    deleteCallback(data) {
        console.log("delete callback from grid", data);
    }

    render() {
        const { appbackApi } = this.props;
        let self = this;

        const actionButtonHandler = (event, rowData) => {
            console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
        }
        const saveSuccessCallBack = (event, data) => {
            console.log("save and edit callback data EVENT", event);
            console.log("save and edit callback data DATA", data);
        }
        let todoColumnMetaData = [
            {
                "columnName": "name",
                "order": 1,
                "locked": false,
                "visible": true,
                "displayName": "Task Name",
                "filter": true,
            },
            {
                "columnName": "description",
                "order": 2,
                "locked": false,
                "visible": true,
                "displayName": "Description",
                "customHeaderComponent": () => { return null }
            },
            {
                "columnName": "createdAt",
                "order": 4,
                "locked": false,
                "visible": true,
                "displayName": "Created At",
                "customHeaderComponent": () => { return null },
                "type": COLUMNTYPE.DATE,
                "typeOptions": {
                    "format": "DD MMM YYYY hh:mm:ss A" //format is from moment js, url: http://momentjs.com/docs/#/parsing/string-format/ 
                }
            },
            {
                "columnName": "updatedAt",
                "order": 5,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Operation",
                "customHeaderComponent":()=> {return null},
                "type": COLUMNTYPE.ACTIONS,
                "typeOptions": {
                    "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
                    // "deleteConfirmModalOptions":{
                    //   "header":"Please Confirm",
                    //   "content":"Are sure you want to delete?",
                    //   "confirmButton":{
                    //     text:"Yes",
                    //     icon:"checkmark",
                    //     callback:(data)=>console.log("confirm callback from grid metaData",data)
                    //   },
                    //   "cancelButton":{
                    //     text:"No",
                    //     icon:"remove",
                    //     callback:(data)=>console.log("cancel callback from grid metaData",data)
                    //   }
                    // },
                    "clickHandler": actionButtonHandler,
                    "buttons": [
                        {
                            "label": "Archive",
                            "key": "archive",
                            "icon": "archive"
                        }
                    ]
                }
            },
            {
                "columnName": "todouser.email",
                "order": 3,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Task By User",
                "includeModel": "todouser",
                 "customHeaderComponent":()=> {return null},
                "type": COLUMNTYPE.LINK,
                "typeOptions": {
                    "path": "/todouser" //make it path
                }
            }
        ]
        const CustomRowComponent = (rowMetaData) => {
            let rowData = rowMetaData.data;
            return (<div style={{
                backgroundColor: "#EDEDED",
                border: "1px solid #777",
                padding: 5,
                margin: 10,
            }}>
                <h1>{rowData.name}</h1>
                <ul>
                    <li><strong>description</strong>: {rowData.description}</li>
                    <li><strong>State</strong>: {rowData.stateName}</li>
                </ul>
            </div>)
        }
        const NoDataComponent = React.createClass({
            render: function () {
                return (<div>
                    <h1>No data is available</h1>
                    <a href="http://www.google.com">You can google for more data</a>
                </div>
                );
            }
        });
        return (
            <div>
                <Segment color='red'>
                    <h3>EXAMPLE INCLUDES:</h3>
                    <ul>
                        <li>Provide custom component for rendering custom row data.</li>
                        <li>We had also passed custom no data component which will be rendered when no data will be avaliable in grid, We need to pass component with <b>customNoDataComponent</b> property.</li>
                    </ul>
                </Segment>
                <AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} customRowComponent={CustomRowComponent} customNoDataComponent={NoDataComponent} />
            </div>
        );
    }
}



export default GridWithCustomRowContent