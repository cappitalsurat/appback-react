import React, { Component } from 'react';
import AppbackApi from "appback-api";
import { AbModelConstants, AbModelForm, AbModelGrid } from 'appback-react'
import { Segment } from 'semantic-ui-react'
import { geoData } from "../data.js";
const { COLUMNTYPE, ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE } = AbModelConstants;

class FormWithCustomLayout extends Component {
  constructor(props) {
    super(props);
  }

  errorCallback(error) {
    console.log("Error from component", error);
  }

  loaderCallback(type) {
    console.log("Loader type=======================", type);
  }

  submitButtonClick() {
    this.refs.myModelForm.getWrappedInstance().submitFormClick();
  }

  cancelButtonClick() {
    this.refs.myModelForm.getWrappedInstance().cancelFormClick();
  }

  formErrorCallback(action, error) {
    console.log("error From form action", action);
    console.log("error From form error", error);
  }
  onFormChange(data) {
    console.log("form data change event callback from AbModelForm", data);
  }

  render() {
    let self = this;
    const { appbackApi } = this.props;
    let isLoggedIn = appbackApi.isLoggedIn();

    const actionButtonHandler = (event, rowData) => {
      console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
    }

    const saveSuccessCallBack = (event, data) => {
      console.log("save and edit callback data EVENT", event);
      console.log("save and edit callback data DATA", data);
    }

    let todoUserColumnMetaData = [
      {
        "columnName": "assignedTo",
        "order": 1,
        "locked": false,
        "visible": true,
        "displayName": "Assigned To",
        "filter": false
      },
      {
        "columnName": "branchId",
        "order": 2,
        "locked": false,
        "visible": true,
        "displayName": "Branch Id",
        "filter": false
      },
      {
        "columnName": "updatedAt",
        "order": 3,
        "locked": true,
        "visible": true,
        "sortable": false,
        "displayName": "Operation",
        "type": COLUMNTYPE.ACTIONS,
        "typeOptions": {
          "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete']
          "clickHandler": actionButtonHandler,
          "buttons": [
            {
              "label": "Archive",
              "key": "archive",
              "icon": "archive"
            }
          ]
        }
      }
    ]


    const schema = {
      title: "Transactions",
      type: "object",
      properties: {
        assignRow: {
          type: 'object',
          title: '',
          transform: 'OUT',
          classNames: "three fields",
          properties: {
            assign: { type: 'boolean', title: 'Assign', classNames: 'field', default: false },
            assignedTo: {
              type: "string", title: "Assigned To", classNames: "field",
              "condition": {
                type: FORM_CONDITION_TYPE.VISIBILITY,
                dependentField: { assignRow: 'assign' }, //Send object for dependent field in object
                valueCheck: true,
                action: FORM_CONDITION_ACTION.HIDDEN
              }
            },
          }
        },
        statusDetails: {
          title: " ",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            transactionTypeId: { type: "string", title: "Transaction Type", classNames: "field" },
            status: { type: "string", title: "Transaction Status", classNames: "field" },
            birthDate: {
              type: 'string',
              title: 'Birth Date*',
              classNames: 'field editable-datepicker',
              required: true,
              errorMsgs: {
                required: 'BirthDate is required.'
              }
            },
          }
        },
        propertyDetailsRow1: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            propertyAddress: { type: "string", title: "Property Address", classNames: "field" },
            yearBuilt: { type: "string", title: "Year Built", classNames: "field" },
            mlsId: { type: "string", title: "MLSID", classNames: "field" }
          }
        },
        address: {
          title: "",
          type: "object",
          transform: "NOT",//Address object will not be transformed...city, state, zipcode will be now saved in address object
          properties: {
            propertyDetailsRow2: {
              title: "",
              type: "object",
              classNames: "three fields",
              transform: "out",
              properties: {
                city: { type: "string", title: "City", classNames: "field" },
                state: { type: "string", title: "State", classNames: "field" },
                zipCode: { type: "string", title: "Zip code", classNames: "field" }
              }
            }
          }
        },
        propertyDetailsRow3: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            county: { type: "string", title: "County", classNames: "field" },
            country: { type: "string", title: "Country", classNames: "field" }
          }
        },
        propertyDetailsRow4: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            propertyType: { type: "string", title: "Property Type", classNames: "field" },
            squareFootage: { type: "string", title: "Square Footage", classNames: "field" },
            lotSize: { type: "string", title: "Lot Size", classNames: "field" }
          }
        },
        propertyDetailsRow5: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            bedrooms: { type: "string", title: "Bedrooms", classNames: "field" },
            bathrooms: { type: "string", title: "Bathrooms", classNames: "field" },
            halfBathrooms: { type: "string", title: "Half-Bathrooms", classNames: "field" }
          }
        },
        salesDetailsRow1: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            salesPrice: { type: "number", title: "Sales Price", classNames: "field" },
            listPrice: { type: "number", title: "List Price", classNames: "field" },
            listingDate: { type: "string", format: "date", title: "Listing Date", classNames: "field" }
          }
        },
        salesDetailsRow2: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            initialEscrowDepositAmount: { type: "number", title: "Initial Escrow Deposite Amount", classNames: "field" },
            additionalEscrowDepositAmount: { type: "number", title: "Additional Escrow Deposite Amount", classNames: "field" },
            additionalEscrowDepositDate: { type: "string", format: "date", title: "Additional Escrow Deposite Date", classNames: "field" }
          }
        },
        salesDetailsRow3: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            initialEscrowDepositDate: { type: "string", format: "date", title: "Initial Escrow Deposite Date", classNames: "field" },
            listingExpirationDate: { type: "string", format: "date", title: "Listing Expiration Date", classNames: "field" },
            escrowDepositHeldBy: { type: "string", title: "Escrow Deposite Held By", classNames: "field" }
          }
        },
        salesDetailsRow4: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            hoaDocumentDate: { type: "string", format: "date", title: "HOA Document Date", classNames: "field" },
            inspectionContingencyDate: { type: "string", format: "date", title: "Inspection Contingency Date", classNames: "field" },
            appraisalContingencyDate: { type: "string", format: "date", title: "Appraisal Contingency Date", classNames: "field" }
          }
        },
        salesDetailsRow5: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            contractEffectiveDate: { type: "string", format: "date", title: "Contract Effective Date", classNames: "field" },
            financingContingencyDate: { type: "string", format: "date", title: "Financing Contingency Date", classNames: "field" },
          }
        },
        salesDetailsRow6: {
          title: "",
          type: "object",
          classNames: "three fields",
          transform: "out",
          properties: {
            closingCostRecord: { type: "string", title: "Closing Cost Record", classNames: "field" },
            lastTermiteReportDate: { type: "string", format: "date", title: "Last Termite Report Date", classNames: "field" },
          }
        },
        statusHistory: {
          "type": "array",
          "title": "Status History",
          "classNames": "fields",
          "items": {
            "type": "object",
            "properties": {
              "contactStuff": {
                "type": "object",
                "title": " ",
                "classNames": "fields",
                "transform": "out",
                "properties": {
                  "phoneNumber": {
                    "type": "string",
                    "title": "Company Phone",
                    "classNames": "five wide field"
                  },
                  "ext": {
                    "type": "number",
                    "title": "Ext.",
                    "classNames": "three wide field"
                  },
                  "type": {
                    "type": "string",
                    "title": "Type",
                    "classNames": "eight wide field"
                  }
                }
              },
              "branchDetails": {
                "type": "object",
                "title": " ",
                "classNames": "two fields",
                "transform": "out",
                "properties": {
                  "branchId": { type: "string", title: "Branch Id", classNames: "field" },
                  "branchArea": { type: "string", title: "Branch Area", classNames: "field" },
                }
              }
            }
          }
        }
      }
    }

    const uiSchema = {
      statusDetails: {
        transactionTypeId: {
          "ui:widget": "AbSelect",
          "ui:options": {
            selectOptions: [
              { label: "Type 1", value: "Type 1" },
              { label: "Type 2", value: "Type 2" }
            ],
            labelKey: "label",
            valueKey: "value",
            searchable: true,
            selectPlaceholder: "Select Transaction Type",
            noResultsText: "No Transaction Found",
          }
        },
        status: {
          "ui:widget": "AbSelect",
          "ui:options": {
            selectOptions: [
              { label: "Prospect", value: "Prospect" },
              { label: "Client", value: "Client" },
              { label: "Open Escrow", value: "Open Escrow" },
              { label: "Escrow Cancelled", value: "Escrow Cancelled" },
              { label: "Contingencies Removed", value: "Contingencies Removed" },
              { label: "Closing Phase", value: "Closing Phase" },
            ],
            labelKey: "label",
            valueKey: "value",
            searchable: true,
            selectPlaceholder: "Select Transaction Status",
            noResultsText: "No Transaction Status Found",
          }
        },
        birthDate: {
          'ui:widget': 'AbEditableDateTimePicker',
          'ui:placeholder': 'MM/DD/YYYY ',
          'ui:options': {
            displayTimeFormat: 'MM/DD/YYYY',
            clearable: true,
          }
        }
      },
      propertyDetailsRow5: {
        bedrooms: {
          "ui:widget": "AbSelect",
          "ui:options": {
            selectOptions: [
              { label: "1", value: "1" },
              { label: "2", value: "2" },
              { label: "3", value: "3" },
              { label: "4", value: "4" },
              { label: "5", value: "5" }
            ],
            labelKey: "label",
            valueKey: "value",
            searchable: true,
            selectPlaceholder: "Select Bedroom count",
          }
        },
        bathrooms: {
          "ui:widget": "AbSelect",
          "ui:options": {
            selectOptions: [
              { label: "1", value: "1" },
              { label: "2", value: "2" },
              { label: "3", value: "3" },
              { label: "4", value: "4" },
              { label: "5", value: "5" }
            ],
            labelKey: "label",
            valueKey: "value",
            searchable: true,
            selectPlaceholder: "Select Bathroom count",
          }
        },
        halfBathrooms: {
          "ui:widget": "AbSelect",
          "ui:options": {
            selectOptions: [
              { label: "1", value: "1" },
              { label: "2", value: "2" },
              { label: "3", value: "3" },
              { label: "4", value: "4" },
              { label: "5", value: "5" }
            ],
            labelKey: "label",
            valueKey: "value",
            searchable: true,
            selectPlaceholder: "Select Half-Bathrooms count",
          }
        }
      }
    }


    const CustomFieldTemplate = (props) => {
      const { id, classNames, label, help, required, description, errors, children } = props;
      let finalClass = classNames;
      if (props.schema && props.schema.type !== "object" && props.schema.type !== "array" && props.schema.classNames) {
        finalClass = props.schema.classNames
      }
      let customLabel = (<label htmlFor={id}>{label}{required ? "*" : null}</label>);
      if (props.schema && (props.schema.type === "array" || props.schema.type === "object")) {
        customLabel = null;
      }
      return (
        <div className={finalClass}>
          {customLabel}
          {description}
          {children}
          {errors}
          {help}
        </div>
      );
    }


    return (
      <div >
        <Segment color='red'>
          <h3>EXAMPLE INCLUDES:</h3>
          <ul>Loader callback in grid example</ul>
          <ul>Dependency in inline fields</ul>
        </Segment>
        <Segment color="red">
          <AbModelGrid modelName="transactions" columnMetadata={todoUserColumnMetaData} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} loaderCallback={self.loaderCallback} />
        </Segment>
        <Segment color="red">
          <AbModelForm modelName="transactions" ref="myModelForm" schema={schema} uiSchema={uiSchema}
            appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack}
            errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)}
            FieldTemplate={CustomFieldTemplate}
          />
        </Segment>
        <Segment color="red">
          <button onClick={() => this.submitButtonClick()}>Save</button>
          <button onClick={() => this.cancelButtonClick()}>Cancel</button>
        </Segment>
      </div >
    );

  }
}

export default FormWithCustomLayout