import React, { Component } from 'react';
import { AbModelGrid, AbModelConstants } from 'appback-react'
import { Segment } from 'semantic-ui-react'
const { COLUMNTYPE, ACTIONBUTTON, GRID_DATA_SOURCE_TYPE } = AbModelConstants;

class GridWithCustomMethod extends Component {
    constructor(props) {
        super(props);
        this.errorCallback = this.errorCallback.bind(this);
        this.deleteCallback = this.deleteCallback.bind(this);

    }
    errorCallback(error) {
        console.log("Error from component", error);
    }

    deleteCallback(data) {
        console.log("delete callback from grid", data);
    }
    dataSuccessCallback = (data) => {
        console.log('Grid success data', data);
      }

    render() {
        const { appbackApi} = this.props;
        let self = this;

        const actionButtonHandler = (event, rowData) => {
            console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
        }
        const saveSuccessCallBack = (event, data) => {
            console.log("save and edit callback data EVENT", event);
            console.log("save and edit callback data DATA", data);
        }
        let todoColumnMetaData = [
            {
                "columnName": "name",
                "order": 1,
                "locked": false,
                "visible": true,
                "displayName": "Task Name",
                "filter": true,
            },
            {
                "columnName": "description",
                "order": 2,
                "locked": false,
                "visible": true,
                "displayName": "Description",
            },
            {
                "columnName": "createdAt",
                "order": 4,
                "locked": false,
                "visible": true,
                "displayName": "Created At",
                "type": COLUMNTYPE.DATE,
                "typeOptions": {
                    "format": "DD MMM YYYY hh:mm:ss A" //format is from moment js, url: http://momentjs.com/docs/#/parsing/string-format/ 
                }
            },
            {
                "columnName": "updatedAt",
                "order": 5,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Operation",
                "type": COLUMNTYPE.ACTIONS,
                "typeOptions": {
                    "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
                    "clickHandler": actionButtonHandler,
                    "buttons": [
                        {
                            "label": "Archive",
                            "key": "archive",
                            "icon": "archive"
                        }
                    ]
                }
            },
            {
                "columnName": "todouser.email",
                "order": 3,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Task By User",
                "includeModel": "todouser",
                "type": COLUMNTYPE.LINK,
                "typeOptions": {
                    "path": "/todouser" //make it path
                }
            }
        ]
        return (
            <div>
                <Segment color='red'>
                    <h3>EXAMPLE INCLUDES:</h3>
                    <ul>
                        <li>Grid loads data from custom method.</li>
                        <li>CustomGridFilter ( showing records which has task name = "Type C") (It will work with both abmodel and custom methods).</li>
                        <li>Grid dataSuccessCallback example: Will provide network reponse in dataSuccessCallback</li>
                    </ul>
                </Segment>
                <AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} dataSourceType={GRID_DATA_SOURCE_TYPE.METHOD} dataSourceOptions={{ methodName: "getLists", data: {}, params: {}, options: { type: "get" } }} customGridFilter={{ "name": "Type C" }} dataSuccessCallback={self.dataSuccessCallback}/>
            </div>
        );
    }
}

export default GridWithCustomMethod