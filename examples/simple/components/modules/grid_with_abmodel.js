import React, { Component } from 'react';
import { AbModelGrid, AbModelConstants } from 'appback-react'
import { Segment } from 'semantic-ui-react'
const { COLUMNTYPE, ACTIONBUTTON } = AbModelConstants;

class GridWithAbModel extends Component {
    constructor(props) {
        super(props);
        this.errorCallback = this.errorCallback.bind(this);
        this.deleteCallback = this.deleteCallback.bind(this);

    }
    errorCallback(error) {
        console.log("Error from component", error);
    }

    deleteCallback(data) {
        console.log("delete callback from grid", data);
    }

    render() {
        const { appbackApi} = this.props;
        let self = this;

        const actionButtonHandler = (event, rowData) => {
            console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
        }
        const saveSuccessCallBack = (event, data) => {
            console.log("save and edit callback data EVENT", event);
            console.log("save and edit callback data DATA", data);
        }
        let todoColumnMetaData = [
            {
                "columnName": "name",
                "order": 1,
                "locked": false,
                "visible": true,
                "displayName": "Task Name",
                "filter": true,
            },
            {
                "columnName": "description",
                "order": 2,
                "locked": false,
                "visible": true,
                "displayName": "Description",
            },
            {
                "columnName": "createdAt",
                "order": 4,
                "locked": false,
                "visible": true,
                "displayName": "Created At",
                "type": COLUMNTYPE.DATE,
                "typeOptions": {
                    "format": "DD MMM YYYY hh:mm:ss A" //format is from moment js, url: http://momentjs.com/docs/#/parsing/string-format/ 
                }
            },
            {
                "columnName": "updatedAt",
                "order": 5,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Operation",
                "type": COLUMNTYPE.ACTIONS,
                "typeOptions": {
                    "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
                    // "deleteConfirmModalOptions":{
                    //   "header":"Please Confirm",
                    //   "content":"Are sure you want to delete?",
                    //   "confirmButton":{
                    //     text:"Yes",
                    //     icon:"checkmark",
                    //     callback:(data)=>console.log("confirm callback from grid metaData",data)
                    //   },
                    //   "cancelButton":{
                    //     text:"No",
                    //     icon:"remove",
                    //     callback:(data)=>console.log("cancel callback from grid metaData",data)
                    //   }
                    // },
                    "clickHandler": actionButtonHandler,
                    "buttons": [
                        {
                            "label": "Archive",
                            "key": "archive",
                            "icon": "archive"
                        }
                    ],
                    buttonsOrder:["archive",ACTIONBUTTON.VIEW,ACTIONBUTTON.EDIT,ACTIONBUTTON.DELETE] // will arrange button as per given list
                }
            },
            {
                "columnName": "todouser.email",
                "order": 3,
                "locked": true,
                "visible": true,
                "sortable": false,
                "displayName": "Task By User",
                "includeModel": "todouser",
                "type": COLUMNTYPE.LINK,
                "typeOptions": {
                    "path": "/todouser" //make it path
                }
            }
        ]
        return (
            <div>
                <Segment color='red'>
                    <h3>EXAMPLE INCLUDES:</h3>
                    <ul>
                        <li>Basic configuration to load grid using AbModelGrid.</li>
                        <li>Configs for loading data from database model.</li>
                        <li>Basic grid column configuration(Filter column and basic searching).</li>
                        <li>Configs for adding action button in Grid ( DELETE, EDIT & VIEW).</li>
                        <li>You can order action button in grid by passing list of button keys in <b>buttonsOrder</b> prop, see this example code for more.</li>
                        <li>Grid <b>errorCallback</b> and record <b>deleteCallback</b>.</li>
                        <li>Config for columnType and it's typeOption for differnt content rendering.(e.g. Formating date, create link in column data etc....).</li>
                        <li>Modify delete confirmation model by providing own visible text (example code is commented in action column).</li>
                        <li>Display included column data.</li>
                        <li>You can adjust per page row by passing <b>pageSize</b> attribute value.</li>
                    </ul>
                </Segment>
                <AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} pageSize={3}/>
            </div>
        );
    }
}



export default GridWithAbModel