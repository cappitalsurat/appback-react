import React, { Component } from 'react';
import AppbackApi from "appback-api";
import { AbModelConstants, AbModelForm, AbModelGrid } from 'appback-react'
import { Segment } from 'semantic-ui-react'
import { geoData } from "../data.js";
const { COLUMNTYPE, ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE } = AbModelConstants;
import moment from 'moment';
// import FileUploadProgress from './fileUploader';

class BasicForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: new Date().getTime()
    }
  }

  errorCallback(error) {
    console.log("Error from component", error);
  }

  submitButtonClick() {
    // this.buttonRef.click();  //will upload image of file upload widget
    this.refs.myModelForm.getWrappedInstance().submitFormClick();
  }

  // uploadFileExternally() {
  //   console.log(this);
  //   this.refs.uploader1.onSubmit()
  // }

  cancelButtonClick() {
    this.refs.myModelForm.getWrappedInstance().cancelFormClick();
  }

  formErrorCallback(action, error) {
    console.log("error From form action", action);
    console.log("error From form error", error);
  }
  onFormChange(data) {
    console.log("form data change event callback from AbModelForm", data);
  }
  validateForm(formData, errors) {
    if (!moment(formData.deletedAt).isValid()) {
      errors.deletedAt.addError('Enter date in valid format.')
    }
    return errors
  }
  // customFormRenderer(onSubmit, onChange) {
  //   debugger
  //   return (
  //     <form id='customForm' style={{ marginBottom: '15px' }}>
  //       <label style={styles.bslabel} htmlFor="exampleInputFile">File input</label>
  //       <input style={{ display: 'block' }} type="file" name='file' id="exampleInputFile" />
  //       <p style={styles.bsHelp}>This is custom form.</p>
  //       <button type="button" style={styles.bsButton} onClick={onSubmit}>Upload</button>
  //     </form>
  //   );
  // }
  render() {
    let self = this;
    const { appbackApi } = this.props;
    let isLoggedIn = appbackApi.isLoggedIn();

    const actionButtonHandler = (event, rowData) => {
      console.log("callback form app.js Event:= " + event + "\n Rowdata:= " + JSON.stringify(rowData));
    }

    const saveSuccessCallBack = (event, data) => {
      this.setState({
        key: new Date().getTime()
      })
      console.log("save and edit callback data EVENT", event);
      console.log("save and edit callback data DATA", data);
    }

    let todoColumnMetaData = [
      {
        "columnName": "name",
        "order": 1,
        "locked": false,
        "visible": true,
        "displayName": "Task Name",
        "filter": true,
      },
      {
        "columnName": "description",
        "order": 2,
        "locked": false,
        "visible": true,
        "displayName": "Description",
      },
      {
        "columnName": "createdAt",
        "order": 4,
        "locked": false,
        "visible": true,
        "displayName": "Created At",
        "type": COLUMNTYPE.DATE,
        "typeOptions": {
          "format": "DD MMM YYYY hh:mm:ss A" //format is from moment js, url: http://momentjs.com/docs/#/parsing/string-format/ 
        }
      },
      {
        "columnName": "updatedAt",
        "order": 5,
        "locked": true,
        "visible": true,
        "sortable": false,
        "displayName": "Operation",
        "type": COLUMNTYPE.ACTIONS,
        "typeOptions": {
          "defaultButtons": [ACTIONBUTTON.DELETE, ACTIONBUTTON.VIEW, ACTIONBUTTON.EDIT], //show default View and edit buttons, list of buttons keys ['edit', 'view', 'delete'],
          "clickHandler": actionButtonHandler,
          "buttons": [
            {
              "label": "Archive",
              "key": "archive",
              "icon": "archive"
            }
          ]
        }
      },
      {
        "columnName": "todouser.email",
        "order": 3,
        "locked": true,
        "visible": true,
        "sortable": false,
        "displayName": "Task By User",
        "includeModel": "todouser",
        "type": COLUMNTYPE.LINK,
        "typeOptions": {
          "path": "/todouser" //make it path
        }
      }
    ]

    const schema = {
      title: "TodoLists",
      type: "object",
      properties: {
        name: {
          type: "string",
          title: "Task Name",
          "minLength": 5,
          "required": true,
          "errorMsgs": {
            "required": "Task name is required",
            "minLength": "Please enter more than 5 character."
          }
        },
        description: {
          type: "string",
          title: "Task Description",
          "required": true,
          "errorMsgs": {
            "required": "Task description is required"
          }
        },
        userId: { type: "string", title: "Task By User" },
        deletedAt: {
          type: "string",
          title: "Registerd As",
          "required": true,
          "errorMsgs": {
            "required": "Deleted date is required"
          }
        },
        profilePic: {
          type: "string",
          title: "Upload single file",
          "required": true,
          "errorMsgs": {
            "required": "Profile picture is required"
          }
        },
        profilePic2: {
          type: "string",
          title: "Upload Multiple file",
          "required": true,
          "errorMsgs": {
            "required": "Profile picture is required"
          }
        },
        // profilePic2: { type: "string", title: "Upload profile pic" }
        // birthDate: {
        //   type: 'string',
        //   title: 'Birth Date*',
        //   // classNames: 'field',
        //   required: true,
        //   errorMsgs: {
        //     required: 'BirthDate is required.'
        //   }
        // },
        // birthDay: {
        //   "type": "string",
        //   title: 'Birth Day*',
        //   // "format": "date"
        // }
      }
    };

    const uiSchema = {
      userId: {    //it will load user list from database(prefilled from database when it load) To make it auto complete set async option to true
        "ui:widget": "AbSyncSelect",
        "ui:options": {
          optionModel: "todousers",
          labelKey: "email",
          valueKey: "id",
          searchable: true,
          selectPlaceholder: "Select User..",
          noResultsText: "No User found",
          async: true,
          appbackApi: appbackApi,
          customFilter: { "disabled": false },
          matchInputFrom: ABSELECT.ANY, //"ANY" will search partial substring, "START" will search in string from start of string
          filterOnFields: ["email", "id"],
          cache: false,
        }
      },
      deletedAt: {
        "ui:widget": "AbDateTimePicker",
        "ui:readonly": false,
        "ui:placeholder": "MMM DD YYYY hh:mm A",
        // "ui:disabled": true,
        "ui:options": {
          targetTimeZone: "GMT-08:00:US/Pacific", // target timezone which will be append at end of date
          className: "customClass"
          // displayDateFormat: false,  //set this false for Time only picker
          // displayTimeFormat: false,  //set this false for Date only picker
        }
      },
      profilePic: {
        "ui:widget": "AbFileUploader",
        "ui:options": {
          appbackApi: appbackApi,
          folderPath: 'other1',
          typeErrorMsg: 'Please upload jpeg or png image only',
          startUpLoadOnSelect: true, // default true
          fileSize:50,
          // fileSizeErrorMsg: 'Please upload file having less then 1 MB size',
          acceptFileType: ['image/*', 'video/mp4', 'application/*'],
          buttonRef: el => this.buttonRef = el,
          onError: (e, req) => { console.log('file uplode error'); },
          onProgress: (e, req, progress) => { console.log('file upload in progress'); },
          onAbort: (e, request) => { console.log('file uplode abort'); },
          onLoad: (e, request) => { console.log('file upload request on load'); },
          onUpload: (url) => { console.log('Uploaded to url', url); },
          onFileChange: (file) => { console.log('On File change')}
        }
      },
      profilePic2: {
        "ui:widget": "AbMultiFileUploader",
        "ui:options": {
          appbackApi: appbackApi,
          folderPath: 'other1',
          typeErrorMsg: 'Please upload jpeg or png image only',
          startUpLoadOnSelect: true, // default true
          fileSize:50,
          // fileSizeErrorMsg: 'Please upload file having less then 1 MB size',
          acceptFileType: ['image/*', 'video/mp4', 'application/*'],
          buttonRef: el => this.buttonRef = el,
          onError: (e, req) => { console.log('file uplode error'); },
          onProgress: (e, req, progress) => { console.log('file upload in progress'); },
          onAbort: (e, request) => { console.log('file uplode abort'); },
          onLoad: (e, request) => { console.log('file upload request on load'); },
          onUpload: (url) => { console.log('Uploaded to url', url); },
          onFileChange: (file) => { console.log('On File change')}
        }
      }
      // birthDate: {
      //   'ui:widget': 'AbEditableDateTimePicker',
      //   'ui:placeholder': 'MM/DD/YYYY ',
      //   'ui:options': {
      //     displayTimeFormat: 'MM/DD/YYYY',
      //     clearable: true,
      //   }
      // },
      // birthDay: {
      //   "ui:widget": 'AbModularDatePicker',
      //   'ui:options': {
      //     showYear: false, //default value is true
      //     // showDay: false, //default value is true
      //     // showMonth: false //default value is true
      //   }
      // }
    }

    return (
      <div>
        <Segment color='red'>
          <h3>EXAMPLE INCLUDES:</h3>
          <ul>
            <li>Basic usage of AbModelForm.</li>
            <li>How to generate from based on form scheme and uiSchema.</li>
            <li>Create new record for perticular model.</li>
            <li>Demonstrated edit feature of form by click edit button in show grid.</li>
            <li>Basic validation and custom messages for validation.</li>
            <li>Configs for <b>AbSyncSelect</b> widget which will load data in dropdown from database model.</li>
          </ul>
        </Segment>
        <Segment color="red">
          <AbModelGrid modelName="TodoLists" columnMetadata={todoColumnMetaData} colsNotToDisplayInSetting={["todouser", "userId", "id"]} appbackApi={appbackApi} errorCallback={self.errorCallback} deleteCallback={self.deleteCallback} key={this.state.key} pageSize={5} />
        </Segment>
        <Segment color="red">
          <AbModelForm modelName="TodoLists" ref="myModelForm" validate={self.validateForm} schema={schema} uiSchema={uiSchema} appbackApi={appbackApi} showDefaultButtons={false} saveCallback={saveSuccessCallBack} errorCallback={self.formErrorCallback} onformDataChange={(data) => self.onFormChange(data)} />
        </Segment>
        <Segment color="red">
          <button onClick={() => this.submitButtonClick()}>Save</button>
          <button onClick={() => this.cancelButtonClick()}>Cancel</button>
        </Segment>
        {/* <Segment>
            <div>
              <h3>Default style</h3>
              <FileUploadProgress key='ex1' url='http://localhost:3000/api/upload'
                ref='uploader1'
                onProgress={(e, request, progress) => { console.log('progress', e, request, progress); }}
                onLoad={(e, request) => { console.log('load', e, request); }}
                onError={(e, request) => { console.log('error', e, request); }}
                onAbort={(e, request) => { console.log('abort', e, request); }}
                appbackApi={appbackApi}
              />
              <button onClick={this.uploadFileExternally.bind(this)}>External Upload</button>
            </div>
          </Segment> */}

        {/* <Segment>
          <div>
            <h3>Default style</h3>
            <FileUploadProgress key='ex3' url='http://localhost:3000/api/upload'
              onProgress={(e, request, progress) => { console.log('progress', e, request, progress); }}
              onLoad={(e, request) => { console.log('load', e, request); }}
              onError={(e, request) => { console.log('error', e, request); }}
              onAbort={(e, request) => { console.log('abort', e, request); }}
              formRenderer={this.customFormRenderer.bind(this)}
              appbackApi={appbackApi}
            />
          </div>
        </Segment> */}
      </div>
    );

  }
}

export default BasicForm