import React, { Component } from 'react';
import FormWithWidget from './FormComponent'
import _ from 'underscore';
import AppbackApi from "appback-api";
import { AbModelGrid, AbModelConstants, AbModelForm } from 'appback-react'
import { Segment, Container, Grid, Sidebar, Menu, Icon, Header, Image } from 'semantic-ui-react'
import { geoData } from "./data.js";
import './main.css';
import GridWithAbModel from './modules/grid_with_abmodel';
import GridWithCustomMethod from './modules/grid_with_custom_method';
import GridWithAdvanceFilter from './modules/grid_with_advance_filter';
import GridWithCustomRowContent from './modules/grid_with_custom_row_content';
import BasicForm from './modules/basic_form';
import AbFormWithArray from './modules/form_with_array_elements';
import AbWidgetsDemo from './modules/ab_widgets_demo';
import AbModelFormDependentDemo from './modules/form_dependent_control_demo';
import FormWithCustomMethod from './modules/form_with_custom_method';
import FormWithCustomLayout from './modules/form_with_custom_layout';
import FormWithWrapper from './modules/form_with_wrapper';
import FullApp from './modules/App';
import {
  MenuList,
  GRID_WITH_ABMODEL,
  FULL_EXAMPLE,
  GRID_WITH_CUSTOM_METHOD,
  GRID_WITH_ADVANCE_FILTER,
  GRID_WITH_CUSTOM_ROW_CONTENT,
  BASIC_FORM_RENDER,
  FORM_WITH_ARRAY_ELEMENT,
  ABMODELFORM_WIDGET_USAGE,
  ABMODELFORM_DEPENDENT_CONTROLS_DEMO,
  FORM_WITH_CUSTOM_METHOD,
  FORM_WITH_CUSTOM_LAYOUT,
  FORM_WITH_WRAPPER
} from './constants';
const { COLUMNTYPE, ACTIONBUTTON, ABCHECKBOXTYPE, ABSELECT, GRID_DATA_SOURCE_TYPE, FORM_CONDITION_ACTION, FORM_CONDITION_TYPE } = AbModelConstants;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appbackApi: new AppbackApi('http://localhost:4000/api'),
      activeMenu: GRID_WITH_ADVANCE_FILTER,
      sidebarActive: true
    }
    this.handleItemClick = this.handleItemClick.bind(this);
    this.renderSections = this.renderSections.bind(this);
    this.renderMenu = this.renderMenu.bind(this);

  }

  renderMenu() {
    const { handleItemClick } = this;
    const { activeMenu } = this.state
    return MenuList.map(function (menuItem, index) {
      return <Menu.Item name={menuItem.name} onClick={handleItemClick} active={activeMenu === menuItem.name} key={menuItem.name}>
        {menuItem.menuText}
      </Menu.Item>
    });
  }

  renderSections(appbackApi) {
    const { activeMenu } = this.state;
    switch (activeMenu) {
      case GRID_WITH_ABMODEL:
        return <GridWithAbModel appbackApi={appbackApi} />
      case GRID_WITH_CUSTOM_METHOD:
        return <GridWithCustomMethod appbackApi={appbackApi} />
      case GRID_WITH_ADVANCE_FILTER:
        return <GridWithAdvanceFilter appbackApi={appbackApi} />
      case GRID_WITH_CUSTOM_ROW_CONTENT:
        return <GridWithCustomRowContent appbackApi={appbackApi} />
      case BASIC_FORM_RENDER:
        return <BasicForm appbackApi={appbackApi} />
      case FORM_WITH_ARRAY_ELEMENT:
        return <AbFormWithArray appbackApi={appbackApi} />
      case FORM_WITH_CUSTOM_METHOD:
        return <FormWithCustomMethod appbackApi={appbackApi} />
      case ABMODELFORM_WIDGET_USAGE:
        return <AbWidgetsDemo appbackApi={appbackApi} />
      case ABMODELFORM_DEPENDENT_CONTROLS_DEMO:
        return <AbModelFormDependentDemo appbackApi={appbackApi} />
      case FORM_WITH_CUSTOM_LAYOUT:
        return <FormWithCustomLayout appbackApi={appbackApi} />
      case FORM_WITH_WRAPPER:
        return <FormWithWrapper appbackApi={appbackApi} />
      case FULL_EXAMPLE:
        return <FullApp />
      default:
        return <h1>Minddd IT</h1>
    }
  }

  handleItemClick(e, itemObj) {
    this.setState({
      activeMenu: itemObj.name,
    })
  }

  render() {
    const { appbackApi, activeMenu, sidebarActive } = this.state;
    let isLoggedIn = appbackApi.isLoggedIn();
    if (!isLoggedIn) {
      appbackApi.login({ "email": "admin@appback.com", "password": "admin@appback" }).
        then((data) => {
          localStorage.setItem("userData", JSON.stringify(data));
          isLoggedIn = true;
          alert("login success")
        });
    }
    if (!isLoggedIn) {
      return null;
    }
    let self = this;
    return (
      <div id="core-layout">
        <Sidebar.Pushable as={Segment}>
          <Sidebar as={Menu} animation='slide along' width='thin' visible={true} icon='labeled' vertical inverted>
            {self.renderMenu()}
          </Sidebar>
          <Sidebar.Pusher id="main-content">
            <div className='core-layout__viewport container'>
              {
                self.renderSections(appbackApi)
              }
            </div>

          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>


    );

  }
}
