export const GRID_WITH_ABMODEL ='GRID_WITH_ABMODEL'
export const GRID_WITH_CUSTOM_METHOD ='GRID_WITH_CUSTOM_METHOD'
export const FULL_EXAMPLE ='FULL_EXAMPLE'
export const GRID_WITH_ADVANCE_FILTER ='GRID_WITH_ADVANCE_FILTER'
export const GRID_WITH_CUSTOM_ROW_CONTENT ='GRID_WITH_CUSTOM_ROW_CONTENT'
export const BASIC_FORM_RENDER ='BASIC_FORM_RENDER'
export const FORM_WITH_ARRAY_ELEMENT ='FORM_WITH_ARRAY_ELEMENT'
export const ABMODELFORM_WIDGET_USAGE ='ABMODELFORM_WIDGET_USAGE'
export const ABMODELFORM_DEPENDENT_CONTROLS_DEMO = "ABMODELFORM_DEPENDENT_CONTROLS_DEMO"
export const FORM_WITH_CUSTOM_METHOD = "FORM_WITH_CUSTOM_METHOD"
export const FORM_WITH_CUSTOM_LAYOUT = "FORM_WITH_CUSTOM_LAYOUT"
export const FORM_WITH_WRAPPER = "FORM_WITH_WRAPPER"

export const MenuList=[
    {
        "menuText":"Basic AbModel Grid",
        "description":"",
        "name": GRID_WITH_ABMODEL
    },
    {
        "menuText":"Grid With Custom Method & custom filter",
        "description":"",
        "name": GRID_WITH_CUSTOM_METHOD
    },
    {
        "menuText":"Grid Advance Filter",
        "description":"",
        "name": GRID_WITH_ADVANCE_FILTER
    },
    {
        "menuText":"Grid With Custom Row Content",
        "description":"",
        "name": GRID_WITH_CUSTOM_ROW_CONTENT
    },
    {
        "menuText":"Basic Form Render",
        "description":"",
        "name": BASIC_FORM_RENDER
    },
    {
        "menuText":"Different AbWidgets Demo",
        "description":"",
        "name": ABMODELFORM_WIDGET_USAGE
    },
    {
        "menuText":"Form With Array of Controls",
        "description":"",
        "name": FORM_WITH_ARRAY_ELEMENT
    },
    {
        "menuText":"Form With Custom method",
        "description":"",
        "name": FORM_WITH_CUSTOM_METHOD
    },
    {
        "menuText":"Form With Dependent Controls",
        "description":"",
        "name": ABMODELFORM_DEPENDENT_CONTROLS_DEMO
    },
    {
        "menuText":"Form with custom layout",
        "description":"",
        "name": FORM_WITH_CUSTOM_LAYOUT
    },
    {
        "menuText":"Playground",
        "description":"",
        "name": FULL_EXAMPLE
    },
    {
        "menuText":"Form With Wrapper",
        "description":"",
        "name": FORM_WITH_WRAPPER
    },
    
]