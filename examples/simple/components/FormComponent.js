import React from 'react'
import { Input } from 'semantic-ui-react'
import Form from "jb-react-jsonschema-form";
import Select from 'react-select';
import 'react-select/dist/react-select.css';
// import './form.css'

class FormWithWidget extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {

    const getUsers = (input) => {

      if (!input) {
        return Promise.resolve({ options: [] });
      }
      return fetch(`https://api.github.com/search/users?q=${input}`)
        .then((response) => response.json())
        .then((json) => {
          return { options: json.items };
        });
    }

    const schema = {
      "title": "A registration form",
      "description": "A simple form example.",
      "type": "object",
      "required": [
        "firstName",
        "lastName"
      ],
      "properties": {
        "firstName": {
          "type": "string",
          "title": "First name"
        },
        "lastName": {
          "type": "string",
          "title": "Last name"
        },
        "selectedValue": {
          "type": "integer",
          "title": "selectTextDemo"
        },
        "age": {
          "type": "integer",
          "title": "Age"
        },
        "bio": {
          "type": "string",
          "title": "Bio"
        },
        "password": {
          "type": "string",
          "title": "Password",
          "minLength": 3
        },

      }
    }

    const uiSchema = {
      "firstName": {
        "ui:autofocus": true
      },
      "age": {
        "ui:widget": "updown"
      },
      "bio": {
        "ui:widget": "textarea"
      },
      "password": {
        "ui:widget": "password",
        "ui:help": "Hint: Make it strong!"
      },
      "date": {
        "ui:widget": "alt-datetime"
      },
      "selectedValue": {
        "ui:widget": "myCustomWidget",
        "ui:options": {
          multi: false,
          backspaceRemoves: true,
          autoload: true,
          valueKey: "id",
          labelKey: "login",
          loadOptions: getUsers
        }
      }
    }



    const MyCustomWidget = (props) => {
      const { options } = props;
      const { autoload, backspaceRemoves, labelKey, loadOptions, multi, valueKey } = options;
      return (
        <Select.Async multi={multi} simpleValue value={props.value} onChange={props.onChange} valueKey={valueKey} labelKey={labelKey} loadOptions={loadOptions} backspaceRemoves={backspaceRemoves} autoload={autoload} />
      );
    };

    const widgets = {
      myCustomWidget: MyCustomWidget
    };
    MyCustomWidget.defaultProps = {
      options: {
        multi: false,
        backspaceRemoves: true,
        autoload: false,
        valueKey: "value",
        labelKey: "label",
        loadOptions: function () { return { options: [] }; }

      }
    }

    return (

      <Form schema={schema}
        uiSchema={uiSchema}
        onChange={() => { console.log("changed") }}
        onSubmit={(data) => {
          console.log("submitted", data.formData);
          alert(JSON.stringify(data.formData));
        }
        }
        formData={this.state}
        widgets={widgets}
        onError={(err) => { console.log("onError", err) }} className="ui form">
        <div>
          <button type="submit" className="ui button">Submit</button>
          <button className="ui button">Cancel</button>
        </div>

      </Form>


    )
  }
}

export default FormWithWidget

