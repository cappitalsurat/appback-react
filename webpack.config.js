'use strict';

var webpack = require('webpack');

var plugins = [
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
  })
];

if (process.env.NODE_ENV === 'production') {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        screw_ie8: true,
        warnings: false
      }
    })
  );
}

module.exports = {
  module: {
    loaders: [
      {
        test: /\.(js|jsx|json)$/,
        exclude: /node_modules/,
        loaders: ['babel?presets[]=es2015&presets[]=react&presets[]=stage-0'],
      },
      { test: /\.html$/, loader: 'raw' },
      { test: /\.css$/, loader: "style-loader!css-loader" },
      { test: /\.scss$/, loader: "sass-loader!style-loader!css-loader" },
      ]
  },
  output: {
    library: 'appback-react',
    libraryTarget: 'umd'
  },
  plugins: plugins,
  resolve: {
    extensions: ['', '.js', '.css', '.scss']
  }
};
