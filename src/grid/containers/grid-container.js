import React, { Component } from 'react';
import { connect } from 'react-redux';
import AbModelGrid from '../components/grid-component';
import * as AbModelGridAction from '../../actions';
import { GRID_DATA_SOURCE_TYPE } from '../../constants';


const mapStateToProps = (state, ownProps) => {
  const { appbackApi, modelName } = ownProps;
  var mapped = {
    abModelData: state.abModels[modelName],
    AbModelInstance: appbackApi.createModelRest(modelName, null),
    showLoader: (state.abModels[modelName]) ? state.abModels[modelName].showLoader : false
  };
  return mapped;
}
function mapDispatchToProps(dispatch, ownProps) {
  let { modelName, dataSourceType, dataSourceOptions, customGridFilter, errorCallback = null, deleteCallback = null, loaderCallback = null, showPager = true, externalSortColumn, externalSortAscending, exportData, exportConfig, exportStartCallback, exportFinishCallback, dataSuccessCallback = null } = ownProps;
  let initialConfigData = { externalSortColumn, externalSortAscending, exportData, exportConfig };
  dispatch(AbModelGridAction.setModelInitialData(modelName, initialConfigData));

  return {
    getModels: function (configData) {
      if (dataSourceType === GRID_DATA_SOURCE_TYPE.METHOD) {
        return dispatch(AbModelGridAction.customMethodRequest({ ...configData, AbModelName: modelName, customGridFilter, errorCallback, loaderCallback, dataSourceType, dataSourceOptions, showPager, dataSuccessCallback }));
      } else {
        return dispatch(AbModelGridAction.modelsRequest({ ...configData, AbModelName: modelName, customGridFilter, errorCallback, loaderCallback, showPager, dataSuccessCallback }));
      }
    },
    setCurrentActiveItem: function (actionType, itemData) {
      let payloadData = {
        action: actionType,
        currentActiveItem: itemData,
        modelName: modelName
      }
      return dispatch(AbModelGridAction.setCurrentActiveItem(payloadData));
    },
    setAdvanceFilterConfig: function (data) {
      return dispatch(AbModelGridAction.mergeGridConfig({ modelName, data }))
    },
    deleteGridRow: function (rowData) {
      const { data, gridReloadConfigData } = rowData;
      const { AbModelInstance } = gridReloadConfigData;
      return dispatch(AbModelGridAction.DeleteGridRow({ AbModelInstance, data, gridReloadConfigData: { ...gridReloadConfigData, ...{ dataSourceOptions, AbModelName: modelName, customGridFilter }, showPager }, deleteCallback, dataSourceType, errorCallback }))
    },
    resetGridData: function () {
      dispatch(AbModelGridAction.resetGridDataOnly(modelName));
    },
    exportGridData: function (props, format, type) {
      return dispatch(AbModelGridAction.exportGridData({ ...props, format, type, exportStartCallback, exportFinishCallback }));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AbModelGrid);