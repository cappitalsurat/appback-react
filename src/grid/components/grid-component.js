import React, { Component, PropTypes } from 'react';
import { Header, Grid, Container, Image, Button, Form, Input, Select, Menu, Icon, Dropdown, Confirm, Segment } from 'semantic-ui-react'
import Griddle from 'jb-griddle-react';
import { appbackApi, COLUMNTYPE, GRID_DATA_SOURCE_TYPE, ACTIONBUTTON } from '../../constants';
import { LinkRenderer, dataRenderer, actionComponent, OtherPager, OtherFilter } from '../../grid-renderer';
import _ from 'underscore';
import './grid.css'

export default class AbModelGrid extends Component {
    constructor(props) {
        super(props);
        this.customFilterFunction = this.customFilterFunction.bind(this);
        this.setPage = this.setPage.bind(this);
        const onButtonClick = (actionType, data, callback) => {
            if (ACTIONBUTTON.DELETE === actionType) {
                const { pageSize = null } = this.props;
                const { currentPage, resultsPerPage, externalSortColumn, externalSortAscending, externalFilter, advanceFilter } = this.props.abModelData;
                let gridReloadConfigData = {
                    page: currentPage,
                    resultsPerPage: (pageSize) ? pageSize : resultsPerPage,
                    externalSortColumn,
                    externalSortAscending,
                    externalFilter,
                    AbModelInstance: props.AbModelInstance,
                    filters: this.state.filterColumn,
                    include: this.state.includeModels,
                    advanceFilter
                }
                props.deleteGridRow({ data, gridReloadConfigData });
            } else {

                var a = props.setCurrentActiveItem(actionType, data);
                if (callback) {
                    callback(actionType, data);
                }
            }
        }
        let advanceFilterColumnNames = _.chain(props.columnMetadata)
            .filter(function (colMetaData) { return (colMetaData.advanceFilter) ? true : false; })
            .map(function (colMetaData) {
                let colFilterData = colMetaData.advanceFilterOptions;
                colFilterData["columnName"] = colMetaData["columnName"];
                return colFilterData
            })
            .value();
        let filters = _.chain(props.columnMetadata)
            .filter(function (colMetaData) { return (colMetaData.filter) ? true : false; })
            .pluck("columnName")
            .value();
        let includeModels = _.chain(props.columnMetadata)
            .filter(function (colMetaData) { return (colMetaData.includeModel) ? true : false; })
            .pluck("includeModel")
            .uniq()
            .value();
        let external
        let modelCols = [];
        if (props.modelCols) {
            modelCols = props.modelCols;
        } else {
            modelCols = _.pluck(props.columnMetadata, "columnName");
        }
        let columnMetadata = _.map(props.columnMetadata, function (colMetaData) {
            if (!colMetaData.customComponent) {
                if (colMetaData.type === COLUMNTYPE.LINK) {
                    colMetaData["customComponent"] = (props) => LinkRenderer(props);
                } else if (colMetaData.type === COLUMNTYPE.ACTIONS) {
                    colMetaData["customComponent"] = (props) => actionComponent(props, onButtonClick);
                } else {
                    colMetaData["customComponent"] = (props) => dataRenderer(props);
                }
            }
            return colMetaData;
        });

        this.state = {
            firstTimeLoad: true,
            filterColumn: filters,
            includeModels: includeModels,
            modelCols: modelCols,
            columnMetadata: props.columnMetadata,
            advanceFilterConfig: advanceFilterColumnNames,
            currentPage: 0
        }
    }
    getChildContext() {
        return { modelName: this.props.modelName }
    }

    componentWillMount() {
        let { setAdvanceFilterConfig } = this.props;
        let { advanceFilterConfig } = this.state;
        setAdvanceFilterConfig({ advanceFilterConfig });
        if (this.state.firstTimeLoad && this.props.showFilter === false && this.props.abModelData) {
            this.getExternalData();
        }
    }
    componentWillUnmount() {
        this.props.resetGridData();
    }
    componentWillReceiveProps(nextProps) {
        let oldCustomGridFilter = this.props.customGridFilter;
        let newCustomGridFilter = nextProps.customGridFilter;
        if (!_.isEqual(oldCustomGridFilter, newCustomGridFilter)) {
            this.setFilter(nextProps.externalFilter);
        }

    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.firstTimeLoad && this.props.showFilter === false && this.props.abModelData) {
            this.getExternalData();
        }
    }
    onRowClick(row, event) {
        // console.log(row.props.data);
    }

    getExternalData(page) {
        const { getModels, pageSize = null } = this.props;
        const { resultsPerPage, externalFilter, advanceFilter, externalSortColumn, externalSortAscending } = this.props.abModelData;
        let configData = {
            page,
            resultsPerPage: (pageSize) ? pageSize : resultsPerPage,
            externalSortColumn: (this.props.externalSortColumn != null && this.state.firstTimeLoad) ? this.props.externalSortColumn : externalSortColumn,
            externalSortAscending: (this.props.externalSortAscending != null && this.state.firstTimeLoad) ? this.props.externalSortAscending : externalSortAscending,
            externalFilter,
            AbModelInstance: this.props.AbModelInstance,
            filters: this.state.filterColumn,
            include: this.state.includeModels,
            advanceFilter
        }
        getModels(configData);
        if (this.state.firstTimeLoad) {
            this.setState({ firstTimeLoad: false })
        }
    }

    setPageSize(size) {
        const { getModels } = this.props;
        const { externalSortColumn, externalSortAscending, externalFilter, advanceFilter } = this.props.abModelData;
        let configData = {
            page: 0,
            resultsPerPage: size,
            externalSortColumn,
            externalSortAscending,
            externalFilter,
            AbModelInstance: this.props.AbModelInstance,
            filters: this.state.filterColumn,
            include: this.state.includeModels,
            advanceFilter
        }
        getModels(configData);

    }

    changeSort(sort, sortAscending) {
        const { getModels } = this.props;
        const { resultsPerPage, externalFilter, advanceFilter } = this.props.abModelData;
        let configData = {
            page: 0,
            resultsPerPage,
            externalSortColumn: sort,
            externalSortAscending: sortAscending,
            externalFilter,
            AbModelInstance: this.props.AbModelInstance,
            filters: this.state.filterColumn,
            include: this.state.includeModels,
            advanceFilter
        }
        getModels(configData);
        this.setState({ currentPage: 0 });
    }

    setFilter(filterData) {
        const { getModels, pageSize = null } = this.props;
        const { resultsPerPage, externalSortColumn, externalSortAscending, externalFilter, advanceFilter } = this.props.abModelData;
        let configData = {
            page: 0,
            resultsPerPage: (pageSize) ? pageSize : resultsPerPage,
            externalSortColumn: (this.props.externalSortColumn != null && this.state.firstTimeLoad) ? this.props.externalSortColumn : externalSortColumn,
            externalSortAscending: (this.props.externalSortAscending != null && this.state.firstTimeLoad) ? this.props.externalSortAscending : externalSortAscending,
            // externalFilter: filterData,
            AbModelInstance: this.props.AbModelInstance,
            filters: this.state.filterColumn,
            include: this.state.includeModels
        }
        if (typeof filterData === "string") {
            configData["externalFilter"] = filterData;
            configData["advanceFilter"] = advanceFilter;
            let searchFilterPattern = new RegExp("^[a-zA-Z0-9  \b]+$");
            if (!searchFilterPattern.test(filterData) && filterData.length > 0){
                return false;
            }
        } else {
            configData["externalFilter"] = externalFilter;
            configData["advanceFilter"] = filterData;
        }
        getModels(configData);
        this.setState({ currentPage: 0 });
    }

    customFilterFunction(items, query) {
        return _.filter(items, (item) => {
            let flat = squish(item);

            for (let key in flat) {
                if (String(flat[key]).toLowerCase().indexOf(query.value.toLowerCase()) >= 0) return true;
            };
            return false;
        });
    };

    setPage(index) {
        index = index > this.props.maxPages ? this.props.maxPages : index < 1 ? 1 : index + 1;
        this.getExternalData(index);
        this.setState({ currentPage: index });
    }

    render() {
        let self = this;
        const { showFilter, showSettings, colsNotToDisplayInSetting, customRowComponent, customNoDataComponent, showPager, pageSize, showLoader, exportData, exportConfig, modelName, exportGridData, AbModelInstance } = this.props;
        const { modelCols, columnMetadata, advanceFilterConfig } = this.state;

        if (!this.props.abModelData) {
            return null;
        }
        const { resultsPerPage } = this.props.abModelData;
        let { maxPages, results, currentPage, externalSortColumn, externalSortAscending, externalFilter, advanceFilter } = this.props.abModelData;
        let useCustomRowComponent = false;
        if (customRowComponent) {
            useCustomRowComponent = true
        }
        let showfooter = showPager;
        if (results && results.length <= 0 && useCustomRowComponent === true) {
            showfooter = false;
            useCustomRowComponent = false;
        }

        const LoadingComponent = () => {
            return (
                <Segment basic loading />
            )
        }

        let configData = {
            page: this.state.currentPage,
            resultsPerPage: (pageSize) ? pageSize : resultsPerPage,
            externalSortColumn: this.props.externalSortColumn != null ? this.props.externalSortColumn : externalSortColumn,
            externalSortAscending: this.props.externalSortAscending != null ? this.props.externalSortAscending : externalSortAscending,
            externalFilter,
            filters: this.state.filterColumn,
            include: this.state.includeModels,
            advanceFilter
        }

        return (
            <div>
                <Grid>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Griddle useExternal={true} externalSetPage={this.setPage} enableSort={true} useGriddleStyles={false} tableClassName="ui compact celled table"
                                columns={modelCols} columnMetadata={columnMetadata} externalLoadingComponent={LoadingComponent} externalIsLoading={showLoader}
                                useCustomFilterer={true} customFilterer={this.customFilterFunction}
                                useCustomFilterComponent={true} customFilterComponent={OtherFilter} showPager={showfooter}
                                useCustomPagerComponent={true} customPagerComponent={(props) => OtherPager({ ...props, ...self.props, exportData, config: exportConfig, modelName, exportGridData, AbModelInstance, ...configData })}
                                externalSetPageSize={(size) => this.setPageSize(size)} externalMaxPage={maxPages}
                                externalCurrentPage={currentPage} results={results} resultsPerPage={(pageSize) ? pageSize : resultsPerPage}
                                showSettings={showSettings} showFilter={showFilter} externalChangeSort={(sort, sortAscending) => this.changeSort(sort, sortAscending)}
                                externalSortColumn={externalSortColumn} externalSortAscending={externalSortAscending} externalSetFilter={(filter) => this.setFilter(filter)}
                                metadataColumns={colsNotToDisplayInSetting} useCustomRowComponent={useCustomRowComponent} customRowComponent={customRowComponent} customNoDataComponent={customNoDataComponent} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );

    }
}

AbModelGrid.propTypes = {
    modelName: PropTypes.string.isRequired,
    showSettings: PropTypes.bool,
    showPager: PropTypes.bool,
    showFilter: PropTypes.bool,
    columnMetadata: PropTypes.array.isRequired,
    modelCols: PropTypes.arrayOf(PropTypes.string),
    colsNotToDisplayInSetting: PropTypes.arrayOf(PropTypes.string),
    appbackApi: PropTypes.object.isRequired,
    customGridFilter: PropTypes.object,
    deleteCallback: PropTypes.func,
    errorCallback: PropTypes.func.isRequired,
    loaderCallback: PropTypes.func,
    dataSuccessCallback: PropTypes.func,
    customRowComponent: PropTypes.func,
    customNoDataComponent: PropTypes.func,
    pageSize: PropTypes.number,
    externalSortColumn: PropTypes.string,
    externalSortAscending: PropTypes.bool,
    dataSourceType: function (props, propName, componentName) {
        let { dataSourceType, dataSourceOptions } = props;
        if (dataSourceType === GRID_DATA_SOURCE_TYPE.METHOD) {
            if (!dataSourceOptions) {
                return new Error(
                    'Provide dataSourceOptions props when dataSourceOptions is GRID_DATA_SOURCE_TYPE.METHOD'
                );
            }
        }
    },
    dataSourceOptions: React.PropTypes.shape({
        methodName: React.PropTypes.string.isRequired,
        data: React.PropTypes.object.isRequired,
        params: React.PropTypes.object.isRequired,
        options: React.PropTypes.object.isRequired
    }),
    exportData: PropTypes.bool,
    exportConfig: PropTypes.object,
    exportStartCallback: PropTypes.func,
    exportFinishCallback: PropTypes.func
}

AbModelGrid.defaultProps = {
    showSettings: false,
    showPager: true,
    showFilter: true,
    colsNotToDisplayInSetting: [],
    customGridFilter: null,
    dataSourceType: GRID_DATA_SOURCE_TYPE.MODEL,
    customRowComponent: null,
    externalSortColumn: null,
    externalSortAscending: null,
    exportData: false,
    exportConfig: null
};

AbModelGrid.childContextTypes = {
    modelName: PropTypes.string
}