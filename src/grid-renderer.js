import React from 'react';
import moment from 'moment';
import { COLUMNTYPE, ACTIONBUTTON } from "./constants";
import { Button, Input, Dropdown, Header, Icon, Modal, Grid, Popup, Menu } from 'semantic-ui-react';
import AbSelectWidget from './form/custom_widgets/ab-select';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import GridFilterComponent from "./grid/containers/grid-filter-container";
import GridConfirmComponent from "./grid/components/grid-confirm-component";

export const LinkRenderer = (props) => {
    const { typeOptions } = props.metadata;
    const { path } = typeOptions;
    if (path) {
        return (<a href={path + "/" + props.data} target="_top">{props.data}</a>)
    } else {
        return (<a>{props.data}</a>)
    }
}

export const dataRenderer = (props) => {
    const { type } = props.metadata
    if (type) {
        if (type === COLUMNTYPE.DATE) {
            const { typeOptions } = props.metadata;
            const { format } = typeOptions;
            if (format) {
                return (<div>{moment(props.data).format(format)}</div>)
            } else {
                return (<div>{moment(props.data).format()}</div>)
            }

        } else if (type === COLUMNTYPE.BOOL) {
            const { typeOptions } = props.metadata;
            const { truePlaceHolder, falsePlaceHolder } = typeOptions;
            if (truePlaceHolder && falsePlaceHolder) {
                return (<div>{(props.data) ? truePlaceHolder : falsePlaceHolder}</div>)
            } else {
                return (<div>{props.data}</div>);
            }
        } else {
            return (<div>{props.data}</div>);
        }
    } else {
        return (<div>{props.data}</div>);
    }
}

export const actionComponent = (props, onClickButton) => {
    let defaultButtonsList = {
        [ACTIONBUTTON.VIEW]: {
            "label": "View",
            "key": ACTIONBUTTON.VIEW,
            "icon": "info"
        },
        [ACTIONBUTTON.EDIT]: {
            "label": "Edit",
            "key": ACTIONBUTTON.EDIT,
            "icon": "edit"
        },
        [ACTIONBUTTON.DELETE]: {
            "label": "Delete",
            "key": ACTIONBUTTON.DELETE,
            "icon": "trash"
        }
    }
    const { typeOptions } = props.metadata;
    let finalButtonList = [];

    function orderButtons(buttonsOrder, button, buttonComponent) {
        if (buttonsOrder && buttonsOrder.indexOf(button.key) > -1) {
            let buttonOrderIndex = buttonsOrder.indexOf(button.key);
            finalButtonList[buttonOrderIndex] = buttonComponent;
        } else {
            finalButtonList.push(buttonComponent);
        }
    }
    if (typeOptions) {
        const { buttons, defaultButtons, clickHandler, deleteConfirmModalOptions = null, buttonsOrder = null } = typeOptions;
        if (defaultButtons && defaultButtons.length > 0) {
            defaultButtons.map((buttonName, i) => {
                let button = defaultButtonsList[buttonName];
                let buttonComponent = null;
                if (button.key === ACTIONBUTTON.DELETE) {
                    buttonComponent = (<GridConfirmComponent key={i} triggerProps={button} index={i} confirmCallback={(e) => onClickButton(button.key, props.rowData, clickHandler)} modalOptions={deleteConfirmModalOptions} />);
                } else {
                    buttonComponent = (<Popup key={i}
                        trigger={<Button icon={button.icon} onClick={(e) => onClickButton(button.key, props.rowData, clickHandler)} />}
                        content={button.label}
                    />);
                }
                orderButtons(buttonsOrder, button, buttonComponent);
            });
        }
        if (buttons && buttons.length > 0) {
            buttons.map((button, i) => {
                let buttonComponent = null;
                if (button.component) {
                    buttonComponent = button.component(props);
                } else {
                    buttonComponent = (
                        <Popup key={i + "Ext"}
                            trigger={<Button icon={button.icon} onClick={(e) => onClickButton(button.key, props.rowData, clickHandler)} />}
                            content={button.label}
                        />);
                }
                orderButtons(buttonsOrder, button, buttonComponent);
            });
        }
    }
    return (
        <div>
            <Button.Group basic size='small'>
                {finalButtonList}
            </Button.Group>
        </div>
    )
}

export const OtherPager = (props) => {
    const pageChange = (event) => {
        props.setPage(parseInt(event.target.getAttribute("data-value")));
    }
    const onChange = (event, args) => {
        props.setPage(args.value);
    }
    const previousPage = () => {
        props.previous();
        // refs.pageDrp.setValue(props.currentPage - 1);
    }
    const nextPage = () => {
        props.next();
        // refs.pageDrp.setValue(props.currentPage + 1);
    }
    const exportModelData = (modelName, config, format) => {
        let link = document.createElement("a")
        let url = config.apiUrl + '/' + modelName + '/report/' + format + '?access_token=' + config.token + '&filter={}'
        link.href = url
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
    }
    // render() {
    let previous = "";
    let next = "";
    let exportComponent = "";

    previous = <Button size="mini" content={props.previousText} disabled={props.currentPage == 0} icon='angle left' labelPosition='left' floated='left' onClick={previousPage.bind(this)} />


    const isLastPage = props.currentPage == (props.maxPage - 1);
    next = <Button size="mini" content={props.nextText} disabled={isLastPage} icon='angle right' labelPosition='right' {...(!props.exportData && { floated: 'right' }) } onClick={nextPage.bind(this)} />


    let options = [];
    let pagesObj = [];
    for (let i = 0; i < props.maxPage; i++) {
        pagesObj.push({ text: i + 1, value: i });
    }

    options = <span className="page-count-wrapper">
        <Dropdown pointing size="mini" scrolling options={pagesObj} value={props.currentPage} onChange={onChange.bind(this)} />
        / {props.maxPage}
    </span>

    let exportOptions = [
        { text: 'All', value: 'ALL' },
        { text: 'Current Page', value: 'CURRENT' }
    ]

    exportComponent = <div className="float-right">
        <Menu size="mini">
            <Menu.Item>
                <Popup
                    trigger={
                        <Dropdown icon='file excel outline' basic compact className="icon">
                            <Dropdown.Menu>
                                <Dropdown.Item onClick={() => { props.exportGridData(props, 'excel', 'ALL') }}>All</Dropdown.Item>
                                <Dropdown.Item onClick={() => { props.exportGridData(props, 'excel', 'CURRENT') }}>Page</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    }
                    content='Export as Excel'
                />
            </Menu.Item>
            <Menu.Item>
                <Popup
                    trigger={
                        <Dropdown icon='file pdf outline' basic compact className="icon">
                            <Dropdown.Menu>
                                <Dropdown.Item onClick={() => { props.exportGridData(props, 'pdf', 'ALL') }}>All</Dropdown.Item>
                                <Dropdown.Item onClick={() => { props.exportGridData(props, 'pdf', 'CURRENT') }}>Page</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    }
                    content='Export as Pdf'
                />
            </Menu.Item>
        </Menu>
    </div>

    return (
        props.exportData ?
            <div>
                {previous}
                {options}
                {next}
                {exportComponent}
            </div>
            :
            <div className='center'>
                {previous}
                {options}
                {next}
            </div>
    )
    // }
}

export const OtherFilter = (props, advanceFilter) => {
    // let answer = null;
    // const searchChange = (e, filter) => {
    //     props.changeFilter(filter.value);
    // }
    // const OnSelectChange = (value) =>{
    //     answer=value;
    // }
    // const { changeFilter, results, currentResults, placeholderText } = props;
    // let value = "";
    return (
        <GridFilterComponent {...props} advanceFilter={advanceFilter} />
    )
    /*return (
        
        <Grid columns='equal' style={{ "margin-bottom": 5 }}>
            <Grid.Row>
                <Grid.Column width={3}>
                    <Input icon='search' placeholder='Search...' onChange={searchChange.bind(this)} />
                </Grid.Column>
               
            </Grid.Row>
        </Grid>
    )*/

}
