import React from 'react'
import { Header, Segment } from 'semantic-ui-react'
import './style.css'

export const Panel = (props) => {
  return <div>{props.children}</div>
}

export const PanelHeader = (props) => {
  return <Header textAlign='center' as='h4' attached='top'>{props.children}</Header>
}

export const PanelContent = (props) => {
  return <Segment className='outer-segment-padding' attached><div className='segment-grid-table'>{props.children}</div></Segment >
}

