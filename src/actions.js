// import _ from 'underscore';
import _ from 'lodash';
import {
    appbackApi,
    MODELS_DATA_REQUEST_SUCCESS,
    SET_MODEL_INITIAL_STATE,
    SET_CURRENT_ACTIVE_ITEM,
    SAVE_MODEL_FORM_DATA,
    SAVE_MODEL_FORM_DATA_SUCCESS,
    CREATE_MODEL_FORM_DATA,
    CREATE_MODEL_FORM_DATA_SUCCESS,
    CLEAR_FORM_DATA,
    MERGE_GRID_CONFIG,
    GRID_DATA_SOURCE_TYPE,
    ACTIONBUTTON,
    RESET_GRID_DATA_ONLY,
    SHOW_LOADER,
    HIDE_LOADER
} from "./constants";


// ------------------------------------
// Actions
// ------------------------------------
export function customMethodRequest(configData) {
    let { page, resultsPerPage, AbModelInstance, AbModelName, externalSortColumn, externalSortAscending, externalFilter, filters, include, errorCallback, loaderCallback, advanceFilter, dataSourceOptions, customGridFilter, showPager, dataSuccessCallback } = configData;
    page = page || 1;
    let recordCount;
    let skip = (page - 1) * resultsPerPage;
    let limit = resultsPerPage;
    let order = externalSortColumn;
    let sortOrder = (externalSortAscending) ? " ASC" : " DESC";
    let filterCols = _.map(filters, function (filterCol) {
        return { [filterCol]: { "regexp": "/" + externalFilter + "/i" } }
    });
    let where = {
        "or": filterCols
    }
    order = order + sortOrder;
    return dispatch => {
        dispatch(showLoading(AbModelName));
        if (loaderCallback) {
            loaderCallback('SHOW_LOADER');
        }
        let countFilter = where.or.length ? where : {};
        let { methodName, data, params, options } = dataSourceOptions;
        if (customGridFilter) {
            countFilter = { ...countFilter, ...customGridFilter };
        }

        if (advanceFilter) {
            countFilter = { ...countFilter, ...advanceFilter };
        }


        let filter;
        if (include.length > 0) {
            filter = { order, limit, skip, where: countFilter, include };
        } else {
            filter = { order, limit, skip, where: countFilter };
        }
        let finalFilter = { "filter": filter }
        if (params) {
            finalFilter = { ...finalFilter, ...params };
        }
        if (showPager != null && showPager === false) {
            limit = recordCount;
        }
        AbModelInstance.custom(methodName, data, finalFilter, options)
            .then((response) => {
                let payloadData = {};
                let recordCount = response.count;
                payloadData.results = response.data;
                payloadData.currentPage = page - 1;
                payloadData.maxPages = Math.ceil(recordCount / limit);
                payloadData.modelName = AbModelName;
                payloadData.recordCount = recordCount;
                payloadData.resultsPerPage = limit;
                payloadData.externalSortColumn = externalSortColumn;
                payloadData.externalSortAscending = externalSortAscending;
                payloadData.externalFilter = externalFilter;
                payloadData.advanceFilter = advanceFilter;
                payloadData.filters = filters;
                payloadData.includes = include;
                dispatch(hideLoading(AbModelName));
                if (loaderCallback) {
                    loaderCallback('HIDE_LOADER');
                }
                if (dataSuccessCallback) {
                    dataSuccessCallback(response);
                }
                dispatch(modelDataRequestSuccess(payloadData));
            }, (error) => {
                console.log("error", error);
                errorCallback(error);
                dispatch(hideLoading(AbModelName));
                if (loaderCallback) {
                    loaderCallback('HIDE_LOADER');
                }
                return { result: "fail", data: error };
            });
    }
}

export function showLoading(AbModelName) {
    return {
        type: SHOW_LOADER,
        payload: { AbModelName }
    }
}

export function hideLoading(AbModelName) {
    return {
        type: HIDE_LOADER,
        payload: { AbModelName }
    }
}

export function modelsRequest(configData) {
    let { page, resultsPerPage, AbModelInstance, AbModelName, externalSortColumn, externalSortAscending, externalFilter, filters, include, customGridFilter, errorCallback, loaderCallback, advanceFilter, showPager, dataSuccessCallback } = configData;
    page = page || 1;
    let recordCount;
    let skip = (page - 1) * resultsPerPage;
    let limit = resultsPerPage;
    let order = externalSortColumn;
    let sortOrder = (externalSortAscending) ? " ASC" : " DESC";
    let filterCols = _.map(filters, function (filterCol) {
        return { [filterCol]: { "regexp": "/" + externalFilter + "/i" } }
    });
    let where = {
        "or": filterCols
    }
    order = order + sortOrder;
    return dispatch => {
        dispatch(showLoading(AbModelName));
        if (loaderCallback) {
            loaderCallback('SHOW_LOADER');
        }
        let countFilter = where.or.length ? where : {};
        if (customGridFilter) {
            countFilter = { ...countFilter, ...customGridFilter };
        }
        if (advanceFilter) {
            countFilter = { ...countFilter, ...advanceFilter };
        }
        AbModelInstance.count(countFilter).fetch()
            .then((resp) => {
                // if (resp.body().data() && resp.statusCode() === 200) {
                let recordCount = resp.count;
                if (showPager != null && showPager === false) {
                    limit = recordCount;
                }
                let filter;
                if (include.length > 0) {
                    filter = { order, limit, skip, where: countFilter, include };
                } else {
                    filter = { order, limit, skip, where: countFilter };
                }
                // console.log("filter", JSON.stringify(filter));
                AbModelInstance.all(filter).fetch()
                    .then((response) => {
                        // if (Array.isArray(response.body()) && response.statusCode() === 200) {
                        //     let actualData = _.map(response.body(), function (row) { return row.data() });
                        let payloadData = {};
                        payloadData.results = response;
                        payloadData.currentPage = page - 1;
                        payloadData.maxPages = Math.ceil(recordCount / limit);
                        payloadData.modelName = AbModelName;
                        payloadData.recordCount = recordCount;
                        payloadData.resultsPerPage = limit;
                        payloadData.externalSortColumn = externalSortColumn;
                        payloadData.externalSortAscending = externalSortAscending;
                        payloadData.externalFilter = externalFilter;
                        payloadData.advanceFilter = advanceFilter;
                        payloadData.filters = filters;
                        payloadData.includes = include;
                        dispatch(hideLoading(AbModelName));
                        if (loaderCallback) {
                            loaderCallback('HIDE_LOADER');
                        }
                        if (dataSuccessCallback) {
                            dataSuccessCallback(response);
                        }
                        dispatch(modelDataRequestSuccess(payloadData));
                        // }
                    }, (error) => {
                        console.log("error", error);
                        errorCallback(error);
                        dispatch(hideLoading(AbModelName));
                        if (loaderCallback) {
                            loaderCallback('HIDE_LOADER');
                        }
                        return { result: "fail", data: error };
                    });
            }, (error) => {
                errorCallback(error);
                dispatch(hideLoading(AbModelName));
                if (loaderCallback) {
                    loaderCallback('HIDE_LOADER');
                }
                return { result: "fail", data: error };
            });
    }
}

export function modelDataRequestSuccess(payloadData) {
    return {
        type: MODELS_DATA_REQUEST_SUCCESS,
        payload: payloadData
    }
}

export function setModelInitialData(abModelName, initialConfigData) {
    return {
        type: SET_MODEL_INITIAL_STATE,
        payload: { modelName: abModelName, initialConfigData }
    }
}

export function setCurrentActiveItem(payloadData) {
    let { currentActiveItem, modelName, action } = payloadData;
    return {
        type: SET_CURRENT_ACTIVE_ITEM,
        payload: {
            modelName,
            form: {
                currentActiveItem,
                action
            }
        }
    }
}


export function saveModelFormDataSuccess(payloadData) {
    return {
        type: SAVE_MODEL_FORM_DATA_SUCCESS,
        payload: payloadData

    }
}

//transformData flag used only for FormWrapper Hoc
//isFormWrapper flag sent from FormWrapper Hoc
export function saveModelFormData(payloadData) {
    return dispatch => {
        let { AbModelInstance, formMetaData, AbModelName, saveCallback, errorCallback, dataSourceType, dataSourceOptions, transformFieldsDetail, transformFlagDetail, transformData = true, isFormWrapper = false } = payloadData;
        let { formData, schema } = formMetaData
        let finalDataToSave = {}

        if (!isFormWrapper) {
            let visbleFields = Object.keys(schema.properties);
            for (let fieldName of visbleFields) {
                if (formData.hasOwnProperty(fieldName)) {
                    finalDataToSave[fieldName] = formData[fieldName];
                }
            }
        } else {
            finalDataToSave = formData;
        }
        const transformInner = (transformArray, transformedFormData) => {
            _.map(_.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    // transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    // delete transformedFormData[fieldName];
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        let tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = { ..._.cloneDeep(tempData) }
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _.map(transformedFormData[fieldName], function (fieldObject) {
                        if (transformArray[fieldName]) {
                            transformInner(transformArray[fieldName], fieldObject);
                        }
                    });
                }
            });
            return transformedFormData;
        }
        const transformFieldsValue = (transformArray, transformedFormData) => {
            _.map(_.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        let tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = { ..._.cloneDeep(tempData) }
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _.map(transformedFormData[fieldName], function (fieldObject, index) {
                        if (transformArray[fieldName]) {
                            let tempData = transformInner(transformArray[fieldName], fieldObject);
                            transformedFormData[fieldName][index] = { ..._.cloneDeep(tempData) }
                        }
                    });
                }
            });
            return transformedFormData;
        }

        let finalDataSet = transformData ? transformFieldsValue(transformFieldsDetail, finalDataToSave) : finalDataToSave;
        finalDataSet = JSON.stringify(
            finalDataSet,
            function (k, v) { if (v === undefined) { return null; } return v; }
        );
        finalDataSet = JSON.parse(finalDataSet);
        let dataSource = null
        if (dataSourceType && dataSourceOptions) {
            let { CREATE, EDIT } = dataSourceOptions;
            finalDataSet["id"] = formData.id;
            if (EDIT && EDIT.methodName) {
                const { methodName, params, options } = EDIT;
                dataSource = AbModelInstance.custom(methodName, finalDataSet, params, options)
            } else if (CREATE && CREATE.methodName) {
                const { methodName, params, options } = EDIT;
                dataSource = AbModelInstance.custom(methodName, finalDataSet, params, options)
            }

            // if (CREATE && CREATE.methodName) {
            //     const { methodName, params, options } = CREATE;
            //     dataSource = AbModelInstance.custom(methodName, formData, params, options)
            // }
        }
        if (!dataSource) {
            dataSource = AbModelInstance.one(formData.id).update(finalDataSet)
        }

        // let dataSource = null
        // if (dataSourceType && dataSourceOptions && dataSourceOptions.methodName) {
        //     const { methodName, params, options } = dataSourceOptions;
        //     finalDataToSave["id"] = formData.id;
        //     dataSource = AbModelInstance.custom(methodName, finalDataToSave, params, options)
        // } else {
        //     dataSource = AbModelInstance.one(formData.id).update(finalDataToSave)
        // }
        if (dataSource) {
            dataSource.then((resp) => {
                if (resp) {
                    let newItemData = resp;
                    const { includes = [] } = payloadData;
                    let apiFilter = {};
                    if (includes.length > 0) {
                        apiFilter = { include: includes };
                    }
                    AbModelInstance.one(formData.id, apiFilter).fetch()
                        .then((response) => {
                            if (response) {
                                dispatch(saveModelFormDataSuccess({ modelName: AbModelName, data: response, saveCallback }));
                                saveCallback("edit", response);
                            }
                        }, (error) => {
                            errorCallback("edit", error);
                            return { result: "fail", data: error };
                        });
                } else {
                    errorCallback("edit", { "statusCode": 200, "message": "Problem in updatind form data", "name": "AbModelForm" });
                    return { result: "fail", data: { "message": "Problem in updating" } }
                }
            }, (error) => {
                errorCallback("edit", error);
                return { result: "fail", data: error };
            });
        }
    }
}

export function createModelFormDataSuccess(payloadData) {
    return {
        type: CREATE_MODEL_FORM_DATA_SUCCESS,
        payload: payloadData
    }
}

//transformData flag used only for FormWrapper Hoc
//isFormWrapper flag sent from FormWrapper Hoc
export function createModelFormData(payloadData) {
    let { AbModelInstance, formMetaData, AbModelName, saveCallback, errorCallback, dataSourceType, dataSourceOptions, transformFieldsDetail, transformFlagDetail, transformData = true, isFormWrapper = false } = payloadData;
    let { formData, schema } = formMetaData;
    let finalDataToSave = {}
    if (!isFormWrapper) {
        let visbleFields = Object.keys(schema.properties);
        for (let fieldName of visbleFields) {
            if (formData.hasOwnProperty(fieldName)) {
                finalDataToSave[fieldName] = formData[fieldName];
            }
        }
    } else {
        finalDataToSave = formData;
    }

    return dispatch => {
        const transformInner = (transformArray, transformedFormData) => {
            _.map(_.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    // transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    // delete transformedFormData[fieldName];
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        let tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = { ..._.cloneDeep(tempData) }
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _.map(transformedFormData[fieldName], function (fieldObject) {
                        if (transformArray[fieldName]) {
                            transformInner(transformArray[fieldName], fieldObject);
                        }
                    });
                }
            });
            return transformedFormData;
        }

        const transformFieldsValue = (transformArray, transformedFormData) => {
            _.map(_.uniq(transformArray), function (fieldName) {
                if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
                    if (transformFlagDetail && transformFlagDetail[fieldName] == true) {
                        transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
                    }
                    if (transformArray[fieldName] && transformArray[fieldName].constructor === Array && transformArray[fieldName].length > 0) {
                        let tempData = transformInner(transformArray[fieldName], transformedFormData[fieldName]);
                        transformedFormData[fieldName] = { ..._.cloneDeep(tempData) }
                    } else {
                        delete transformedFormData[fieldName];
                    }
                } else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
                    _.map(transformedFormData[fieldName], function (fieldObject, index) {
                        if (transformArray[fieldName]) {
                            let tempData = transformInner(transformArray[fieldName], fieldObject);
                            transformedFormData[fieldName][index] = { ..._.cloneDeep(tempData) }
                        }
                    });
                }
            });
            return transformedFormData;
        }

        let finalDataSet = transformData ? transformFieldsValue(transformFieldsDetail, finalDataToSave) : finalDataToSave;
        finalDataSet = JSON.stringify(
            finalDataSet,
            function (k, v) { if (v === undefined) { return null; } return v; }
        );
        finalDataSet = JSON.parse(finalDataSet);
        let dataSource = null
        if (dataSourceOptions) {
            let { CREATE } = dataSourceOptions;
            if (CREATE && CREATE.methodName) {
                const { methodName, params, options } = CREATE;
                dataSource = AbModelInstance.custom(methodName, finalDataSet, params, options)
            }
        }
        if (!dataSource) {
            dataSource = AbModelInstance.create(finalDataSet)
        }
        if (dataSource) {
            dataSource.then((response) => {
                if (response) {
                    dispatch(createModelFormDataSuccess({ modelName: AbModelName, data: response, saveCallback }));
                    saveCallback("create", response);
                }
            }, (error) => {
                console.log("error", error);
                errorCallback("create", error);
                return { result: "fail", data: error };
            });
        }

    }
}

export function clearFormData(payloadData) {
    return {
        type: CLEAR_FORM_DATA,
        payload: payloadData
    }
}

export function getSelectModelData(payload) {
    const { AbModelInstance, filter } = payload;
    return dispatch => {
        return AbModelInstance.all(filter).fetch()
    }
}

export function mergeGridConfig(payloadData) {
    return {
        type: MERGE_GRID_CONFIG,
        payload: payloadData
    }
}

export function DeleteGridRow(payloadData) {
    let { id, AbModelInstance, data, AbModelName, deleteCallback, errorCallback, abModelData, gridReloadConfigData, dataSourceType } = payloadData;
    if (data.id) {
        return dispatch => {
            AbModelInstance.one(data.id).delete()
                .then((response) => {
                    if (response) {
                        if (deleteCallback) {
                            deleteCallback(response);
                        }
                        if (dataSourceType && dataSourceType === GRID_DATA_SOURCE_TYPE.METHOD) {
                            return dispatch(customMethodRequest(gridReloadConfigData));
                        } else {
                            return dispatch(modelsRequest(gridReloadConfigData));
                        }
                    }
                }, (error) => {
                    console.log("error", error);
                    if (errorCallback) {
                        errorCallback(error);
                    }
                    return { result: "fail", data: error };
                });
        }
    } else {
        errorCallback({ message: "Problem in deleting data, Id missing." })
    }
}


export function getPreLoadFormData(payloadData) {
    const { AbModelInstance, filter, AbModelName, saveCallback, errorCallback, dataSourceType, dataSourceOptions, transformSelectDetail } = payloadData;
    const transformSelectValue = (transformArray, dataObject) => {
        _.map(_.uniq(transformArray), function (fieldName) {
            if (dataObject && dataObject.constructor === Array) {
                _.map(dataObject, function (data) {
                    if (data && data[fieldName]) {
                        if (transformArray[fieldName]) {
                            transformSelectValue(transformArray[fieldName], data[fieldName])
                        } else {
                            if (data[fieldName].constructor === Array) {
                                data[fieldName] = data[fieldName].join(",");
                            }
                        }
                    }
                });
            } else if (dataObject && dataObject.constructor === Object) {

                if (dataObject && dataObject[fieldName]) {
                    if (transformArray[fieldName]) {
                        transformSelectValue(transformArray[fieldName], dataObject[fieldName])
                    } else {
                        if (dataObject[fieldName].constructor === Array) {
                            dataObject[fieldName] = dataObject[fieldName].join(",");
                        }
                    }
                }
            }
        });
    }
    if (filter && filter.id) {
        return dispatch => {
            let dataSource = null;
            if (dataSourceType && dataSourceOptions && dataSourceOptions.methodName) {
                const { methodName, data, params, options } = dataSourceOptions;
                dataSource = AbModelInstance.custom(methodName, data, { ...filter }, options)
            } else {
                dataSource = AbModelInstance.one(filter.id).fetch()
            }


            if (dataSource) {
                dataSource.then((response) => {
                    if (response) {
                        if (saveCallback) {
                            saveCallback("preload", response);
                        }

                        if (transformSelectDetail && transformSelectDetail.length > 0) {
                            transformSelectValue(transformSelectDetail, response);
                        }
                        return dispatch(setCurrentActiveItem({ currentActiveItem: response, modelName: AbModelName, action: ACTIONBUTTON.EDIT }));
                    } else {
                        if (errorCallback) {
                            errorCallback("preload", "No preload Data Found");
                        }
                    }
                }, (error) => {
                    console.log("error", error);
                    if (errorCallback) {
                        errorCallback("preload", error);
                    }
                });
            }
        }
    } else {
        return true;
    }

}

export function resetGridDataOnly(abModelName) {
    return {
        type: RESET_GRID_DATA_ONLY,
        payload: { modelName: abModelName }
    }
}

export function importFile(payloadData) {
    return dispatch => {
        const { file, importUrl, successCallback, errorCallback } = payloadData;

        let formData = new FormData();
        formData.append("file", file);
        // formData.append("token", responseData.token);
        const xhr = new XMLHttpRequest()
        // xhr.withCredentials = true;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200 || xhr.status === 201) {
                    // Successfully uploaded the file.
                    console.log("success", xhr);
                    if (successCallback) {
                        successCallback(JSON.parse(xhr.responseText));
                    }
                } else {
                    // The file could not be uploaded.
                    console.log("Error", xhr);
                    if (errorCallback) {
                        errorCallback(JSON.parse(xhr.responseText).error);
                    }
                }
            }
        }
        xhr.upload.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
                console.log("add upload event-listener" + evt.loaded + "/" + evt.total);
            }
        }, false);
        xhr.open("POST", importUrl);
        xhr.send(formData);
    }
}

export function exportGridData(data) {
    let { page } = data;
    const { modelName, config, format, resultsPerPage, externalSortColumn, externalSortAscending, externalFilter, filters, include, customGridFilter, advanceFilter, exportStartCallback, exportFinishCallback, exportErrorCallback, type, dataSourceOptions } = data;
    exportStartCallback();
    let filter = {};
    let fileName = config.fileName ? (config.fileName + '.xlsx') : 'report.xlsx';
    let exportUrl = config.apiUrl + "/" + modelName;
    if (dataSourceOptions && dataSourceOptions.methodName) {
        exportUrl += "/" + dataSourceOptions.methodName;
    }

    page = page || 1;
    let recordCount;
    let skip = (page - 1) * resultsPerPage;
    let limit = resultsPerPage;
    let order = externalSortColumn;
    let sortOrder = (externalSortAscending) ? " ASC" : " DESC";
    let filterCols = _.map(filters, function (filterCol) {
        return { [filterCol]: { "regexp": "/" + externalFilter + "/i" } }
    });
    let where = {
        "or": filterCols
    }
    order = order + sortOrder;
    let countFilter = where.or.length ? where : {};
    if (customGridFilter) {
        countFilter = { ...countFilter, ...customGridFilter };
    }
    if (advanceFilter) {
        countFilter = { ...countFilter, ...advanceFilter };
    }
    filter = { order, where: countFilter };
    if (type != "ALL") {
        filter = Object.assign(filter, { skip, limit });
    }
    if (include.length > 0) {
        filter = Object.assign(filter, { include });
    }

    return (dispatch) => {
        let body = {
            "config": config.configObj,
            "api": {
                "url": exportUrl,
                "method": "GET",
                "qs": {
                    "filter": filter
                },
                "expression": config.expression
            }
        };
        if (dataSourceOptions && !_.isEmpty(dataSourceOptions.params)) {
            body.api.qs = _.assignIn(body.api.qs, dataSourceOptions.params);
        }

        fetch(config.apiUrl + '/service/export/' + format, {
            method: 'POST',
            headers: {
                'Authorization': config.token,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                if (format === 'excel') {
                    return response.blob(); //returns excel file blob
                }
                return response.json(); //returns url json of pdf file
            })
            .then((responseData) => {
                console.log("exportGridData responseData: ", responseData);
                if (format === 'excel') {
                    let link = document.createElement("a");
                    link.href = window.URL.createObjectURL(responseData);
                    document.body.appendChild(link);
                    link.download = fileName
                    link.click();
                    document.body.removeChild(link);
                    exportFinishCallback();
                } else {
                    let link = document.createElement("a");
                    link.href = responseData.message;
                    link.target = "_blank";
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                    exportFinishCallback();
                }

            })
            .catch(error => {
                console.log("exportGridData error: ", error)
                if (exportErrorCallback) {
                    exportErrorCallback(error);
                }
            });
    }
}
// export const actions = {
//     modelsRequest,
//     modelDataRequestSuccess,
//     setModelInitialData,
//     setCurrentActiveItem,
//     saveModelFormData,
//     saveModelFormDataSuccess,
//     createModelFormData
// }
