import React, { Component, PropTypes } from 'react';
import { Label, Button, Segment } from 'semantic-ui-react'
export default function ArrayFieldTemplate(props) {
    let { theme } = props;
    const ArrayFieldItem = (element) => {
        let segmentTitle = element.index+1;
        let { children } = element;
        if(children){
            if(children.props && children.props.schema &&  children.props.schema.segmentTitle){
                segmentTitle =  children.props.schema.segmentTitle;
            }
        }
        return <Segment.Group key={element.index}>
            <Segment attached>
                <Label color={(theme.color)?theme.color:null} ribbon> {segmentTitle} </Label>
                <div>{element.children}</div>
            </Segment>
            {(element.hasRemove || element.hasMoveDown || element.hasMoveUp) && <Segment attached="bottom" textAlign='right'>
                {element.hasRemove &&
                <Button onClick={element.onDropIndexClick(element.index)} basic size="small" icon='trash' color="red" />}
                <Button.Group basic size='small'>
                    {element.hasMoveDown &&
                        <Button onClick={element.onReorderClick(element.index, element.index + 1)} icon="arrow down" />}
                    {element.hasMoveUp &&
                        <Button onClick={element.onReorderClick(element.index, element.index - 1)} icon="arrow up" />}
                </Button.Group>
            </Segment>}
        </Segment.Group>
    }

    return (
        <div className={props.className}>
            <label>{props.title}</label>
            {props.items && props.items.map(ArrayFieldItem)}
            {props.canAdd && <Button onClick={props.onAddClick} color={(theme.color)?theme.color:null} basic icon="plus" content="Add" size="tiny" />}
        </div>
    );
}