var wildcard = require('wildcard');

export const filterFormDataBasedOnSchema = (schema, formData) => {
    let finalFormData = {};
    let fields = Object.keys(schema.properties);
    for (let fieldName of fields) {
        if (formData.hasOwnProperty(fieldName)) {
            finalFormData[fieldName] = formData[fieldName];
        }
    }
    return finalFormData;
}

export const getFormTransformList = (mainSchema) => {
    let self = this;
    let fieldTobeTransform = [];
    let transformFlagDetail = {}
    const fetchFields = (schema, parent) => {
        _.forOwn(schema.properties, function (schemeObj, key) {
            if (schemeObj && key) {
                if (schemeObj.type && schemeObj.type === "object" && schemeObj.transform) {
                    if (schemeObj.transform !== "NOT") {
                        transformFlagDetail[key] = true
                    } else {
                        transformFlagDetail[key] = false
                    }
                    parent.push(key);
                    if (schemeObj.properties) {
                        parent[key] = [];
                        fetchFields(schemeObj, parent[key]);
                    }
                } else if (schemeObj.type && schemeObj.type === "array" && schemeObj.items && schemeObj.items.constructor === Object) {
                    parent.push(key);
                    parent[key] = [];
                    fetchFields(schemeObj.items, parent[key]);
                }

            }
        })
    }
    fetchFields(mainSchema, fieldTobeTransform);
    return { transformFieldsDetail: fieldTobeTransform, transformFlagDetail };
}

export const transformFieldsValueBefore = (schema, dataObject, finalData = {}) => {
    // let finalData = {}
    _.forOwn(schema.properties, (schemaObj, key) => {
        if (schemaObj.type === "object") {
            if (!finalData[key]) {
                finalData[key] = {}
            }
            if (dataObject[key]) {
                transformFieldsValueBefore(schemaObj, dataObject[key], finalData[key]);
            } else {
                transformFieldsValueBefore(schemaObj, dataObject, finalData[key]);
            }
        } else if (schemaObj.type === "array") {
            if (!finalData[key]) {
                finalData[key] = [];
            }
            _.map(dataObject[key], (fieldObj, index) => {
                if (!finalData[key][index]) {
                    finalData[key][index] = {}
                }
                transformFieldsValueBefore(schemaObj.items, fieldObj, finalData[key][index]);
            });
        } else {
            if (dataObject[key]) {
                finalData[key] = _.cloneDeep(dataObject[key])
                delete dataObject[key];
            }
        }
    });
    return finalData;
}


export const compareFileWithMimeType = (target, pattern) => {
    const reMimePartSplit = /[\/\+\.]/;
    function test(pattern) {
        let result = wildcard(pattern, target, reMimePartSplit);

        // ensure that we have a valid mime type (should have two parts)
        return result && result.length >= 2;
    }

    return pattern ? test(pattern.split(';')[0]) : test;
};