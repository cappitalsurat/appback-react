import React, { PropTypes } from "react";
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import _ from 'lodash';
import { FORM_UTILS } from '../../constants';

const AbSelectWidget = (props) => {
    const {id, value, onChange, disabled, options, required } = props;
    const { backspaceRemoves, labelKey, multi, valueKey, selectOptions, searchable, clearable, noResultsText, selectPlaceholder, searchPromptText, autofocus, customFilter, optionRenderer, valueRenderer, delimiter } = options;
    let finalOptions = selectOptions;
    if(customFilter){
       finalOptions =  _.filter(selectOptions, customFilter);
    }
    const valueChanged = (formData) => {
        const sendData = (formData == null || formData.toString().length === 0) ? undefined : formData
        onChange(sendData)
    }
    return (
        <div style={required && !searchable ? {height: '36px'} : {}}>
            <Select multi={multi} options={finalOptions} simpleValue clearable={clearable} disabled={disabled} valueKey={valueKey} labelKey={labelKey} value={value} onChange={valueChanged} searchable={searchable} noResultsText={noResultsText} placeholder={selectPlaceholder} searchPromptText={searchPromptText} autofocus={autofocus} required={required} optionRenderer={optionRenderer} valueRenderer={valueRenderer} delimiter={delimiter}/>
            {required && !searchable && <input tabIndex={-1} value={value} required={true} style={{width: 0, height: 0, opacity: 0, padding: 0, marginLeft: '30px'}} />}
        </div>
    );
};





AbSelectWidget.defaultProps = {
    options: {
        multi: false,
        clearable: true,
        searchable: false,
        backspaceRemoves: true,
        noResultsText: "No results found",
        selectPlaceholder: "Select ...",
        searchPromptText: "Type to search",
        autofocus: false,
        customFilter:{},
        optionRenderer:null,
        valueRenderer:null,
        delimiter:FORM_UTILS.DELIMITER
    },
};

AbSelectWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.any,
    required: PropTypes.bool,
    autofocus: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    options: PropTypes.shape({
        selectOptions: PropTypes.array.required,
        labelKey: PropTypes.string.required,
        valueKey: PropTypes.string.required,
        multi: PropTypes.bool,
        clearable: PropTypes.bool,
        searchable: PropTypes.bool,
        backspaceRemoves: PropTypes.bool,
        noResultsText: PropTypes.string,
        selectPlaceholder: PropTypes.string,
        searchPromptText: PropTypes.string,
        autofocus: PropTypes.bool,
        customFilter: PropTypes.object,
        optionRenderer:PropTypes.func,
        valueRenderer:PropTypes.func,
        delimiter: PropTypes.string
    }).isRequired,
};
export default AbSelectWidget;
