
import { EventEmitter } from 'events';
import React from 'react';
import PropTypes from 'prop-types';
import { Progress, Image, Icon, Button } from 'semantic-ui-react';
import './css/ab-fileuploader.css';
import moment from 'moment';
import { compareFileWithMimeType } from '../form-helper'
import { xlsFile, pdfFile, docFile, pptFile } from '../../base64images'
import async from 'async';
import _ from 'lodash';
const mime = require('mime-types')

const getFileCategory = (filePath) => {
  let selectedFileCategory = mime.lookup(filePath);
  if (selectedFileCategory) {
    let selectedFileCategoryArray = selectedFileCategory.split('/')[0];
    if (selectedFileCategoryArray === 'application') {
      selectedFileCategoryArray = filePath.substring(filePath.lastIndexOf('.') + 1, filePath.length) || filePath;
    }
    return selectedFileCategoryArray;
  }
  return null
}

class AbMultiFileUploaderWidget extends React.Component {
  constructor(props) {
    super(props);
    let { value } = props;
    let fileCategory = null;

    this.proxy = new EventEmitter();
    this.state = {
      progress: -1,
      hasError: false,
      selectedFiles: null,
      selectedFilesCategory: null,
      signedUrlObjects: null,
      previewImgUrls: null,
      hasOtherError: false,
      errorMsg: null
    };
    this.onFileSelect = this.onFileSelect.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
    this.downloadFile = this.downloadFile.bind(this)
  }
  componentDidMount() {
    if (this.props.value && this.props.value.length > 0) {
      let files = this.props.value.split(',')
      let filesCategory = []
      _.forEach(files, function (fileName, index) {
        let fileCategory = getFileCategory(fileName);
        filesCategory.push(fileCategory)
      })
      this.setState({
        selectedFilesCategory: filesCategory
      })
    }
  }
  componentWillReceiveProps(nextProps) {
    let files = null
    if (this.props.value !== nextProps.value && this.props.value !== null) {
      if (nextProps.value && nextProps.value.length > 0) {
        files = nextProps.value.split(',')
      }
      let filesCategory = []
      _.forEach(files, function (fileName, index) {
        let fileCategory = getFileCategory(fileName);
        filesCategory.push(fileCategory)
      })
      this.setState({
        selectedFilesCategory: filesCategory
      })
    }
  }
  onSubmit(e) {
    let self = this;
    if (e) {
      e.preventDefault();
    }
    if (this.state.selectedFiles === null) {
      return;
    }
    let { folderPath } = this.props.options;
    this.setState({
      progress: 0,
      hasError: false
    }, () => {
      let signedUrlObjects = [];
      async.eachSeries(this.state.selectedFiles, function (file, callback) {
        self.getSignedUrl(file, folderPath, (error, data) => {
          if (data) {
            signedUrlObjects.push(data)
          } else {
            signedUrlObjects.push(null)
          }
          callback()
        });
      }, function (err) {
        self.setState({ signedUrlObjects: signedUrlObjects }, () => {
        });
        self._doUpload(signedUrlObjects, self.state.selectedFiles);
      });

    });
  }

  onFileSelect(e) {
    let self = this;
    let { id, options } = this.props;
    let { acceptFileType, typeErrorMsg, fileSize, fileSizeErrorMsg, startUpLoadOnSelect, onFileChange } = options;
    let selectedFiles = e.target.files;
    let isError = false;
    this.setState({
      hasOtherError: false,
      errorMsg: null
    }, () => {
      if (selectedFiles && selectedFiles.length > 0) {
        _.forEach(selectedFiles, function (file, index) {
          if (acceptFileType.filter(compareFileWithMimeType(file.type)).length === 0) {
            isError = true;
            self.setState({
              hasOtherError: true,
              errorMsg: (typeErrorMsg) ? typeErrorMsg : `Valid file type are ${acceptFileType.join(',')}`
            })
            return false;
          } else if (fileSize) {
            let fileSizeInMb = file.size / 1048576;
            if (fileSizeInMb > fileSize) {
              isError = true;
              self.setState({
                hasOtherError: true,
                errorMsg: (fileSizeErrorMsg) ? fileSizeErrorMsg : `Upload file having less then ${fileSize} MB`
              })
              return false;
            }
          }
        })
        if (!isError) {
          if (selectedFiles && selectedFiles.length > 0) {
            onFileChange(selectedFiles)
            let selectedFilesCategory = (self.state.selectedFilesCategory) ? self.state.selectedFilesCategory : []
            let previewImgUrls = (self.props.value) ? self.props.value.split(',') : []
            _.forEach(selectedFiles, function (file, index) {
              let fileType = file.type.split('/');
              if (fileType[0] === 'application') {
                fileType[0] = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length) || file.name;
              }
              selectedFilesCategory.push(fileType[0]);
              previewImgUrls.push(window.URL.createObjectURL(file))
            })
            this.setState({
              selectedFiles: selectedFiles,
              selectedFilesCategory: selectedFilesCategory,
              previewImgUrls: previewImgUrls
            }, () => {
              _.forEach(selectedFilesCategory, function (fileType, index) {
                if (fileType === 'video') {
                  self.refs[`video_${index}`].load();
                }
              })
              if (startUpLoadOnSelect) {
                this.onSubmit();
              }
            });
          }
        }
      }
    })
  }

  getSignedUrl(file, folderPath, callback) {
    let { appbackApi } = this.props.options;
    const params = {
      key: `${folderPath}/${file.name.substr(0, file.name.lastIndexOf('.') - 1)}_${moment(Date.now()).format('YYYY-MM-DDTHH:mm:ss')}${file.name.substr(file.name.lastIndexOf('.'))}`,
      extension: file.name.substr(file.name.lastIndexOf('.')),
      contentType: file.type
    };
    appbackApi.getUploadUrl(params)
      .then(data => {
        if (data && data.signedRequest) {
          callback(null, data);
        } else {
          callback(data, null);
        }
      })
      .catch(error => {
        console.error('Error from getUploadUrl', error);
        callback(error, null);
      });
  }

  cancelUpload() {
    this.proxy.emit('abort');
    this.setState({
      progress: -1,
      hasError: false
    });
  }



  progressRenderer(progress, totalFile, hasError, cancelHandler) {
    let message = null
    if (hasError || progress > -1) {
      if (hasError) {
        message = (<span style={{ color: '#a94442' }}>Failed to upload ...</span>);
      }
      return (
        <Progress value={progress} total={totalFile} progress label='ratio' indicating={progress !== totalFile} error={(hasError) ? true : false} color='violet' success={progress === totalFile}>{message}</Progress>
      );
    }
    return '';
  }
  previewFiles(values, previewImgUrls, selectedFilesCategory, selectedFiles) {

    return selectedFilesCategory.map((selectedFileCategory, index) => {
      let value = (values) ? values[index] : null;
      let previewImgUrl = (previewImgUrls) ? previewImgUrls[index] : null;
      // let selectedFileCategory = selectedFilesCategory[index];
      let selectedFile = (selectedFiles) ? selectedFiles[index] : null;
      return (
        <span key={`span_${index}`} style={{ padding: '5px' }}>
          {((value || previewImgUrl) && selectedFileCategory === 'image') && <Image src={(value && !previewImgUrl) ? `${value}` : previewImgUrl} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && selectedFileCategory === 'video') && <div><video id={`video_${index}`} width="200" ref={`video_${index}`}>
            <source src={(value && !previewImgUrl) ? `${value}` : previewImgUrl} type="video/mp4" />
            Your browser does not support HTML5 video.
           </video></div>}
          {((value || previewImgUrl) && selectedFileCategory === 'pdf') && <Image src={pdfFile} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && (selectedFileCategory === 'xlsx' || selectedFile === 'xls' || selectedFileCategory === 'csv')) && <Image src={xlsFile} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && (selectedFileCategory === 'docx' || selectedFile === 'doc')) && <Image src={docFile} size='tiny' shape='rounded' />}
          {((value || previewImgUrl) && (selectedFileCategory === 'pptx' || selectedFile === 'ppt')) && <Image src={pptFile} size='tiny' shape='rounded' />}
          {(value && !previewImgUrl) &&
            <Button.Group basic size='small' color='violet'>
              <Button icon='download' onClick={(e) => this.downloadFile(e, value)} />
              <Button icon='delete' onClick={(e) => this.deleteFile(e, index)} />
            </Button.Group>}
        </span>
      )
    });
  }
  deleteFile(e, index) {
    if (e) {
      e.preventDefault();
    }
    if (this.props.value) {
      let valueArray = this.props.value.split(',')
      valueArray.splice(index, 1);
      let filesString = valueArray.join(',');
      this.props.onChange(filesString.length > 0 ? filesString : undefined)
    }
  }
  downloadFile(e, url) {
    if (e) {
      e.preventDefault();
    }
    window.open(url);
  }
  render() {
    let { previewImgUrls, hasOtherError, errorMsg, selectedFilesCategory, selectedFiles } = this.state;
    let { value, options, id } = this.props;
    let { inputRef, buttonRef, showProgress = true, acceptFileType } = options;
    // const formElement = this.props.formRenderer(this.onSubmit.bind(this), this.onFileSelect);
    let progressElement = null;
    if (showProgress) {
      let totalFile = -1;
      if (this.state.selectedFiles && this.state.selectedFiles.length > 0) {
        totalFile = this.state.selectedFiles.length;
      }
      progressElement = this.progressRenderer(this.state.progress, totalFile, this.state.hasError, this.cancelUpload.bind(this));
    }


    return (
      <div className='form-control customClass'>
        <input type="file" name="file" onChange={this.onFileSelect} ref={inputRef} accept={acceptFileType.join(',')} multiple />
        <input type="button" style={{ display: 'none' }} ref={buttonRef} onClick={this.onSubmit.bind(this)} />
        {hasOtherError && <div>
          <ul className="error-detail bs-callout bs-callout-info"><li className="text-danger">{errorMsg}</li></ul>
        </div>}
        <div id="progress-container">
          {progressElement}
        </div>
        <div className='preview-container'>
          {(selectedFilesCategory) && this.previewFiles(value ? value.split(',') : null, previewImgUrls, selectedFilesCategory, selectedFiles)}
        </div>
      </div>
    );
  }

  _getFormData() {
    if (this.props.formGetter) {
      return this.props.formGetter();
    }
    return this.state.selectedFiles;
  }

  _doUpload(signedObjs, selectedFiles) {
    let self = this;
    let finalFilesUrl = [];
    if (this.props.value && this.props.value.length > 0) {
      finalFilesUrl = this.props.value.split(',');
    }
    async.eachOfSeries(signedObjs, function (signObj, index, callback) {
      const form = selectedFiles[index];
      let url = signObj.signedRequest
      let fileObj = selectedFiles[index];
      const req = new XMLHttpRequest();
      req.open('PUT', url);
      req.setRequestHeader('X-Amz-ACL', 'public-read');
      req.setRequestHeader('Content-Type', fileObj.type);
      let { options } = self.props

      req.addEventListener('load', (e) => {
        self.proxy.removeAllListeners(['abort']);
        const newState = { progress: self.state.progress + 1 };
        if (req.status >= 200 && req.status <= 299) {

          // options.onUpload(signObj.url)
          finalFilesUrl.push(signObj.url)
          // onChange(this.state.signedUrlObject.url)
          self.setState(newState, () => {
            // options.onLoad(e, req);
          });
        } else {
          newState.hasError = true;
          self.setState(newState, () => {
            options.onError(e, req);
          });
        }
        callback();
      }, false);

      req.addEventListener('error', (e) => {
        self.setState({
          hasError: true
        }, () => {
          options.onError(e, req);
        });
      }, false);

      req.upload.addEventListener('progress', (e) => {
        let progress = 0;
        if (e.total !== 0) {
          progress = parseInt((e.loaded / e.total) * 100, 10);
        }
        // this.setState({
        //   progress
        // }, () => {
        //   options.onProgress(e, req, progress);
        // });
      }, false);

      req.addEventListener('abort', (e) => {
        self.setState({
          progress: -1
        }, () => {
          options.onAbort(e, req);
        });
      }, false);

      self.proxy.once('abort', () => {
        req.abort();
      });

      self.props.beforeSend(req)
        .send(form);

    }, function (err) {
      let { onChange, options } = self.props;
      if (err) {
        console.error(err.message);
        return false;
      }
      options.onUpload(finalFilesUrl)
      let filesString = finalFilesUrl.join(',');
      onChange(filesString.length > 0 ? filesString : undefined)
    });
  }
}

AbMultiFileUploaderWidget.propTypes = {
  url: PropTypes.string,
  formGetter: PropTypes.func,
  formRenderer: PropTypes.func,
  progressRenderer: PropTypes.func,
  formCustomizer: PropTypes.func,
  beforeSend: PropTypes.func,
  onProgress: PropTypes.func,
  onLoad: PropTypes.func,
  onError: PropTypes.func,
  onAbort: PropTypes.func,
  onChange: PropTypes.func
};

AbMultiFileUploaderWidget.defaultProps = {
  formRenderer: (onSubmit, onFileSelect) => (
    <form className="_react_fileupload_form_content" ref="form" method="post" onSubmit={onSubmit}>
      <div>
        <input type="file" name="file" onChange={onFileSelect} ref={ref => this.inputRef = ref} />
      </div>
      <input type="submit" />
    </form>
  ),
  formCustomizer: (form) => form,
  beforeSend: (request) => request,
  options: {
    onProgress: (e, request, progress) => { },
    onLoad: (e, request) => { },
    onError: (e, request) => { },
    onAbort: (e, request) => { },
    onUpload: (url) => { },
    onFileChange: (file) => { },
    folderPath: 'other',
    acceptFileType: ['image/*'],
    fileSize: 2,
    startUpLoadOnSelect: true
  }
};

export default AbMultiFileUploaderWidget;
