import React, { Component } from "react";
import ReactPhoneInput from 'react-phone-input-2';
import _ from 'lodash';

class AbPhoneInputWidget extends Component {
	constructor() {
		super()
		this.state = { value: undefined }
	}

	componentWillMount() {
		const { value } = this.props
		if (value) {
			this.setState({ phone: value })
		} else {
			this.setState({ phone: undefined })
		}
	}

	render() {
		const { id, value, onChange, options, placeholder, required = false, disabled = false } = this.props;
		const { autoFormat = false, onlyCountries = undefined, excludeCountries = undefined, preferredCountries = undefined, regions = undefined,
			defaultCountry = undefined, disableAreaCodes = false, disableCountryCode = false, disableDropdown = false,
			inputStyle = { width: '100%' }, buttonStyle = undefined, dropdownStyle = undefined } = options;
		const { phone } = this.state
		const valueChanged = (value) => {
			const sendData = (value == null || value.toString().length === 0) ? undefined : value
			this.setState({ phone: sendData })
			onChange(sendData)
		}
		return (
			<ReactPhoneInput
				disabled={disabled}
				autoFormat={autoFormat}
				defaultCountry={defaultCountry}
				onChange={valueChanged}
				value={phone}
				id={id}
				required={required}
				onlyCountries={onlyCountries}
				excludeCountries={excludeCountries}
				preferredCountries={preferredCountries}
				regions={regions}
				disableAreaCodes={disableAreaCodes}
				disableCountryCode={disableCountryCode}
				inputStyle={inputStyle}
				buttonStyle={buttonStyle}
				dropdownStyle={dropdownStyle}
			/>
		);
	}
}

export default AbPhoneInputWidget;
