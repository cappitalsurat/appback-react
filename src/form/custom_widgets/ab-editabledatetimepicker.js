import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import './css/react-datepicker.css';
// import 'react-datepicker/react-datepicker-cssmodules.css'
import './css/datepicker-overrides.css'
import _ from 'lodash';

class AbEditableDateTimePickerWidget extends Component {
	constructor(props) {
		super(props);
		this.state = {
			actualDate: props.value && moment(props.value),
			parseDate: props.value
		}
	}
	componentWillReceiveProps(nextProps) {
		this.setState({
			actualDate: nextProps.value && moment(nextProps.value),
			parseDate: nextProps.value
		})
	}
	render() {		
		const { onChange, options, placeholder, required = false, disabled = false } = this.props;
		const { displayDateFormat = 'MM/DD/YYYY', clearable = false, showMonthDropdown = false, showYearDropdown = false, dropdownMode = undefined, 
				minDate = undefined, maxDate = undefined } = options;
		const parseDate = (dtValue) => {
			let parseDateVal = dtValue != null ? _.clone(moment(dtValue).toString()) : undefined;
			this.setState({
				actualDate: dtValue,
				parseDate: parseDateVal
			}, () => {
				onChange(parseDateVal);
			})
		}
		return (
			<div>
				<DatePicker
					selected={this.state.actualDate}
					dateFormat={displayDateFormat}
					placeholderText={placeholder}
					isClearable={clearable}
					disabled={disabled}
					onChange={parseDate}
					onSelect={parseDate}
					required={required}
					showMonthDropdown={showMonthDropdown}
					showYearDropdown={showYearDropdown}
					dropdownMode={dropdownMode}
					minDate={minDate}
					maxDate={maxDate}					
				/>
				<input type='hidden' value={this.state.parseDate} />
			</div>
		)
	}
}

export default AbEditableDateTimePickerWidget
