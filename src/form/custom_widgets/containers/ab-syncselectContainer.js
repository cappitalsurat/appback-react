import React, { Component } from 'react';
import { connect } from 'react-redux';
import AbSyncSelectWidget from '../ab-syncselect';
import * as AbModelAction from '../../../actions';

const mapStateToProps = (state, ownProps) => {
  const { appbackApi, optionModel} = ownProps.options;
  var mapped = {
    AbModelInstance: appbackApi.createModelRest(optionModel, null)
  };
  return mapped;
}
function mapDispatchToProps(dispatch, ownProps) {
  return {
    getDataFromServer: function (payloadData) {
      const { optionModel } = ownProps.options;
      payloadData["AbModelName"] = optionModel;
      return dispatch(AbModelAction.getSelectModelData(payloadData));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AbSyncSelectWidget);