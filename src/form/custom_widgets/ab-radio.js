import React, { PropTypes } from "react";
import { Radio } from 'semantic-ui-react';
import { ABCHECKBOXTYPE } from '../../constants';

function AbRadioWidget(props) {
    const {id, disabled, options, value, autofocus, onChange} = props;
    const {enumOptions, inline, radioType, radioName} = options;
    let name = Math.random().toString();
    
    if(radioName){
        name = radioName
    }
    return (
        <div className={(inline) ? 'fields inline' : 'fields grouped'} id={id}>{
            enumOptions.map((option, index) => {
                const checked = option.value && value && option.value.toString() === value.toString();
                const radio = (
                    <div key={index} className={"field"}>
                        <Radio color="green"
                            id={`${id}_${index}`}
                            slider={(ABCHECKBOXTYPE.SLIDER === radioType)}
                            toggle={(ABCHECKBOXTYPE.TOGGLE === radioType)}
                            checked={checked}
                            name={name}
                            value={option.value.toString()}
                            label={<label>{option.label}</label>}
                            disabled={disabled}
                            autoFocus={autofocus && index === 0}
                            onChange={(event, data) => {
                                onChange(data.value.toString())
                            } } />
                    </div>
                );
                return radio
            })
        }</div>
    );
}

AbRadioWidget.defaultProps = {
    autofocus: false,
    options: {
        inline: false,
        radioName: null
    },
};

AbRadioWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    options: PropTypes.shape({
        enumOptions: PropTypes.array,
        inline: PropTypes.bool,
        radioType: PropTypes.string,
        radioName: PropTypes.string
    }).isRequired,
    value: PropTypes.any,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    autofocus: PropTypes.bool,
    onChange: PropTypes.func
};


export default AbRadioWidget;
