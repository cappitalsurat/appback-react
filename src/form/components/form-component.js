import React, { Component, PropTypes } from 'react';
import Form from "jb-react-jsonschema-form";
import { ACTIONBUTTON, FORM_CONDITION_TYPE, FORM_CONDITION_ACTION, GRID_DATA_SOURCE_TYPE } from '../../constants';
import _ from 'lodash';
import './form.css';
import { Label, Button, Segment } from 'semantic-ui-react'
import { AbMaskedInputWidget, AbCheckboxWidget, AbCheckboxesWidget, AbRadioWidget, AbSelectWidget, AbAsyncSelectWidget, AbSyncSelectWidget, AbDateTimePickerWidget, 
  AbEditableDateTimePickerWidget, AbModularDatePickerWidget, AbFileUploaderWidget, AbMultiFileUploaderWidget, AbPhoneInputWidget } from '../custom_widgets';
import ArrayFieldTemplate from '../array-field-template'
import CompactArrayFieldTemplate from '../compact-array-field-template'
export default class AbModelForm extends Component {

  constructor(props) {
    super(props);
    this.getFormSelectTransformList = this.getFormSelectTransformList.bind(this);
    let conditionsObj = {};
    const transformConditionObject = (schemaProperties) => {
      _.forEach(schemaProperties, (properties, key) => {
        if (properties['condition']) {
          conditionsObj[key] = properties.condition;
        } else if (properties['type'] == 'object') {
          transformConditionObject(properties['properties']);
        }
      })
    }
    if (props.schema.properties) {
      transformConditionObject(props.schema.properties);
    }

    let mainSchema = _.cloneDeep(props.schema);
    let mainUiScheme = _.cloneDeep(props.uiSchema);
    let transformSelectDetail = this.getFormSelectTransformList(mainUiScheme);
    let { transformFieldsDetail, transformFlagDetail } = this.getFormTransformList(mainSchema);
    this.state = {
      formData: props.formData,
      actualSchema: { ...props.schema },
      actualUiSchema: { ...props.uiSchema },
      schema: { ...mainSchema },
      uiSchema: { ...mainUiScheme },
      conditionsObj: conditionsObj,
      key: new Date().getTime(),
      transformSelectDetail: transformSelectDetail,
      transformFieldsDetail: transformFieldsDetail,
      transformFlagDetail: transformFlagDetail
    }
    var modifiedData = this.modifySchemeBasedOnCondition(_.cloneDeep(props.formData), true);
    this.state = { ...this.state, ...modifiedData }

    console.log("form-component constructor called");
  }

  getFormSelectTransformList = (mainUiScheme) => {
    let self = this;
    let selectCountToBeTransform = [];
    const fetchValues = (uiSchema, parent) => {
      if (uiSchema.constructor === Array) {
        _.map(uiSchema, function (fieldObj) {
          fetchValues(fieldObj, parent)
        });
      } else if (uiSchema.constructor === Object) {
        _.forOwn(uiSchema, function (schemeObj, key) {
          if (schemeObj && key) {
            if (schemeObj["ui:widget"] && schemeObj["ui:widget"] === 'AbMultiFileUploader') {
              parent.push(key);
            } else if (schemeObj["ui:options"] && schemeObj["ui:options"].multi && schemeObj["ui:widget"] && (schemeObj["ui:widget"] === "AbSelect" || schemeObj["ui:widget"] === "AbAsyncSelect" || schemeObj["ui:widget"] === "AbSyncSelect")) {
              parent.push(key);
            } else if (schemeObj["items"]) {
              parent.push(key);
              parent[key] = [];
              fetchValues(schemeObj["items"], parent[key]);
            } else {
              if (!schemeObj["ui:options"]) {
                parent.push(key);
                parent[key] = []
                fetchValues(schemeObj, parent[key])
              }
            }
          }
        })
      }
    }
    fetchValues(mainUiScheme, selectCountToBeTransform);
    return selectCountToBeTransform;
  }

  getFormTransformList = (mainSchema) => {
    let self = this;
    let fieldTobeTransform = [];
    let transformFlagDetail = {}
    const fetchFields = (schema, parent) => {
      // if (schema.constructor === Array) {
      // 	// _.map(schema, function (fieldObj) {
      // 	// 	fetchFields(fieldObj, parent)
      // 	// });
      // } else if (schema.type === "object" && schema.properties) {
      _.forOwn(schema.properties, function (schemeObj, key) {
        if (schemeObj && key) {
          if (schemeObj.type && schemeObj.type === "object" && schemeObj.transform) {
            if (schemeObj.transform !== "NOT") {
              transformFlagDetail[key] = true
            } else {
              transformFlagDetail[key] = false
            }
            parent.push(key);
            if (schemeObj.properties) {
              parent[key] = [];
              fetchFields(schemeObj, parent[key]);
            }
          } else if (schemeObj.type && schemeObj.type === "array" && schemeObj.items && schemeObj.items.constructor === Object && (!schemeObj.items.transform || schemeObj.items.transform !== 'NOT')) {
            parent.push(key);
            parent[key] = [];
            fetchFields(schemeObj.items, parent[key]);
          }

        }
      })
    }
    fetchFields(mainSchema, fieldTobeTransform);
    return { transformFieldsDetail: fieldTobeTransform, transformFlagDetail };
  }

  transformSelectValue = (transformArray, dataObject) => {
    _.map(_.uniq(transformArray), (fieldName) => {
      if (dataObject && dataObject.constructor === Array) {
        _.map(dataObject, (data) => {
          if (data && data[fieldName]) {
            if (transformArray[fieldName]) {
              this.transformSelectValue(transformArray[fieldName], data[fieldName])
            } else {
              if (data[fieldName].constructor === Array) {
                data[fieldName] = data[fieldName].join(",");
              }
            }
          }
        });
      } else if (dataObject && dataObject.constructor === Object) {

        if (dataObject && dataObject[fieldName]) {
          if (transformArray[fieldName]) {
            this.transformSelectValue(transformArray[fieldName], dataObject[fieldName])
          } else {
            if (dataObject[fieldName].constructor === Array) {
              dataObject[fieldName] = dataObject[fieldName].join(",");
            }
          }
        }
      }
    });
  }
  transformFieldsValueBefore = (schema, dataObject, finalData) => {
    _.forOwn(schema.properties, (schemaObj, key) => {
      if (schemaObj.type === "object") {
        if (!finalData[key]) {
          finalData[key] = {}
        }
        if (dataObject[key]) {
          this.transformFieldsValueBefore(schemaObj, dataObject[key], finalData[key]);
        } else {
          this.transformFieldsValueBefore(schemaObj, dataObject, finalData[key]);
        }
      } else if (schemaObj.type === "array" && schemaObj.items && schemaObj.items.type !== 'string') {
        if (!finalData[key]) {
          finalData[key] = [];
        }
        _.map(dataObject[key], (fieldObj, index) => {
          if (!finalData[key][index]) {
            finalData[key][index] = {}
          }
          this.transformFieldsValueBefore(schemaObj.items, fieldObj, finalData[key][index]);
        });
      } else {
        if (key in dataObject) {
          finalData[key] = _.cloneDeep(dataObject[key])
          delete dataObject[key];
        }
      }
    });
  }

  componentWillMount() {
    const { formData, saveCallback, errorCallback, dataSourceType, dataSourceOptions, loadData, getPreLoadFormData, AbModelInstance, formAction } = this.props;
    const { transformSelectDetail, actualSchema } = this.state;
    if (dataSourceType && dataSourceType === GRID_DATA_SOURCE_TYPE.METHOD) {
      if (dataSourceOptions && dataSourceOptions.LOAD && dataSourceOptions.LOAD.methodName) {
        if (loadData && loadData.id !== undefined) {
          getPreLoadFormData({ "filter": loadData, AbModelInstance, saveCallback, errorCallback, dataSourceType, "dataSourceOptions": dataSourceOptions.LOAD, transformSelectDetail });
        }
      }
    } else {
      if (!formData || Object.keys(formData).length <= 0 || formAction !== ACTIONBUTTON.EDIT) {
        if (loadData && loadData.id !== undefined) {
          getPreLoadFormData({ "filter": loadData, AbModelInstance, saveCallback, errorCallback });
        }
      }
    }

    if (formData) {
      let transformedFormData = _.cloneDeep(formData);
      // this.transformSelectValue(transformSelectDetail, transformedFormData);
      let finalData = {};
      this.transformFieldsValueBefore(actualSchema, transformedFormData, finalData);
      finalData = JSON.stringify(
        finalData,
        function (k, v) { if (v === null) { return undefined; } return v; }
      );
      finalData = JSON.parse(finalData)
      if (transformSelectDetail) {
        this.transformSelectValue(transformSelectDetail, finalData);
      }
      transformedFormData = { ...transformedFormData, ..._.cloneDeep(finalData) };
      this.modifySchemeBasedOnCondition(transformedFormData, false);
    }
  }

  componentWillReceiveProps(nextProps) {
    let { transformSelectDetail, actualSchema } = this.state;
    // let transformedFormData = _.cloneDeep(nextProps.formData);
    let transformedFormData;
    if (!_.isEqual(this.props.formData, nextProps.formData)) {
      transformedFormData = _.cloneDeep(nextProps.formData);
    } else {
      transformedFormData = _.cloneDeep(this.state.formData);
    }
    if (Object.keys(this.state.formData).length === 0 && Object.keys(transformedFormData).length > 0) {
      this.transformSelectValue(transformSelectDetail, transformedFormData);
      // let finalData = {};
      // transformFieldsValueBefore(actualSchema,transformedFormData,finalData);
      // transformedFormData = { ..._.cloneDeep(finalData)}
    }
    if (!_.isEqual(this.state.formData, transformedFormData)) {
      let finalData = {};
      this.transformFieldsValueBefore(actualSchema, transformedFormData, finalData);
      finalData = JSON.stringify(
        finalData,
        function (k, v) { if (v === null) { return undefined; } return v; }
      );
      finalData = JSON.parse(finalData)
      if (transformSelectDetail) {
        this.transformSelectValue(transformSelectDetail, finalData);
      }
      // transformedFormData = { ..._.cloneDeep(finalData) }
      transformedFormData = { ...transformedFormData, ..._.cloneDeep(finalData) }
      this.modifySchemeBasedOnCondition(transformedFormData, false);
      // this.setState({ formData: { ...nextProps.formData } });
    }
  }
  submitFormClick() {
    this.refs.formSubmitBtn.click();
  }
  cancelFormClick() {
    const { clearFormData } = this.props;
    this.setState({ key: new Date().getTime() })
    clearFormData();
  }
  setData(newFields) {
    let tempState = _.cloneDeep(this.state.formData);
    this.setState({
      formData: { ...tempState, ...newFields }
    })
  }

  getFormCurrentData() {
    return this.state.formData;
  }

  getDependentFieldFormValue(formData, dependentField) {
    let dependentValue = undefined
    if (dependentField.constructor === String) {
      if (formData[dependentField] !== undefined && formData[dependentField].toString().length > 0) {
        dependentValue = formData[dependentField];
      }
    } else if (dependentField.constructor === Object) {
      const getdependentValue = (form, dependentFieldKey) => {
        const key = _.keys(dependentFieldKey)
        if (key.length > 0 && dependentFieldKey[key[0]].constructor === Object) {
          if (form[key[0]]) {
            getdependentValue(form[key], dependentFieldKey[key[0]])
          }
        } else if (key.length > 0 && dependentFieldKey[key[0]].constructor === String) {
          if (form[key[0]]) {
            dependentValue = form[key[0]][dependentFieldKey[key[0]]]
          }
        }
      }
      getdependentValue(formData, dependentField)
    }
    return dependentValue
  }

  modifySchemeBasedOnCondition(formData, firstLoad) {
    let { conditionsObj, actualSchema, actualUiSchema } = this.state;
    let tempSchema = _.cloneDeep(actualSchema);
    let tempUiSchema = _.cloneDeep(actualUiSchema);
    _.forOwn(conditionsObj, (conditionProps, key) => {
      let { type = null, dependentField = null, fieldCheck, action, valueCheck = null, multiValueCheck = null } = conditionProps;
      if (type) {
        if (dependentField) {
          if (formData && type === FORM_CONDITION_TYPE.DEPENDENCY) {
            const dependentValue = this.getDependentFieldFormValue(formData, dependentField)
            if (dependentValue) {
              if (tempUiSchema) {
                const findFieldInUiSchema = (temporaryUiSchema) => {
                  if (temporaryUiSchema[key]) {
                    let widgetObject = temporaryUiSchema[key];
                    if (widgetObject["ui:widget"] === "AbSelect" || widgetObject["ui:widget"] === "AbSyncSelect") {
                      if (widgetObject["ui:options"]) {
                        if (widgetObject["ui:options"].customFilter) {
                          let customFilterToSend = widgetObject["ui:options"].customFilter;
                          if (customFilterToSend.where) {
                            customFilterToSend.where[fieldCheck] = dependentValue;
                          } else {
                            customFilterToSend['where'] = { [fieldCheck]: dependentValue };
                          }
                          widgetObject["ui:options"].customFilter = customFilterToSend
                        } else {
                          widgetObject["ui:options"]["customFilter"] = { where: { [fieldCheck]: dependentValue } }
                        }
                      }
                    }
                  } else {
                    _.forEach(temporaryUiSchema, (tUiSchema) => {
                      if (!_.isEmpty(tUiSchema) && tUiSchema.constructor === Object) {
                        findFieldInUiSchema(tUiSchema)
                      }
                    })
                  }
                }
                findFieldInUiSchema(tempUiSchema)
              }
            }
          } else {
            if (type === FORM_CONDITION_TYPE.VISIBILITY) {
              const fieldValue = this.getDependentFieldFormValue(formData, dependentField)
              if (valueCheck) {
                if (fieldValue !== undefined && fieldValue === valueCheck) {
                  return;
                }
              } else if (multiValueCheck) {
                if (fieldValue !== undefined && _.includes(multiValueCheck, fieldValue)) {
                  return;
                }
              } else {
                if (fieldValue !== undefined && fieldValue.toString().length > 0) {
                  return;
                }
              }
            }
            if (action === FORM_CONDITION_ACTION.HIDDEN) {
              if (tempSchema.properties[key]) {
                delete tempSchema.properties[key];
                delete tempUiSchema[key];
                delete formData[key];
              } else {
                const findProperty = (schemaProperties, uiSchema, dataObject) => {
                  _.forEach(schemaProperties, (schemaProperty, schemaKey) => {
                    if (schemaKey == key) {
                      delete schemaProperties[key];
                      delete dataObject[key];
                      delete uiSchema[key];
                    } else if (schemaProperty['type'] == 'object') {
                      let uiSchemaToSend = _.cloneDeep(uiSchema);
                      if (uiSchema[schemaKey]) {
                        uiSchemaToSend = uiSchema[schemaKey];
                      }
                      if (dataObject[schemaKey]) {
                        findProperty(schemaProperty.properties, uiSchemaToSend, dataObject[schemaKey])
                      } else {
                        findProperty(schemaProperty.properties, uiSchemaToSend, dataObject)
                      }
                    }
                  })
                }
                findProperty(tempSchema.properties, tempUiSchema, formData);
              }
            } else if (action === FORM_CONDITION_ACTION.DISABLED) {
              if (tempSchema.properties[key]) {
                if (tempUiSchema[key]) {
                  tempUiSchema[key]["ui:disabled"] = true;
                } else {
                  tempUiSchema[key] = { "ui:disabled": true };
                }
              } else {
                const findProperty = (schemaProperties, uiSchema) => {
                  _.forEach(schemaProperties, (schemaProperty, schemaKey) => {
                    if (schemaKey == key) {
                      // uiSchema[schemaKey] = { "ui:disabled": true };
                      uiSchema[schemaKey] = { ...uiSchema[schemaKey], "ui:disabled": true };
                    } else if (schemaProperty['type'] == 'object') {
                      if (!uiSchema[schemaKey]) {
                        uiSchema[schemaKey] = {};
                      }
                      findProperty(schemaProperty.properties, uiSchema[schemaKey])
                    }
                  })
                }
                findProperty(tempSchema.properties, tempUiSchema);
              }
            } else if (action === FORM_CONDITION_ACTION.ENABLED) {
              let finalAction = false;
              let fieldValue = this.getDependentFieldFormValue(formData, dependentField)
              if (valueCheck) {
                if (fieldValue && fieldValue === valueCheck) {
                  finalAction = true
                }
              } else if (multiValueCheck) {
                if (fieldValue && _.includes(multiValueCheck, fieldValue)) {
                  finalAction = true
                }
              } else {
                if (fieldValue && fieldValue.toString().length > 0) {
                  finalAction = true;
                }
              }
              if (tempSchema.properties[key]) {
                if (tempUiSchema[key]) {
                  tempUiSchema[key]["ui:disabled"] = finalAction;
                } else {
                  tempUiSchema[key] = { "ui:disabled": finalAction };
                }
              } else {
                const findProperty = (schemaProperties, uiSchema) => {
                  _.forEach(schemaProperties, (schemaProperty, schemaKey) => {
                    if (schemaKey == key) {
                      uiSchema[schemaKey] = { ...uiSchema[schemaKey], "ui:disabled": finalAction };
                    } else if (schemaProperty['type'] == 'object') {
                      if (!uiSchema[schemaKey]) {
                        uiSchema[schemaKey] = {};
                      }
                      findProperty(schemaProperty.properties, uiSchema[schemaKey])
                    }
                  })
                }
                findProperty(tempSchema.properties, tempUiSchema);
              }
            }
          }
        }
      }
    });
    if (firstLoad) {
      return { schema: tempSchema, uiSchema: tempUiSchema };
    } else {
      this.setState({
        formData: { ...formData },
        schema: { ...tempSchema },
        uiSchema: { ...tempUiSchema }
      })
    }
  }
  onChange(data) {
    const { onformDataChange } = this.props

    // if (data.errors && data.errors.length === 0) {
    this.modifySchemeBasedOnCondition(data.formData, false);
    // }

    if (onformDataChange) {
      onformDataChange(data);
    }
  }
  render() {
    let self = this;
    if (!this.props.abModelData) {
      return null;
    }
    const { AbModelInstance, showDefaultButtons, saveCallback, FieldTemplate, liveValidate, validate, showErrorList, noHtml5Validate, dataSourceType, dataSourceOptions } = this.props;
    const { filterCols, includes, externalFilter, formData, formAction, theme = {}, widgets } = this.props;
    const { createModelFormData, saveModelFormData, clearFormData } = this.props;
    const { schema, uiSchema, key, transformSelectDetail, transformFieldsDetail } = this.state;
    const log = (type) => {/*console.log.bind(console, type)*/ };

    const beformSubmitTransform = (metaData) => {
      const { formData, uiSchema } = metaData;
      const transformSelectValue = (transformArray, dataObject) => {
        _.map(_.uniq(transformArray), function (fieldName) {
          if (dataObject && dataObject.constructor === Array) {
            _.map(dataObject, function (data) {
              if (data && data[fieldName]) {
                if (transformArray[fieldName]) {
                  transformSelectValue(transformArray[fieldName], data[fieldName])
                } else {
                  if (data[fieldName].constructor === String) {
                    data[fieldName] = data[fieldName].split(",");
                  }
                }
              }
            });
          } else if (dataObject && dataObject.constructor === Object) {
            if (dataObject && dataObject[fieldName]) {
              if (transformArray[fieldName]) {
                transformSelectValue(transformArray[fieldName], dataObject[fieldName])
              } else {
                if (dataObject[fieldName].constructor === String) {
                  dataObject[fieldName] = dataObject[fieldName].split(",");
                }
              }
            }
          }
        });
      }

      // const transformInner = (transformArray, transformedFormData) => {
      // 	_.map(_.uniq(transformArray), function (fieldName) {
      // 		if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
      // 			transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
      // 			delete transformedFormData[fieldName];
      // 		} else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
      // 			_.map(transformedFormData[fieldName], function (fieldObject) {
      // 				if (transformArray[fieldName]) {
      // 					transformInner(transformArray[fieldName], fieldObject);
      // 				}
      // 			});
      // 		}
      // 	});
      // 	return transformedFormData;
      // }
      // const transformFieldsValue = (transformArray, transformedFormData) => {
      // 	// let transformedFormData = _.cloneDeep(transformedFormData);
      // 	_.map(_.uniq(transformArray), function (fieldName) {
      // 		if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Object) {
      // 			transformedFormData = { ...transformedFormData, ..._.cloneDeep(transformedFormData[fieldName]) };
      // 			delete transformedFormData[fieldName];
      // 		} else if (transformedFormData[fieldName] && transformedFormData[fieldName].constructor === Array) {
      // 			_.map(transformedFormData[fieldName], function (fieldObject, index) {
      // 				if (transformArray[fieldName]) {
      // 					let tempData = transformInner(transformArray[fieldName], fieldObject);
      // 					transformedFormData[fieldName][index] = { ..._.cloneDeep(tempData) }
      // 					// fieldObject = {...tempData};
      // 				}
      // 			});
      // 		}
      // 	});
      // 	return transformedFormData;
      // }
      transformSelectValue(transformSelectDetail, formData);
      // let finalData = {..._.cloneDeep(formData)};
      // let finalData = transformFieldsValue(transformFieldsDetail, _.cloneDeep(formData));
      // transformFieldsValue(transformFieldsDetail, finalData);
      metaData.formData = { ...formData }
      return metaData;

    }
    const formSubmit = (formMetaData) => {
      let transformedFormData = beformSubmitTransform(_.cloneDeep(formMetaData));
      const { transformFieldsDetail, transformFlagDetail } = this.state;
      let payloadData = {
        formMetaData: transformedFormData,
        AbModelInstance,
        filterCols,
        includes,
        externalFilter,
        saveCallback,
        dataSourceType,
        dataSourceOptions,
        transformFieldsDetail,
        transformFlagDetail
      }

      if (formAction === ACTIONBUTTON.CREATE) {
        createModelFormData(payloadData)
      } else if (formAction === ACTIONBUTTON.EDIT) {
        if (!_.isEqual(formData, formMetaData.formData)) {
          saveModelFormData(payloadData);
        } else {
          if (saveCallback) {
            saveCallback(formData);
          }
        }
      } else {

      }
    }
    const getSubmitButtonText = () => {
      if (formAction === ACTIONBUTTON.EDIT) {
        return ("Save");
      } else {
        return ("Create");
      }
    }
    let customWidgets = {
      AbCheckbox: AbCheckboxWidget,
      AbCheckboxes: AbCheckboxesWidget,
      AbRadio: AbRadioWidget,
      AbSelect: AbSelectWidget,
      AbAsyncSelect: AbAsyncSelectWidget,
      AbSyncSelect: AbSyncSelectWidget,
      AbDateTimePicker: AbDateTimePickerWidget,
      AbMaskedInput: AbMaskedInputWidget,
      AbEditableDateTimePicker: AbEditableDateTimePickerWidget,
      AbModularDatePicker: AbModularDatePickerWidget,
      AbFileUploader: AbFileUploaderWidget,
      AbMultiFileUploader: AbMultiFileUploaderWidget,
      AbPhoneInput: AbPhoneInputWidget
    };
    if (widgets) {
      customWidgets = { ...customWidgets, ...widgets }
    }

    let transformErrors = (errors) => {
      for (let index = 0; index < errors.length; index++) {
        const { schema, name, argument, instance } = errors[index];
        if (schema && schema.errorMsgs && schema.errorMsgs[name]) {
          errors[index].message = schema.errorMsgs[name];
        }
        if (name === 'type' && Array.isArray(argument) && argument.includes('number') && !isNaN(instance)) {
          errors.splice(index, 1);
          index--;
        }
      }
      return errors;
    }
    let onError = (error) => {
      const { errorCallback } = this.props;
      if (errorCallback) {
        errorCallback('validation', error)
      }
    }
    return (
      <div>
        <Form schema={schema}
          uiSchema={uiSchema}
          widgets={customWidgets}
          onChange={(data) => self.onChange(data)}
          onSubmit={(data) => formSubmit(data)}
          onError={(error) => onError(error)}
          formData={self.state.formData}
          ArrayFieldTemplate={(props) => props['schema'] && props['schema']['compact'] ? CompactArrayFieldTemplate({ ...props, ...{ theme: theme } }) : ArrayFieldTemplate({ ...props, ...{ theme: theme } })}
          className="ui form"
          showErrorList={showErrorList}
          noHtml5Validate={noHtml5Validate}
          FieldTemplate={FieldTemplate}
          transformErrors={transformErrors}
          liveValidate={liveValidate}
          validate={validate}
          key={key}
        >
          <div className={(showDefaultButtons) ? 'show' : 'hide'}>
            <button type="submit" ref="formSubmitBtn" className="ui button">{getSubmitButtonText()}</button>
            <button type="button" className="ui button" onClick={(e) => this.cancelFormClick(e)}>Cancel</button>
          </div>
        </Form>
      </div>
    );

  }
}

AbModelForm.propTypes = {
  modelName: PropTypes.string.isRequired,
  schema: PropTypes.object.isRequired,
  uiSchema: PropTypes.object,
  appbackApi: PropTypes.object.isRequired,
  showDefaultButtons: PropTypes.bool,
  saveCallback: PropTypes.func,
  errorCallback: PropTypes.func.isRequired,
  onformDataChange: PropTypes.func,
  liveValidate: PropTypes.bool,
  validate: PropTypes.func,
  showErrorList: PropTypes.bool,
  noHtml5Validate: PropTypes.bool,
  widgets: PropTypes.object
}

AbModelForm.defaultProps = {
  showDefaultButtons: true,
  saveCallback: null,
  uiSchema: {},
  liveValidate: false,
  validate: null,
  showErrorList: false,
  noHtml5Validate: false,
};
