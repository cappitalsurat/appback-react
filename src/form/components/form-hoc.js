import React from 'react';
import { connect } from 'react-redux';
import * as AbModelFormAction from '../../actions';

const FormWrapper = (props) => (Component) => {
    const mapStateToProps = (state, ownProps) => {
        const { appbackApi, modelName } = props;
        var mapped = {
            abModelData: state.abModels[modelName],
            formData: (state.abModels[modelName]) ? state.abModels[modelName].form.currentActiveItem : {},
            formAction: (state.abModels[modelName]) ? state.abModels[modelName].form.action : null,
            filterCols: (state.abModels[modelName]) ? state.abModels[modelName].filterCols : [],
            includes: (state.abModels[modelName]) ? state.abModels[modelName].includes : [],
            AbModelInstance: appbackApi.createModelRest(modelName, null)
        };
        return mapped;
    }
    const mapDispatchToProps = (dispatch, ownProps) => {
        dispatch(AbModelFormAction.setModelInitialData(props.modelName));
        return {
            saveModelFormData: function (payloadData) {
                const { modelName, errorCallback } = props;
                payloadData["AbModelName"] = modelName;
                payloadData["isFormWrapper"] = true;
                return dispatch(AbModelFormAction.saveModelFormData(payloadData));
            },
            createModelFormData: function (payloadData) {
                const { modelName, errorCallback } = props;
                payloadData["AbModelName"] = props.modelName;
                payloadData["isFormWrapper"] = true;
                return dispatch(AbModelFormAction.createModelFormData(payloadData));
            },
            clearFormData: function () {
                dispatch(AbModelFormAction.clearFormData({ modelName: props.modelName }));
            },
            getPreLoadFormData: function (loadData) {
                const { filter, AbModelInstance, saveCallback, errorCallback } = loadData;
                dispatch(AbModelFormAction.getPreLoadFormData({ AbModelName: props.modelName, ...{ ...loadData } }));
            }
        };
    }


    class FormHoc extends React.Component {
        render() {
            return <Component {...this.props} {...props} />
        }
    }
    return connect(mapStateToProps, mapDispatchToProps)(FormHoc)
}

export default FormWrapper